package com.colan.kindercare.bindiingUtils

import androidx.databinding.DataBindingComponent

import com.restaurant.management.bindingUtils.BaseImageViewBinding
import com.restaurant.management.bindingUtils.IImageViewBinding

class BaseDataBindingComponent : DataBindingComponent {

    override fun getIImageViewBinding(): IImageViewBinding {
        return BaseImageViewBinding()
    }

}