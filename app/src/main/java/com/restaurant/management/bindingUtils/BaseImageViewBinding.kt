package com.restaurant.management.bindingUtils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.restaurant.management.R

class BaseImageViewBinding : IImageViewBinding {

    override fun setImageFromDrawable(imageView: ImageView, filePath: Int?) {
        Glide.with(imageView.context).load(filePath).into(imageView)
    }

    override fun setImageFromUrl(imageView: ImageView, filePath: String?) {
        val option: RequestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_logo)
                .fitCenter()

        Glide.with(imageView.context)
                .load(filePath)
                .apply(option)
                .centerCrop()
                .into(imageView)
    }

}