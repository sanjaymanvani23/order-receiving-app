package com.restaurant.management.bindingUtils

import androidx.databinding.BindingAdapter
import android.view.View
import android.widget.ImageView

interface IImageViewBinding {

    @BindingAdapter("customImageFromDrawable")
    fun setImageFromDrawable(imageView: ImageView, filePath: Int?)

    @BindingAdapter("customImageFromUrl")
    fun setImageFromUrl(imageView: ImageView, filePath: String?)


}