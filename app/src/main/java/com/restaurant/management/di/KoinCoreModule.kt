package com.restaurant.management.di

import com.restaurant.management.data.local.sharedPref.ISharedPreferenceService
import com.restaurant.management.data.local.sharedPref.SharedPreferenceImp
import com.restaurant.management.data.remote.AuthTokenRefreshInterceptor
import com.restaurant.management.data.remote.client.ITrackingClient
import com.restaurant.management.data.remote.client.ITrackingClientBuilder
import com.restaurant.management.data.remote.client.TrackingClient
import com.restaurant.management.data.remote.client.TrackingClientBuilder
import com.restaurant.management.data.repository.IUserControllerRespository
import com.restaurant.management.data.repository.UserControllerRepository
import com.restaurant.management.data.repository.order.IOrderControllerRespository
import com.restaurant.management.data.repository.order.OrderControllerRepository
import okhttp3.Interceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

class KoinCoreModule {
    val koinCoreModule = module {
        single<ISharedPreferenceService>( definition = { SharedPreferenceImp(androidApplication()) })
        single<Interceptor> { AuthTokenRefreshInterceptor(get()) }
        single<ITrackingClientBuilder> { TrackingClientBuilder(get(),get()) }
        single<ITrackingClient> { TrackingClient(get()) }
        single<IUserControllerRespository> { UserControllerRepository(get<ITrackingClient>().getUserControllerRepository()) }
        single<IOrderControllerRespository> { OrderControllerRepository(get<ITrackingClient>().getOrderControllerRepository()) }
    }
}