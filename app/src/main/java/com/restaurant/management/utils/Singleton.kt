package com.restaurant.management.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object Singleton {

    var bundle = Bundle()

    var currencyFormat = ""

    var viewBtn = ""

    var timeZone = ""

    var isTimeAdded = false

    var isOrdeDetailsScreen = false


    var mp: MediaPlayer? = null

    var helpContent: String = ""

    fun navigateTo(context: Context, clazz: Class<*>, mExtras: Bundle) {
        val intent = Intent(context, clazz)
        intent.putExtras(mExtras)
        context.startActivity(intent)
    }

    fun navigateWithClearTop(context: Context, clazz: Class<*>, mExtras: Bundle) {
        val intent = Intent(context, clazz)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtras(mExtras)
        context.startActivity(intent)
        ActivityCompat.finishAffinity(context as Activity)
    }

    fun getTime(timezone: String?): String? {
        val c = Calendar.getInstance()
        val date = c.time //current date and time in UTC
        val df = SimpleDateFormat("HH:MM")
        df.setTimeZone(TimeZone.getTimeZone(timezone)) //format in given timezone
        return df.format(date)
    }

    fun dateFormat(date: String, timezone: String?): String {
        val normalForat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        Log.d("normal length", "" + normalForat.length)
        Log.d("date length", "" + date.length)
        val timeZoneID = timezone
        val tz = TimeZone.getTimeZone(timeZoneID)
        if (date.length == 20) {
            val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
//            val dates: Date = parser.parse(date)
//            val calend= changeTimezoneOfDate(dates,TimeZone.getDefault(),tz)
//            Log.d("newdate","fromDate:"+date+" ToDate:"+calend?.time)

            parser.setTimeZone(TimeZone.getTimeZone("UTC"))
            val parsed: Date = parser.parse(date)

            val formatter = SimpleDateFormat("dd.MM.yyyy HH:mm")
            formatter.timeZone = tz
            val output = formatter.format(parsed)
            return output
        } else {
            val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            parser.setTimeZone(TimeZone.getTimeZone("UTC"))
            val parsed: Date = parser.parse(date)

            val formatter = SimpleDateFormat("dd.MM.yyyy HH:mm")
            formatter.timeZone = tz
            val output = formatter.format(parsed)
            return output
        }
    }

    fun changeTimezoneOfDate(date: Date?, fromTZ: TimeZone, toTZ: TimeZone): Calendar? {
        val calendar = Calendar.getInstance()
        calendar.time = date
        val millis = calendar.timeInMillis
        val fromOffset = fromTZ.getOffset(millis).toLong()
        val toOffset = toTZ.getOffset(millis).toLong()
        val convertedTime = millis - (fromOffset - toOffset)
        val c = Calendar.getInstance()
        c.timeInMillis = convertedTime
        return c
    }

    fun dateTimeFormat(date: String): String {
        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val formatter = SimpleDateFormat("HH:mm")
        val output = formatter.format(parser.parse(date))
        return output
    }

    fun convertTimeZone(date: Date, fromTimeZone: TimeZone?, toTimeZone: TimeZone?): Date? {
        val fromTimeZoneOffset: Long = getTimeZoneUTCAndDSTOffset(date, fromTimeZone!!)
        val toTimeZoneOffset: Long = getTimeZoneUTCAndDSTOffset(date, toTimeZone!!)
        return Date(date.time + (toTimeZoneOffset - fromTimeZoneOffset))
    }

    private fun getTimeZoneUTCAndDSTOffset(date: Date, timeZone: TimeZone): Long {
        var timeZoneDSTOffset: Long = 0
        if (timeZone.inDaylightTime(date)) {
            timeZoneDSTOffset = timeZone.dstSavings.toLong()
        }
        return timeZone.rawOffset + timeZoneDSTOffset
    }

    fun timeToDate(time: String): String {
        val parser = SimpleDateFormat("HH:mm")
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val output = formatter.format(parser.parse(time))
        return output
    }

    fun timeToDate(time: String, date: String): String {
        val parser = SimpleDateFormat("dd.MM.yyyy HH:mm")
        val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        var output = formatter.format(parser.parse("$date $time"))

//        val dateFormat =
//            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
//        dateFormat.timeZone = TimeZone.getTimeZone("UTC")
//        val formatted = dateFormat.format(output)

        val format =
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        try {
            val date = format.parse(output)
            output = getDateInUTC(date)
            System.out.println(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return output
    }

    fun getDateInUTC(date: Date?): String? {
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        val dateAsString = sdf.format(date)
        println("UTC$dateAsString")
        return dateAsString
    }

    fun getCalander(calendarDate: Calendar, year: Int, month: Int, dayOfMonth: Int): Calendar {
        calendarDate.set(Calendar.YEAR, year)
        calendarDate.set(Calendar.MONTH, month)
        calendarDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        return calendarDate
    }


    fun getDateinYYYYMMDD(calendarDate: Calendar): String {

        if (calendarDate == null) {
            return ""
        }
        return Constants.DATE_FILTER_HISTORY.format(calendarDate.time)
    }

    fun checkStartEndDate(fromDate: String, toDate: String): Boolean {
        var result = false
        try {
            val sdf = SimpleDateFormat("yyyy-MM-dd")

            val dateStart = sdf.parse(fromDate)
            val dateEnd = sdf.parse(toDate)

            if (dateStart.compareTo(dateEnd) > 0) {
                println("dateStart is after dateEnd ")
                result = false
            } else if (dateStart.compareTo(dateEnd) < 0) {
                println("dateStart is before dateEnd ")
                result = true
            } else if (dateStart.compareTo(dateEnd) === 0) {
                println("dateStart is equal to dateEnd ")
                result = true
            } else {
                println("How to get here?")
            }
        } catch (ex: ParseException) {
            ex.printStackTrace()
        }
        return result
    }


    fun getCalenderFromStringDate(stringDate: String): Calendar {
        val cal = Calendar.getInstance()
        cal.time = Constants.DATE_FILTER_HISTORY.parse(stringDate)
        return cal
    }

    fun getTwoNumberInTwoDigit(num: Int): String {
//        val formatter: NumberFormat = DecimalFormat("##")
//        return formatter.format(num)
//        return  String.format("%02d", num)
        return num.toString().padStart(2, '0')
    }

    fun isDateisCurrentDate(date: String): Boolean {
        val cal = Calendar.getInstance()
        val sdf = SimpleDateFormat(Constants.DATE_FILTER_HISTORY_FORMAT, Locale.getDefault())
        cal.time = sdf.parse(date)

        val year: Int = cal.get(Calendar.YEAR)
        val month: Int = cal.get(Calendar.MONTH) + 1
        val day: Int = cal.get(Calendar.DAY_OF_MONTH)

        val currentCalendar = Calendar.getInstance()
        return currentCalendar.get(Calendar.DAY_OF_MONTH) == day
                && (currentCalendar.get(Calendar.MONDAY) + 1) == month
                && currentCalendar.get(Calendar.YEAR) == year


    }

}