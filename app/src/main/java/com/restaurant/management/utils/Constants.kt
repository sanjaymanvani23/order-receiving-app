package com.restaurant.management.utils

import java.text.SimpleDateFormat
import java.util.*

object Constants {

    const val ALREADY_LOGED_IN = "already_loged"
    const val AUTHORIZATION_KEY = "authorization"
    const val AUTOMATIC_PRINT = "AUTOMATIC_PRINT"
    const val NUM_OF_COPIES = "NUM_OF_COPIES"
    const val VOLUME_COUNT = "VOLUME_COUNT"
    const val NUM_OF_VOLUME = "NUM_OF_VOLUME"
    const val SELECTED_ALARM = "SELECTED_ALARM"
    const val SELECTED_ALARM_INDEX = "SELECTED_ALARM_INDEX"
    const val REFRESH_TOKEN = "REFRESH_TOKEN"
    const val IS_FIRST_TIME = "IS_FIRST_TIME"
    const val USER_NAME = "userName"
    const val USER_ID = "userId"
    const val USER_EMAIL = "userEmail"
    const val TIME_ZONE = "TIME_ZONE"
    const val APP_NAME = "APP_NAME"
    const val APP_IMAGE = "APP_IMAGE"
    const val STATUS = "STATUS"
    const val ADDRESS = "ADDRESS"
    const val PHONE_NO = "PHONE_NO"
    const val CURRENCY = "CURRENCY"
    const val NEW = "New"
    const val COOKING = "Accepted"
    const val IN_DELIVERY = "In-Delivery"
    const val Dispatched = "Dispatched"
    const val DeliveryRejected = "DeliveryRejected"
    const val DeliveryAccepted = "DeliveryAccepted"
    const val DeliveryCancelled = "DeliveryCancelled"
    const val LOGOUT = "LOGOUT"

    const val Completed = "Completed"
    const val Delivered = "Delivered"
    const val PaymentCollected = "PaymentCollected"
    const val PickedUp = "PickedUp"

    const val PreOrdered = "PreOrdered"
    const val PREORDERACCEPT = "PREORDERACCEPT"
    const val PREORDERREJECT = "PREORDERREJECT"
    const val ACCEPT = "Accepted"
    const val REJECTED = "Rejected"
    const val REJECTED_REASON = "REJECTED_REASON"
    const val DISPATCHED = "Dispatched"
    const val ARCHIVE = "Archived"

    const val TODAY = "1"
    const val THISWEEK = "2"
    const val LASTWEEK = "3"
    const val THISMONTH = "4"
    const val LASTMONTH = "5"

    const val PREORDER = "1"
    const val CURRENT_ORDER = "2"

    const val CASH = "2"
    const val ONLINE = "1"

    const val CLICKED_ORDERID = "CLICKED_ORDERID"
    const val DETAIL_PAGE = "DETAIL_PAGE"
    const val HISTORY_PAGE = "HISTORY_PAGE"
    const val FIRST_TIME_DETAILS = "FIRST_TIME_DETAILS"
    const val SHOW_CUSTOMER_DETAILS = "SHOW_CUSTOMER_DETAILS"

    const val LANGUAGE_NAME_LIST = "LANGUAGE_NAME_LIST"
    const val LANGUAGE_DISPLAY_NAME_LIST = "LANGUAGE_DISPLAY_NAME_LIST"

    const val SELECTED_LANGUAGE = "SELECTED_LANGUAGE"

    val DATE_FILTER_HISTORY_FORMAT = "yyyy/MM/dd hh:mm"
    val DATE_FILTER_HISTORY = SimpleDateFormat(DATE_FILTER_HISTORY_FORMAT, Locale.getDefault())
}