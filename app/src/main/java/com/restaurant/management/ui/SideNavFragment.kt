package com.restaurant.management.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.restaurant.management.R
import com.restaurant.management.R.layout.side_nav_fragment
import com.restaurant.management.adapter.SideNavAdapter
import com.restaurant.management.data.local.sharedPref.ISharedPreferenceService
import com.restaurant.management.data.model.sidenav.SideNavItem
import kotlinx.android.synthetic.main.side_nav_fragment.*
import org.koin.core.KoinComponent
import org.koin.core.inject


class SideNavFragment :Fragment(), KoinComponent {

    val sharedPreference: ISharedPreferenceService by inject()

    private val sideNavAdapter: SideNavAdapter = SideNavAdapter { position, item ->
        onItemClick(position,item)
    }

    private fun onItemClick(position: Int, item: SideNavItem) {
        Toast.makeText(requireContext(),""+item.itemName,Toast.LENGTH_LONG).show()
        (activity as DashBoardActivity).closeDrawer(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?  = inflater.inflate(side_nav_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViews()
        close_iv.setOnClickListener {
            (activity as DashBoardActivity).closeDrawer()
        }
    }

    private fun setUpViews() {
        rv_side_nav_options?.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        rv_side_nav_options?.adapter = sideNavAdapter
        sideNavAdapter.setNavItemsData(prrepareNavItems())
    }

    //Create List of items to be displayed on the sidenav
    private fun prrepareNavItems(): List<SideNavItem> {
        val menuItemsList = ArrayList<SideNavItem>()
        menuItemsList.add(SideNavItem(1,sharedPreference.getStringValue("home"),R.drawable.home_active,R.drawable.home_inactive,true))
        menuItemsList.add(SideNavItem(2,sharedPreference.getStringValue("WIFISelection"),R.drawable.wifi_active,R.drawable.wifi_inactive,false))
        menuItemsList.add(SideNavItem(3,sharedPreference.getStringValue("OrderHistory"),R.drawable.history_active,R.drawable.history_inactive,false))
        menuItemsList.add(SideNavItem(4,sharedPreference.getStringValue("settings"),R.drawable.settings_active,R.drawable.settings_inactive,false))
        menuItemsList.add(SideNavItem(5,sharedPreference.getStringValue("Help"),R.drawable.help_active,R.drawable.help_inactive,false))
        menuItemsList.add(SideNavItem(6,sharedPreference.getStringValue("logoutText"),R.drawable.ic_logout,R.drawable.ic_logout,false))
        return menuItemsList
    }
}