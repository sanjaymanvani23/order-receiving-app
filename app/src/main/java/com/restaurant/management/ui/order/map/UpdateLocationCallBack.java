package com.restaurant.management.ui.order.map;

import android.location.Location;

public interface UpdateLocationCallBack {
    void onUpdatedLocation(Location updatedLocation);
}
