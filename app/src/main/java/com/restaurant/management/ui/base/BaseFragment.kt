package com.restaurant.management.ui.base

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.restaurant.management.MyApplication
import com.restaurant.management.utils.LoadingIndicatorView

abstract class BaseFragment<T : ViewDataBinding, out V : BaseViewModel<*>> : Fragment() {
    var baseActivity: BaseActivity<*, *>? = null
        private set
    var mRootView: View? = null
    private var mRootViewGrp: ViewGroup? = null
    var viewDataBinding: T? = null
        private set
    private var mViewModel: V? = null

    private var progressBar: ProgressBar? = null
    private var loadingIndicatorView: LoadingIndicatorView? = null
    var primaryColor = ObservableField(Color.parseColor("#20B8AB"))

    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract val bindingVariable: Int

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutId: Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract val viewModel: V


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>) {
            val activity = context as BaseActivity<*, *>?
            this.baseActivity = activity
            //activity!!.onFragmentAttached()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = viewModel
        setHasOptionsMenu(false)
        initLoadingIndicator()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        mRootView = viewDataBinding!!.root
        mRootViewGrp = container
        return mRootView
    }


    /*function:  Loading Indicator*/
    fun initLoadingIndicator() {
        try {
            loadingIndicatorView = LoadingIndicatorView(requireContext())
            loadingIndicatorView?.setColor(
                String.format(
                    "#%06X",
                    0xFFFFFF and primaryColor.get()!!
                )
            )
        } catch (e: Exception) {
        }
    }

    fun showLoadingIndicator() {
        try {
            if (!loadingIndicatorView?.isShowing!!) {
                loadingIndicatorView?.show()
            }
        } catch (e: Exception) {
        }
    }

    fun dismissLoadingIndicator() {
        try {
            if (loadingIndicatorView?.isShowing!!) {
                loadingIndicatorView?.dismiss()
            }
        } catch (e: Exception) {
        }
    }

    fun putToast(message: String?) {
        Toast.makeText(MyApplication.appContext, "" + message, Toast.LENGTH_SHORT).show()
    }


    override fun onDetach() {
        dismissProgressBar()
        baseActivity = null
        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDataBinding!!.setVariable(bindingVariable, mViewModel)
        viewDataBinding!!.executePendingBindings()
    }


    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()

    }

    override fun onDestroy() {
        super.onDestroy()
    }


    /**
     * common show progress bar for all screens
     *
     * @param parent layout of screen
     */
    fun showProgressBar(parent: ViewGroup) {
        baseActivity!!.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        progressBar = ProgressBar(baseActivity, null, android.R.attr.progressBarStyleLarge)
        when (parent) {
            is RelativeLayout -> {
                val params = RelativeLayout.LayoutParams(130, 130)
                params.addRule(RelativeLayout.CENTER_IN_PARENT)
                parent.addView(progressBar, params)
            }
            is FrameLayout -> {
                val params = FrameLayout.LayoutParams(130, 130)
                params.gravity = Gravity.CENTER
                parent.addView(progressBar, params)
            }
            is CoordinatorLayout -> {
                val params = CoordinatorLayout.LayoutParams(130, 130)
                params.gravity = Gravity.CENTER
                parent.addView(progressBar, params)
            }
            is ConstraintLayout -> {
                val params = ConstraintLayout.LayoutParams(130, 130)
                params.topToTop = ConstraintSet.PARENT_ID
                params.startToStart = ConstraintSet.PARENT_ID
                params.endToEnd = ConstraintSet.PARENT_ID
                params.bottomToBottom = ConstraintSet.PARENT_ID
                parent.addView(progressBar, params)
            }
        }
        progressBar?.visibility = View.VISIBLE  //To show ProgressBar
    }


    /**
     * common dismiss progress bar for all screens
     *
     */
    fun dismissProgressBar() {
        progressBar?.visibility = View.GONE     // To Hide ProgressBar
        baseActivity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }
}