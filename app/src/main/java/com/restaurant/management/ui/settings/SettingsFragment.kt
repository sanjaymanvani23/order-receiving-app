package com.restaurant.management.ui.settings

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.restaurant.management.BR
import com.restaurant.management.R
import com.restaurant.management.databinding.FragmentSettingsBinding
import com.restaurant.management.ui.CommonViewModel
import com.restaurant.management.ui.IFragmentTransaction
import com.restaurant.management.ui.LoginActivity
import com.restaurant.management.ui.base.BaseFragment
import com.restaurant.management.ui.base.BaseNavigator
import com.restaurant.management.utils.Constants
import com.restaurant.management.utils.Singleton
import kotlinx.android.synthetic.main.fragment_settings.*


class SettingsFragment : BaseFragment<FragmentSettingsBinding, CommonViewModel>(),
    BaseNavigator, AdapterView.OnItemSelectedListener {

    private lateinit var iFragmentTransaction: IFragmentTransaction
    private var spinnerTouched = false
    var mp: MediaPlayer? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        viewModel.setNavigator(this)
        return viewDataBinding?.root
    }

    override val bindingVariable: Int
        get() = BR.dashBoardVM
    override val layoutId: Int
        get() = R.layout.fragment_settings
    override val viewModel: CommonViewModel
        get() = activity?.run {
            ViewModelProvider(this).get(CommonViewModel::class.java)
        } ?: throw Exception("Invalid Activity")


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.languageList.clear()
        viewModel.languageDisplayNameList.clear()
        viewModel.languageList.addAll(
            viewModel.sharedPreference.getStringValue(Constants.LANGUAGE_NAME_LIST).split(",")
        )
        viewModel.languageDisplayNameList.addAll(
            viewModel.sharedPreference.getStringValue(Constants.LANGUAGE_DISPLAY_NAME_LIST)
                .split(",")
        )

        country_spn.setOnItemSelectedListener(this)
        country_spn.setOnTouchListener { v, event ->
            println("Real touch felt.")
            spinnerTouched = true
            false
        }
        alarm_spinner.setOnTouchListener { v, event ->
            println("Real touch felt.")
            spinnerTouched = true
            false
        }

        val aa: ArrayAdapter<*> =
            ArrayAdapter<String>(
                requireContext(),
                android.R.layout.simple_spinner_item,
                viewModel.languageDisplayNameList
            )
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        country_spn.setAdapter(aa)
        val index = viewModel.languageList.indexOf(
            viewModel.sharedPreference.getStringValue(Constants.SELECTED_LANGUAGE)
        )
        alarm_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                if (spinnerTouched) {
                    viewModel.sharedPreference.setStringValue(
                        Constants.SELECTED_ALARM,
                        "" + p0?.selectedItem
                    )
                    viewModel.sharedPreference.setIntegerValue(
                        Constants.SELECTED_ALARM_INDEX,
                        position
                    )
                  //  putToast("" + p0?.selectedItem)
                }
                Log.d("myalarm",""+viewModel.sharedPreference.getStringValue(Constants.SELECTED_ALARM))
                spinnerTouched = false
            }

        }
        text_btn.setOnClickListener {
            //putToast("" + alarm_spinner.selectedItem)
            mp?.isPlaying?.let {
                if (it) {
                    mp?.reset()
                    mp?.stop()
                }
            }

            when (alarm_spinner.selectedItem) {
                "Alarm 1" -> {
                    testRingTone(R.raw.alarm_1)
                }
                "Alarm 2" -> {
                    testRingTone(R.raw.alarm_2)
                }
                "Alarm 3" -> {
                    testRingTone(R.raw.alarm_3)
                }
                "Alarm 4" -> {
                    testRingTone(R.raw.alarm_4)
                }
                "Alarm 5" -> {
                    testRingTone(R.raw.alarm_5)
                }
                "Alarm 6" -> {
                    testRingTone(R.raw.alarm_6)
                }
                "Alarm 7" -> {
                    testRingTone(R.raw.alarm_7)
                }
                "Alarm 8" -> {
                    testRingTone(R.raw.alarm_8)
                }
                "Alarm 9" -> {
                    testRingTone(R.raw.alarm_9)
                }
                "Alarm 10" -> {
                    testRingTone(R.raw.alarm_10)
                }
                "Alarm 11" -> {
                    testRingTone(R.raw.alarm_11)
                }
                "Alarm 12" -> {
                    testRingTone(R.raw.alarm_12)
                }
                "Alarm 13" -> {
                    testRingTone(R.raw.alarm_13)
                }
                "Alarm 14" -> {
                    testRingTone(R.raw.alarm_14)
                }
                "Alarm 15" -> {
                    testRingTone(R.raw.alarm_15)
                }
                "Alarm 16" -> {
                    testRingTone(R.raw.alarm_16)
                }
                "Alarm 15" -> {
                    testRingTone(R.raw.alarm_15)
                }
                "Alarm 16" -> {
                    testRingTone(R.raw.alarm_16)
                }
                "FC Basel 1" -> {
                    testRingTone(R.raw.fc_basel_1)
                }
                "FC Basel 2" -> {
                    testRingTone(R.raw.fc_basel_2)
                }
            }
        }
        country_spn.setSelection(index)
        if (viewModel.sharedPreference.containsKey(Constants.SELECTED_ALARM_INDEX)) {
            alarm_spinner.setSelection(viewModel.sharedPreference.getIntegerValue(Constants.SELECTED_ALARM_INDEX))
        }

        viewModel.isAutoPrintChecked.set(viewModel.sharedPreference.getBooleanValue(Constants.AUTOMATIC_PRINT))

        viewModel.isAutoPrintChecked.get()?.let {
            if (it) {
                viewModel.isAutoPrintOn.set(true)
                viewModel.isAutoPrintOff.set(false)
            } else {
                viewModel.isAutoPrintOn.set(false)
                viewModel.isAutoPrintOff.set(true)
            }
        }


        toggle.setOnCheckedChangeListener { group, checkedId -> // checkedId is the RadioButton selected
            when (checkedId) {
                R.id.off_button -> {
                    viewModel.isAutoPrintChecked.set(false)
                    viewModel.sharedPreference.setBooleanValue(Constants.AUTOMATIC_PRINT, false)
                }
                R.id.on_button -> {
                    viewModel.isAutoPrintChecked.set(false)
                    viewModel.sharedPreference.setBooleanValue(Constants.AUTOMATIC_PRINT, true)
                }
            }
        }


        viewModel.numberOfCopy.set(viewModel.sharedPreference.getIntegerValue(Constants.NUM_OF_COPIES))

        increase.setOnClickListener {
            val count = viewModel.numberOfCopy.get()?.toInt()?.plus(1)
            viewModel.numberOfCopy.set(count)
            count?.let { it1 ->
                viewModel.sharedPreference.setIntegerValue(
                    Constants.NUM_OF_COPIES,
                    it1
                )
            }
        }
        decrease.setOnClickListener {
            val count = viewModel.numberOfCopy.get()?.toInt()?.minus(1)
            viewModel.numberOfCopy.set(count)
            count?.let { it1 ->
                viewModel.sharedPreference.setIntegerValue(
                    Constants.NUM_OF_COPIES,
                    it1
                )
            }
        }

        val audioManager = requireContext().getSystemService(Context.AUDIO_SERVICE) as AudioManager
        if (viewModel.volumeCount.get() == 1) {
            viewModel.volumeCount.set(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC))
        } else {
            viewModel.volumeCount.set(viewModel.sharedPreference.getIntegerValue(Constants.VOLUME_COUNT))
        }

        //putToast("" + audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC))
        volume_tv_increase.setOnClickListener {
            if (viewModel.volumeCount.get()!! < audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)) {
                val count = viewModel.volumeCount.get()?.toInt()?.plus(1)
                viewModel.volumeCount.set(count)
                count?.let { it1 ->
                    audioManager.setStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        it1, 0
                    )
                };
                count?.let { it1 ->
                    viewModel.sharedPreference.setIntegerValue(
                        Constants.VOLUME_COUNT,
                        it1
                    )
                }
            }
        }

        volume_tv_decrease.setOnClickListener {
            val count = viewModel.volumeCount.get()?.toInt()?.minus(1)
            viewModel.volumeCount.set(count)
            count?.let { it1 ->
                audioManager.setStreamVolume(
                    AudioManager.STREAM_MUSIC,
                    it1, 0
                )
            };
            count?.let { it1 ->
                viewModel.sharedPreference.setIntegerValue(
                    Constants.VOLUME_COUNT,
                    it1
                )
            }
        }

        cancel_settings.setOnClickListener {
            iFragmentTransaction.transaction(this)
            //Singleton.navigateWithClearTop(requireContext(),DashBoardActivity::class.java,Singleton.bundle)
        }
        observeResponse()
    }

    private fun testRingTone(res: Int) {
        mp = MediaPlayer.create(requireContext(), res)
        mp?.setOnPreparedListener(MediaPlayer.OnPreparedListener {
            mp?.start()
        })
    }

    private fun observeResponse() {
        viewModel.localiztationResponse.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                it.data?.result?.forEach {
                    viewModel.sharedPreference.setStringValue(
                        it.key.toString(),
                        it.label.toString()
                    )
                }

                Singleton.navigateWithClearTop(
                    requireContext(),
                    LoginActivity::class.java,
                    Singleton.bundle
                )

            }
        })
    }


    override fun onClickView(v: View?) {

    }

    override fun goTo(clazz: Class<*>, mExtras: Bundle?) {

    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        if (spinnerTouched) {
            viewModel.sharedPreference.setStringValue(
                Constants.SELECTED_LANGUAGE,
                viewModel.languageList[position]
            )
            p0?.setSelection(position)
            viewModel.callLanguageAPI(viewModel.languageList[position])

            spinnerTouched = false
//        if(viewModel.sharedPreference.containsKey(Constants.SELECTED_LANGUAGE)){
//            val index=viewModel.languageDisplayNameList.indexOf(viewModel.sharedPreference.getStringValue(Constants.SELECTED_LANGUAGE))
//            p0?.setSelection(index)
//        }else{
//            viewModel.sharedPreference.setStringValue(Constants.SELECTED_LANGUAGE,viewModel.languageList[position])
//        }

        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            iFragmentTransaction = context as IFragmentTransaction
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement OnFragmentSelectedLister")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mp?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mp?.stop()
    }

}