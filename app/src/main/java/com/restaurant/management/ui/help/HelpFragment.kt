package com.restaurant.management.ui.help

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.restaurant.management.R
import com.restaurant.management.utils.Singleton


class HelpFragment : Fragment() {

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_help, container, false)
        val webView: WebView =view.findViewById(R.id.myWebView)
        webView.settings.javaScriptEnabled=true
        webView.loadDataWithBaseURL(null,
            Singleton.helpContent,"text/html; charset=utf-8", "UTF-8",null)
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

    }


}