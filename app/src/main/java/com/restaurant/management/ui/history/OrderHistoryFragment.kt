package com.restaurant.management.ui.history

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayout
import com.google.firebase.firestore.*
import com.restaurant.management.BR
import com.restaurant.management.R
import com.restaurant.management.adapter.BaseRecyclerViewAdapter
import com.restaurant.management.adapter.OnDataBindCallback
import com.restaurant.management.data.model.SpinnerModel
import com.restaurant.management.data.model.response.Item
import com.restaurant.management.databinding.FragmentOrderHistoryBinding
import com.restaurant.management.databinding.HistoryOrderListItemBinding
import com.restaurant.management.ui.CommonViewModel
import com.restaurant.management.ui.IFragmentTransaction
import com.restaurant.management.ui.base.BaseFragment
import com.restaurant.management.ui.base.BaseNavigator
import com.restaurant.management.ui.order.OrderDetailsFragment
import com.restaurant.management.utils.Constants
import com.restaurant.management.utils.Singleton
import kotlinx.android.synthetic.main.fragment_home.tabLayout
import kotlinx.android.synthetic.main.fragment_order_history.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class OrderHistoryFragment : BaseFragment<FragmentOrderHistoryBinding, CommonViewModel>(),
    OnDataBindCallback<HistoryOrderListItemBinding>, BaseNavigator,
    DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private lateinit var iFragmentTransaction: IFragmentTransaction
    private var baseRecyclerAdapter: BaseRecyclerViewAdapter<Item, HistoryOrderListItemBinding>? =
        null
    val firebaseFirestoreDB = FirebaseFirestore.getInstance()
    private var filterType: String? = "1"
    private var paymentType: String? = null
    val documentRef =
        firebaseFirestoreDB.collection("RestaurantCollection").document("Restaurants-1")
            .collection("BranchCollection").document("Branch-1").collection("orders")

    companion object {
        var status: String = "New"
    }

    override val bindingVariable: Int
        get() = BR.dashBoardVM
    override val layoutId: Int
        get() = R.layout.fragment_order_history
    override val viewModel: CommonViewModel
        get() = activity?.run {
            ViewModelProvider(this).get(CommonViewModel::class.java)
        } ?: throw Exception("Invalid Activity")


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        viewModel.setNavigator(this)
        viewModel.isOrderhistory.set(true)
        viewModel.orderDetailsPage.set(false)

        setStartEndDate()

        documentRef.get().addOnSuccessListener { result ->
            for (document in result) {
//                Log.d("onCreateView data", "" + document.data)
            }
        }
        documentRef.addSnapshotListener(this) { documentSnapshot, firebaseFirestoreException ->
            viewModel.getRefreshOrderHistoryAPI(filterType, paymentType)
        }

//        viewModel.getCourier()
        viewModel.getCourierHistory()
        viewModel.getOrderHistoryAPI(Constants.TODAY, null)
        setUpRecyclerView()
        observeResponse()
        return viewDataBinding!!.root
    }

    private fun setStartEndDate() {
        val writeDate = SimpleDateFormat(Constants.DATE_FILTER_HISTORY_FORMAT, Locale.getDefault())
        writeDate.timeZone =
            TimeZone.getTimeZone(viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE))
        val currentCalendar = Calendar.getInstance()
        val currentDateTimeString = writeDate.format(currentCalendar.time)

        Log.e(
            "OrderHistoryFragment",
            "setStartEndDate currentDateTimeString: $currentDateTimeString"
        )
        val year: Int = currentCalendar.get(Calendar.YEAR)
        val month: Int = currentCalendar.get(Calendar.MONTH) + 1
        val day: Int = currentCalendar.get(Calendar.DAY_OF_MONTH)
        viewModel.startDate.set(
            "$year/${Singleton.getTwoNumberInTwoDigit(month)}/${Singleton.getTwoNumberInTwoDigit(
                day
            )} 00:00"
        )
        viewModel.endDate.set(
            "$year/${Singleton.getTwoNumberInTwoDigit(month)}/${Singleton.getTwoNumberInTwoDigit(
                day
            )} 23:59"
        )

        Log.e("DATA", "Data: stardate: " + viewModel.startDate.get())
        Log.e("DATA", "Data: endDate: " + viewModel.endDate.get())
        viewModel.startDate.set("2021/01/11 00:00")
        viewModel.endDate.set("2021/01/14 15:40")
//        Log.e("DATA", "Data: stardate: " + viewModel.startDate.get())
//        Log.e("DATA", "Data: endDate: " + viewModel.endDate.get())

//        txtStartDateTime.text = viewModel.startDate.get()
//        txtEndDateTime.text = viewModel.endDate.get()
    }

    private fun setupRadioGroup() {
//        radioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener{
//            override fun onCheckedChanged(p0: RadioGroup?, id: Int) {
//                when(id){
//                    R.id.paypal_rb->{
//                        paymentType=Constants.ONLINE
//                    }
//                    R.id.cash_rb->{
//                        paymentType=Constants.CASH
//                    }
//                }
//                viewModel.getOrderHistoryAPI(filterType,paymentType)
//            }
//
//        })

        paypal_rb.setOnClickListener {
            if (paypal_rb.isChecked) {
                viewModel.isOnline.set(true)
                paymentType = Constants.ONLINE
                viewModel.getOrderHistoryAPI(filterType, paymentType)
            } else {
                viewModel.isOnline.set(false)
                viewModel.getOrderHistoryAPI(filterType, paymentType)
            }
        }

        cash_rb.setOnClickListener {
            if (cash_rb.isChecked) {
                viewModel.isCash.set(true)
                paymentType = Constants.CASH
                viewModel.getOrderHistoryAPI(filterType, paymentType)
            } else {
                viewModel.isCash.set(false)
                viewModel.getOrderHistoryAPI(filterType, paymentType)
            }
        }

    }

    fun Query.addSnapshotListener(
        lifecycleOwner: LifecycleOwner,
        listener: (QuerySnapshot?, FirebaseFirestoreException?) -> Unit
    ) {
        val registration = addSnapshotListener(listener)
        lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                registration.remove()
                lifecycleOwner.lifecycle.removeObserver(this)
            }
        })
    }


    fun DocumentReference.addSnapshotListener(
        owner: LifecycleOwner,
        listener: (DocumentSnapshot?, FirebaseFirestoreException?) -> Unit
    ): ListenerRegistration {
        val registration = addSnapshotListener(listener)

        owner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                registration.remove()
                owner.lifecycle.removeObserver(this)
            }

            /*
            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun onStop() {

            }
             */
        })

        return registration
    }

    private fun observeResponse() {

        viewModel.mShowProgressBar.observe(viewLifecycleOwner, Observer {
            when (it) {
                true -> showLoadingIndicator()
                false -> dismissLoadingIndicator()
            }
        })


        viewModel.courierResponseHistory.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Log.e(
                    "OrderHistoryFragment",
                    "viewModel.courierResponse called viewModel.courierList: " + viewModel.courierListHistory.size
                )
                viewModel.courierListHistory.clear()
                it.data?.result?.forEach {
                    it.userFk?.name()?.let { it1 -> viewModel.courierListHistory.add(it1) }
                    viewModel.courierListModelHistory.add(SpinnerModel(it.id, it.userFk?.name()))
                }

                viewModel.courierListHistory.add(0, "Select Courier")
                viewModel.courierListModelHistory.add(0, SpinnerModel(0, "Select Courier"))

                val aa = ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_spinner_item,
                    viewModel.courierListHistory
                )
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                courier_spn.adapter = aa
                courier_spn.setSelection(0, false)
                courier_spn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {}

                    override fun onItemSelected(
                        p0: AdapterView<*>?,
                        p1: View?,
                        position: Int,
                        p3: Long
                    ) {
                        if (position > 0) {
                            viewModel.courierIdHistory.set(viewModel.courierListModelHistory[position].id)
                            //                            Toast.makeText(
                            //                                requireActivity(),
                            //                                "Courier name: " + viewModel.courierList.get(position) + " ID: " + viewModel.courierId.get(),
                            //                                Toast.LENGTH_SHORT
                            //                            ).show()
                        } else {

                            viewModel.courierIdHistory.set(-1)
                        }
                    }

                }
            }

        })

        /*
            viewModel.courierResponse.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Log.e("OrderHistoryFragment","viewModel.courierResponse called viewModel.courierList: "+viewModel.courierList.size)
                viewModel.courierList.clear()
                it.data?.result?.forEach {
                    it.userFk?.name()?.let { it1 -> viewModel.courierList.add(it1) }
                    viewModel.courierListModel.add(SpinnerModel(it.id, it.userFk?.name()))
                }

                viewModel.courierList.add(0, "Select Courier")
                viewModel.courierListModel.add(0, SpinnerModel(0, "Select Courier"))

                val aa = ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_spinner_item,
                    viewModel.courierList
                )
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                courier_spn.adapter = aa
                courier_spn.setSelection(0, false)
                courier_spn.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {}

                    override fun onItemSelected(
                        p0: AdapterView<*>?,
                        p1: View?,
                        position: Int,
                        p3: Long
                    ) {
                        if (position > 0) {
                            viewModel.courierIdHistory.set(viewModel.courierListModel[position].id)
                            //                            Toast.makeText(
                            //                                requireActivity(),
                            //                                "Courier name: " + viewModel.courierList.get(position) + " ID: " + viewModel.courierId.get(),
                            //                                Toast.LENGTH_SHORT
                            //                            ).show()
                        } else {

                            viewModel.courierIdHistory.set(-1)
                        }
                    }

                }
            }

        })
        */

        viewModel.orderResponse.observe(viewLifecycleOwner, Observer {

            if (it != null) {
                Log.e("OrderHistoryFragment", "Order response received")
                viewModel.mShowProgressBar.value = false
                viewModel.orderItem.clear()
                viewModel.totalCount.set(0)
                viewModel.totalCount.set(it.data?.result?.totalCount)
                viewModel.totalHistoryAmount.set(it.data?.result?.ordersTotal.toString())
                it.data?.result?.items?.let { it1 -> viewModel.orderItem.addAll(it1) }
//                val filteredOrderList=it.data?.result?.items?.filter { f->f.status== status }
//                filteredOrderList?.let { it1 -> viewModel.orderItem.addAll(it1) }
                baseRecyclerAdapter?.cleatDataSet()
                baseRecyclerAdapter?.addDataSet(viewModel.orderItem)
                viewModel.orderResponse.value = null
            } else {
                Log.e("OrderHistoryFragment", "Order response is null")
                viewModel.mShowProgressBar.value = false
            }
        })
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtStartDateTime.text = viewModel.startDate.get()
        txtEndDateTime.text = viewModel.endDate.get()

        setupRadioGroup()

        tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("Today"))
            ?.let { tabLayout?.addTab(it) }
        //tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("Yesterday"))?.let { tabLayout?.addTab(it) }
        tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("ThisWeek"))
            ?.let { tabLayout?.addTab(it) }
        tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("LastWeek"))
            ?.let { tabLayout?.addTab(it) }
        tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("ThisMonth"))
            ?.let { tabLayout?.addTab(it) }
        tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("LastMonth"))
            ?.let { tabLayout?.addTab(it) }
        tabLayout?.tabGravity = TabLayout.GRAVITY_FILL
        tabLayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.text) {
                    viewModel.sharedPreference.getStringValue("Today") -> filterType =
                        Constants.TODAY
                    // viewModel.sharedPreference.getStringValue("Yesterday")->filterType=Constants.TODAY
                    viewModel.sharedPreference.getStringValue("ThisWeek") -> filterType =
                        Constants.THISWEEK
                    viewModel.sharedPreference.getStringValue("LastWeek") -> filterType =
                        Constants.LASTWEEK
                    viewModel.sharedPreference.getStringValue("ThisMonth") -> filterType =
                        Constants.THISMONTH
                    viewModel.sharedPreference.getStringValue("LastMonth") -> filterType =
                        Constants.LASTMONTH
                }

                viewModel.getOrderHistoryAPI(filterType, paymentType)

            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        cancel_history.setOnClickListener {
            iFragmentTransaction.transaction(this)
        }
        refresh_api.setOnClickListener {
            viewModel.getOrderHistoryAPI(filterType, paymentType)
        }

        calendar = GregorianCalendar()

        txtStartDateTime.setOnClickListener {
            showDateTimePicker(true)
//            showDateTimePicker(requireActivity(), true, txtStartDateTime)
        }

        txtEndDateTime.setOnClickListener {

            if (dateFrom.isEmpty()) {
                Toast.makeText(
                    requireContext(),
                    "Please select Start date first",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            showDateTimePicker(false)
//            showDateTimePicker(requireActivity(), false, txtEndDateTime)
        }


    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            iFragmentTransaction = context as IFragmentTransaction
        } catch (e: ClassCastException) {
            throw ClassCastException("$context must implement OnFragmentSelectedLister")
        }
    }

    private fun setUpRecyclerView() {
        baseRecyclerAdapter = BaseRecyclerViewAdapter(
            R.layout.history_order_list_item, BR.orderItem, ArrayList(), null,
            this
        )
        viewDataBinding!!.orderListRv.adapter = baseRecyclerAdapter
    }

    override fun onClickView(v: View?) {
        when (v?.id) {
            R.id.prev -> {
                if (viewModel.skipCount.get() != "0") {
                    viewModel.skipCount.set(
                        (viewModel.skipCount.get()?.toInt()?.minus(10)).toString()
                    )
                    viewModel.getOrderHistoryAPI(filterType, paymentType)
                } else {
                    putToast("No items in prev")
                }

            }
            R.id.next -> {
                val skipCount = viewModel.skipCount.get()?.toInt()
                if (viewModel.totalCount.get()!! > skipCount?.plus(10)!! && !viewModel.orderItem.isNullOrEmpty()) {
                    viewModel.skipCount.set(
                        (viewModel.skipCount.get()?.toInt()?.plus(10)).toString()
                    )
                    viewModel.getOrderHistoryAPI(filterType, paymentType)
                } else {
                    putToast("No more items")
                }

            }
        }
    }

    override fun goTo(clazz: Class<*>, mExtras: Bundle?) {

    }

    override fun onItemClick(view: View, position: Int, v: HistoryOrderListItemBinding) {
        if (view == v.viewTv) {
            viewModel.orderId.set(viewModel.orderItem[position].orderAddressFK?.orderId)
            requireActivity().supportFragmentManager.beginTransaction().add(
                R.id.container,
                OrderDetailsFragment()
            )
                .addToBackStack(null)
                .commit()
        }
    }

    override fun onDataBind(v: HistoryOrderListItemBinding, onClickListener: View.OnClickListener) {
        v.viewTv.setOnClickListener(onClickListener)
    }

    override fun onDetach() {
        super.onDetach()
        viewModel.isOrderhistory.set(false)
    }

    private var timeZoneMilis: Long = 0
    private var calendar: Calendar? = null
    var datePickerDialog: DatePickerDialog? = null

    var isStartDateSelected = false
    var dateFrom: String = ""
    var dateTo: String = ""

    private fun showDateTimePicker(isStart: Boolean) {

        isStartDateSelected = isStart

        val timezone: String = viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE)

        val writeDate = SimpleDateFormat(Constants.DATE_FILTER_HISTORY_FORMAT, Locale.getDefault())
        writeDate.timeZone = TimeZone.getTimeZone(timezone)

        val currentCalendar = Calendar.getInstance()
        val currentDateTimeString = writeDate.format(currentCalendar.time)
        Log.e("OrderHistoryFragment", "currentDateTimeString: $currentDateTimeString")
        Log.e("OrderHistoryFragment", "writeDate.timeZone: ${writeDate.timeZone}")

//        val timeZoneDate = SimpleDateFormat("dd.MM.yyyy, hh.mm a")
        val timeZoneDate = Constants.DATE_FILTER_HISTORY

        try {
//            val date = timeZoneDate.parse(currentDateTimeString)
//            timeZoneMilis = date.time
            timeZoneMilis = timeZoneDate.parse(currentDateTimeString).time
            Log.e("OrderHistoryFragment", "timeZoneMilis: $timeZoneMilis")
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        calendar?.timeInMillis = timeZoneMilis
        val year: Int = calendar?.get(Calendar.YEAR)!!
        val month: Int = calendar?.get(Calendar.MONTH)!!
        val day: Int = calendar?.get(Calendar.DAY_OF_MONTH)!!

        datePickerDialog = DatePickerDialog(requireContext(), this, year, month, day)

        if (isStartDateSelected) {
//            datePickerDialog!!.datePicker.minDate = timeZoneMilis - 1000
//            datePickerDialog!!.datePicker.maxDate =
//                System.currentTimeMillis() - 1000 + 1000 * 60 * 60 * 24 * 20
        } else {
            datePickerDialog!!.datePicker.minDate =
                Singleton.getCalenderFromStringDate(dateFrom).timeInMillis
        }

        datePickerDialog!!.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        calendar!![Calendar.YEAR] = year
        calendar!![Calendar.MONTH] = month
        calendar!![Calendar.DAY_OF_MONTH] = dayOfMonth

        val hour = calendar!![Calendar.HOUR]
        val minute = calendar!![Calendar.MINUTE]
        val timePickerDialog = TimePickerDialog(
            requireContext(), this, hour, minute, DateFormat.is24HourFormat(requireContext())
        )
        timePickerDialog.show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        Log.e(
            "OrderHistoryFragment",
            "onTimeSet: $hourOfDay $minute"
        )
        calendar!![Calendar.HOUR_OF_DAY] = hourOfDay
        calendar!![Calendar.MINUTE] = minute
        Log.e(
            "OrderHistoryFragment",
            "Cal HOUR: " + calendar!![Calendar.HOUR] +
                    "Cal HOUR_OF_DAY: " + calendar!![Calendar.HOUR_OF_DAY] +
                    "Cal MINUTE: " + calendar!![Calendar.MINUTE]
        )

        if (isStartDateSelected) {
            dateFrom = Singleton.getDateinYYYYMMDD(calendar!!)
            viewModel.startDate.set(dateFrom)
            txtStartDateTime.text = dateFrom
            /*if(Singleton.isDateisCurrentDate(dateFrom)){
                txtStartDateTime.text = "$hourOfDay:$minute"
            }else{
                txtStartDateTime.text = dateFrom
            }*/
        } else {
            dateTo = Singleton.getDateinYYYYMMDD(calendar!!)
            viewModel.endDate.set(dateTo)
            txtEndDateTime.text = dateTo
            /* if(Singleton.isDateisCurrentDate(dateTo)){
                 txtEndDateTime.text = "$hourOfDay:$minute"
             }else{
                 txtEndDateTime.text = dateTo
             }*/

        }

        Log.e(
            "OrderHistoryFragment",
            "dateFrom: " + dateFrom + "dateTo: " + dateTo
        )
        /*
        if (timeZoneMilis <= calendar!!.timeInMillis) {
//            viewModel.setTime(calendar.time, false)
            txtStartDateTime.text = SimpleDateFormat("MMM, dd - HH:mm", Locale.getDefault())
                .format(calendar!!.time)
                .toUpperCase()
        } else {
            Toast.makeText(requireContext(), "Invalid Time", Toast.LENGTH_LONG).show()
        }*/
    }


    private fun showDateTimePicker(
        activity: Activity,
        isFromDate: Boolean,
        edtDate: TextView
    ) {
        var datePickerDialog: DatePickerDialog? = null

        val activeDate: Calendar = Calendar.getInstance()

        val year: Int = activeDate.get(Calendar.YEAR)
        val month: Int = activeDate.get(Calendar.MONTH)
        val day: Int = activeDate.get(Calendar.DAY_OF_MONTH)

        datePickerDialog =
            DatePickerDialog(
                activity,
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->

                    if (isFromDate) {
                        Singleton.getCalander(activeDate, year, month, dayOfMonth)
                        dateFrom = Singleton.getDateinYYYYMMDD(activeDate)
                        edtDate.text = dateFrom
                    } else {
                        Singleton.getCalander(activeDate, year, month, dayOfMonth)
                        dateTo = Singleton.getDateinYYYYMMDD(activeDate)

                        if (Singleton.checkStartEndDate(
                                dateFrom,
                                dateTo
                            )
                        ) {
                            edtDate.text = dateTo
                        } else {
                            Toast.makeText(
                                activity,
                                "Please select proper date",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                }, year, month, day
            )

        if (!isFromDate) {
            datePickerDialog.datePicker.minDate =
                Singleton.getCalenderFromStringDate(dateFrom).timeInMillis
        }

        datePickerDialog.show()
    }

}
