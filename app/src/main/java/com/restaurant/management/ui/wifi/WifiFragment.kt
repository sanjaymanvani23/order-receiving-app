package com.restaurant.management.ui.wifi

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.net.wifi.WifiNetworkSpecifier
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.restaurant.management.R
import com.restaurant.management.ui.DashBoardActivity
import kotlinx.android.synthetic.main.fragment_wifi.view.*
import java.util.*


class WifiFragment : Fragment() {
    private var wifiList: ListView? = null
    private var wifiManager: WifiManager? = null
    private val MY_PERMISSIONS_ACCESS_COARSE_LOCATION = 1
    var receiverWifi: WifiReceiver? = null
    var pass: EditText? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        wifiManager =
            context?.getSystemService(Context.WIFI_SERVICE) as WifiManager?
        if (!wifiManager?.isWifiEnabled()!!) {
            Toast.makeText(context, "Turning WiFi ON...", Toast.LENGTH_LONG).show()
            wifiManager!!.isWifiEnabled = true
        }

        startScan()

    }

    private fun startScan() {
        if (ActivityCompat.checkSelfPermission(
                context as DashBoardActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            !== PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                context as DashBoardActivity,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                MY_PERMISSIONS_ACCESS_COARSE_LOCATION
            )
        } else {
            wifiManager!!.startScan()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_wifi, container,
            false)
        wifiList=view.findViewById(R.id.wifi_rcv)
        view.wifi_rcv.onItemClickListener = OnItemClickListener { parent, view, position, id -> // selected item
            val ssid = (view as TextView).text.toString()
            connectToWifi(ssid)
            Toast.makeText(context, "Wifi SSID : $ssid", Toast.LENGTH_SHORT).show()
        }
        return view
    }

    override fun onResume() {
        super.onResume()

        receiverWifi = wifiManager?.let { wifiList?.let { it1 -> WifiReceiver(it, it1) } }
        val intentFilter = IntentFilter()
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
        (context as DashBoardActivity).registerReceiver(receiverWifi, intentFilter)
        getWifi()
    }
    override fun onPause() {
        super.onPause()
        (context as DashBoardActivity).unregisterReceiver(receiverWifi)
    }
    private fun getWifi() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                Toast.makeText(context, "location turned off", Toast.LENGTH_SHORT).show()
                ActivityCompat.requestPermissions(
                    context as Activity,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                    MY_PERMISSIONS_ACCESS_COARSE_LOCATION
                )
            } else {
                Toast.makeText(context, "location turned on", Toast.LENGTH_SHORT).show()
                wifiManager!!.startScan()
            }
        } else {
            Toast.makeText(context, "scanning", Toast.LENGTH_SHORT).show()
            wifiManager!!.startScan()
        }
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MY_PERMISSIONS_ACCESS_COARSE_LOCATION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(context, "permission granted", Toast.LENGTH_SHORT).show()
                wifiManager!!.startScan()
            } else {
                Toast.makeText(context, "permission not granted", Toast.LENGTH_SHORT)
                    .show()
                return
            }
        }
    }

    private fun connectToWifi(wifiSSID: String) {
        val dialog = Dialog(context!!)
        dialog.setContentView(R.layout.fragment_wifi_connect)
        dialog.setTitle("Connect to Network")
        val textSSID = dialog.findViewById(R.id.textSSID1) as TextView
        val dialogButton: Button = dialog.findViewById(R.id.okButton) as Button
        pass = dialog.findViewById(R.id.textPassword) as EditText
        textSSID.text = wifiSSID

        // if button is clicked, connect to the network;
        dialogButton.setOnClickListener(View.OnClickListener {
            val checkPassword: String = pass!!.text.toString()
            //latestVersion("12345687",checkPassword)
            // ConnectToNetworkWPA(wifiSSID, checkPassword)
            finallyConnect(checkPassword,wifiSSID)
            dialog.dismiss()
        })
        dialog.show()
    }
    @RequiresApi(Build.VERSION_CODES.Q)
    private fun latestVersion(ddd: String, checkPassword: String) {
        val builder = WifiNetworkSpecifier.Builder()
        builder.setSsid(ddd)
        builder.setWpa2Passphrase(checkPassword)
        val wifiNetworkSpecifier = builder.build()
        var networkRequestBuilder: NetworkRequest.Builder? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            networkRequestBuilder = NetworkRequest.Builder()
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            networkRequestBuilder!!.addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            networkRequestBuilder!!.setNetworkSpecifier(wifiNetworkSpecifier)
        }
        var networkRequest: NetworkRequest? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            networkRequest = networkRequestBuilder!!.build()
        }
        val cm = context?.applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (cm != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cm.requestNetwork(networkRequest, object : NetworkCallback() {
                    override fun onAvailable(network: Network) {
                        super.onAvailable(network)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            cm.bindProcessToNetwork(network)
                        }
                    }
                })
            }
        }
    }
    private fun finallyConnect(networkPass: String, networkSSID: String) {


        val wifiConfig = WifiConfiguration()
        wifiConfig.SSID = java.lang.String.format("\"%s\"", networkSSID)
        wifiConfig.preSharedKey = java.lang.String.format("\"%s\"", networkPass)

        // remember id

        // remember id
        val netId: Int = wifiManager!!.addNetwork(wifiConfig)
        wifiManager?.disconnect()
        wifiManager!!.enableNetwork(netId, true)
        wifiManager!!.reconnect()

        val conf = WifiConfiguration()
        conf.SSID = "\"\"" + networkSSID.toString() + "\"\""
        conf.preSharedKey = "\"" + networkPass.toString() + "\""
        wifiManager!!.addNetwork(conf)
    }
    fun ConnectToNetworkWEP(
        networkSSID: String,
        password: String
    ): Boolean {
        return try {
            val conf = WifiConfiguration()
            conf.SSID =
                "\"" + networkSSID + "\"" // Please note the quotes. String should contain SSID in quotes
            conf.wepKeys[0] = "\"" + password + "\"" //Try it with quotes first
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE)
            conf.allowedGroupCiphers.set(WifiConfiguration.AuthAlgorithm.OPEN)
            conf.allowedGroupCiphers.set(WifiConfiguration.AuthAlgorithm.SHARED)
            val wifiManager = context?.applicationContext?.getSystemService(Context.WIFI_SERVICE) as WifiManager
            var networkId = wifiManager.addNetwork(conf)
            if (networkId == -1) {
                //Try it again with no quotes in case of hex password
                conf.wepKeys[0] = password
                networkId = wifiManager.addNetwork(conf)
            }
            val list =
                wifiManager.configuredNetworks
            for (i in list) {
                if (i.SSID != null && i.SSID == "\"" + networkSSID + "\"") {
                    wifiManager.disconnect()
                    wifiManager.enableNetwork(i.networkId, true)
                    wifiManager.reconnect()
                    break
                }
            }

            //WiFi Connection success, return true
            true
        } catch (ex: java.lang.Exception) {
            System.out.println(Arrays.toString(ex.stackTrace))
            false
        }
    }
    fun ConnectToNetworkWPA(
        networkSSID: String,
        password: String
    ): Boolean {
        return try {
            val conf = WifiConfiguration()
            conf.SSID =
                "\"" + networkSSID + "\"" // Please note the quotes. String should contain SSID in quotes
            conf.preSharedKey = "\"" + password + "\""
            conf.status = WifiConfiguration.Status.ENABLED
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP)
            conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP)
            conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK)
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP)
            conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP)
            Log.d("connecting", conf.SSID + " " + conf.preSharedKey)
            val wifiManager = context?.applicationContext?.getSystemService(Context.WIFI_SERVICE) as WifiManager
            wifiManager.addNetwork(conf)
            Log.d("after connecting", conf.SSID + " " + conf.preSharedKey)
            val list =
                wifiManager.configuredNetworks
            for (i in list) {
                if (i.SSID != null && i.SSID == "\"" + networkSSID + "\"") {
                    wifiManager.disconnect()
                    wifiManager.enableNetwork(i.networkId, true)
                    wifiManager.reconnect()
                    Log.d("re connecting", i.SSID + " " + conf.preSharedKey)
                    break
                }
            }


            //WiFi Connection success, return true
            true
        } catch (ex: Exception) {
            println(Arrays.toString(ex.stackTrace))
            false
        }
    }

}
