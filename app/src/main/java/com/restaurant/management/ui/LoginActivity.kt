package com.restaurant.management.ui

import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.restaurant.management.BR
import com.restaurant.management.R
import com.restaurant.management.data.local.sharedPref.ISharedPreferenceService
import com.restaurant.management.data.remote.Status
import com.restaurant.management.databinding.ActivityLoginBinding
import com.restaurant.management.ui.base.BaseActivity
import com.restaurant.management.ui.base.BaseNavigator
import com.restaurant.management.utils.Constants
import com.restaurant.management.utils.Singleton
import org.koin.android.ext.android.inject

class LoginActivity : BaseActivity<ActivityLoginBinding, CommonViewModel>(),
    BaseNavigator{
    private lateinit var viewModel: CommonViewModel
    private val sharedPreference: ISharedPreferenceService by inject()
    private var mDelayHandler: Handler? = null
    private val splashDelay: Long = 100





    private val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            if(sharedPreference.containsKey(Constants.ALREADY_LOGED_IN)) {
                if (sharedPreference.getBooleanValue(Constants.ALREADY_LOGED_IN)) {
                    sharedPreference.setBooleanValue(Constants.FIRST_TIME_DETAILS,true)
                    Singleton.navigateTo(this, DashBoardActivity::class.java, Singleton.bundle)
                    finish()
                }
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setNavigator(this)
        mDelayHandler = Handler()
        mDelayHandler!!.postDelayed(mRunnable, splashDelay)
        observeResponse()
    }

    private fun observeResponse() {
        viewModel.mShowProgressBar.observe(this, Observer {
            when (it) {
                true -> showLoadingIndicator()
                false -> dismissLoadingIndicator()
            }
        })
        viewModel.loginResponse.observe(this, Observer {
            if (it != null) {
                when (it.status) {
                    Status.LOADING -> {
                        viewModel.mShowProgressBar.value = true
                    }
                    Status.SUCCESS -> {
                        viewModel.callSettingsAPI()
                        try {
                           // Log.d("accessToken", "" + it.data!!.accessToken)
                            sharedPreference.setStringValue(
                                Constants.AUTHORIZATION_KEY,
                                "" + it.data!!.result.accessToken
                            )
                            sharedPreference.setStringValue(
                                Constants.REFRESH_TOKEN,
                                "" + it.data!!.result.refreshToken
                            )
                            sharedPreference.setStringValue(
                                Constants.USER_ID,
                                "" + it.data!!.result.userId
                            )
                            sharedPreference.setStringValue(
                                Constants.USER_NAME,
                                "" + viewModel.userName.get()
                            )
                            sharedPreference.setStringValue(
                                Constants.USER_EMAIL,
                                "" + viewModel.userName.get()
                            )
                            sharedPreference.setBooleanValue(
                                Constants.ALREADY_LOGED_IN,
                                true
                            )
                            sharedPreference.setBooleanValue(Constants.FIRST_TIME_DETAILS,true)
                        } catch (e: Exception) {
                            viewModel.mShowProgressBar.value = false
                            Toast.makeText(this, "" + e.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                    Status.ERROR, Status.FAILURE -> {
                        viewModel.mShowProgressBar.value = false
                        if (it.message.equals("invalid_username_or_password")) {
                            putToast("Please enter valid Email ID  or Password")
                        } else {
                            putToast("" + it.message)
                        }

                    }

                }
            }
        })

        viewModel.localiztationResponse.observe(this, Observer {
            if(it!=null){
                it.data?.result?.forEach {
                    viewModel.sharedPreference.setStringValue(it.key.toString(),
                        it.label.toString()
                    )
                }
                if (sharedPreference.getBooleanValue(Constants.ALREADY_LOGED_IN)) {
                    sharedPreference.setStringValue(Constants.SELECTED_ALARM,"Alarm 1")
                    goTo(DashBoardActivity::class.java, null)
                    finish()
                }
            }
        })

        viewModel.settingsResponse.observe(this, Observer {
            if (it != null) {
                when (it.status) {
                    Status.LOADING -> {
                        viewModel.mShowProgressBar.value = true
                    }
                    Status.SUCCESS -> {
                        viewModel.mShowProgressBar.value = false
                        try {
                            // Log.d("accessToken", "" + it.data!!.accessToken)
                            sharedPreference.setStringValue(
                                Constants.APP_NAME,""+it.data?.result?.name
                            )
                            sharedPreference.setStringValue(
                                Constants.APP_IMAGE,""+ it.data?.result?.applicationMedias!![0].blobLink
                            )
                            sharedPreference.setStringValue(
                                Constants.CURRENCY,""+it.data.result.currency
                            )
                            sharedPreference.setStringValue(
                                Constants.TIME_ZONE,""+it.data.result.timeZone?.name
                            )
                            sharedPreference.setStringValue(
                                Constants.STATUS,""+it.data.result.applicationMedias!![0].status
                            )
                            sharedPreference.setStringValue(
                                Constants.ADDRESS,""+it.data.result.contactDetails?.address
                            )
                            sharedPreference.setStringValue(
                                Constants.PHONE_NO,""+it.data.result.contactDetails?.phone
                            )
                            val languageNameList=ArrayList<String>()
                            val languageDisplayNameList=ArrayList<String>()
                            it.data.result.languages?.forEach {
                                it.name?.let { it1 -> languageNameList.add(it1) }
                            }
                            it.data.result.languages?.forEach {
                                it.displayName?.let { it1 -> languageDisplayNameList.add(it1) }
                            }
                            sharedPreference.setStringValue(
                                Constants.LANGUAGE_NAME_LIST,languageNameList.joinToString(separator = ",")
                            )

                            sharedPreference.setStringValue(
                                Constants.LANGUAGE_DISPLAY_NAME_LIST,languageDisplayNameList.joinToString(separator = ",")
                            )

                            sharedPreference.setBooleanValue(Constants.SHOW_CUSTOMER_DETAILS,
                                it.data.result.isShowCustomerDetails!!)

                            viewModel.languageList.clear()
                            viewModel.languageList.addAll(viewModel.sharedPreference.getStringValue(Constants.LANGUAGE_NAME_LIST).split(","))
                            viewModel.callLanguageAPI(viewModel.languageList[0])

                        } catch (e: Exception) {
                            viewModel.mShowProgressBar.value = false
                            Toast.makeText(this, "" + e.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                    Status.ERROR, Status.FAILURE -> {
                        viewModel.mShowProgressBar.value = false
                        if (it.message.equals("invalid_username_or_password")) {
                            putToast("Please enter valid Email ID  or Password")
                        } else {
                            putToast("" + it.message)
                        }

                    }

                }
            }
        })
    }

    override fun getBindingVariable(): Int {
        return BR.loginVM
    }

    override fun getViewModel(): CommonViewModel {
        viewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        return viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }

    override fun onClickView(v: View?) {
        when(v?.id){
            R.id.login_btn -> {
                viewModel.login()
            }
        }

    }

    override fun goTo(clazz: Class<*>, mExtras: Bundle?) {
        Singleton.navigateTo(
            this@LoginActivity,
            clazz,
            Singleton.bundle
        )
    }

    public override fun onDestroy() {
        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }
        super.onDestroy()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (newConfig.orientation === Configuration.ORIENTATION_LANDSCAPE) {
//            val inflater =
//                requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//            inflater.inflate(R.layout.fragment_home, null)
        } else if (newConfig.orientation === Configuration.ORIENTATION_PORTRAIT) {
//            val inflater =
//                requireActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//            inflater.inflate(R.layout.fragment_home, null)
        }
    }


}