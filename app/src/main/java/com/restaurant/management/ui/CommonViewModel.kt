package com.restaurant.management.ui

import android.app.Application
import android.view.View
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.restaurant.management.data.local.sharedPref.ISharedPreferenceService
import com.restaurant.management.data.model.SpinnerModel
import com.restaurant.management.data.model.request.GetOrderRequest
import com.restaurant.management.data.model.request.LoginRequest
import com.restaurant.management.data.model.request.UpdateOrderRequest
import com.restaurant.management.data.model.response.*
import com.restaurant.management.data.remote.BaseResponse
import com.restaurant.management.data.remote.Resource
import com.restaurant.management.data.repository.IUserControllerRespository
import com.restaurant.management.data.repository.order.IOrderControllerRespository
import com.restaurant.management.ui.base.BaseNavigator
import com.restaurant.management.ui.base.BaseViewModel
import com.restaurant.management.utils.Config
import com.restaurant.management.utils.Constants
import com.restaurant.management.utils.Singleton.timeToDate
import org.koin.core.KoinComponent
import org.koin.core.inject

class CommonViewModel (
    application: Application
) : BaseViewModel<BaseNavigator>(application), KoinComponent {

    private val userControllerRepository: IUserControllerRespository by inject()
    private val orderControllerRepository: IOrderControllerRespository by inject()
    val sharedPreference: ISharedPreferenceService by inject()
    val requestedDateTime = ObservableField("")
    val userName = ObservableField("")
    val password = ObservableField("")
    val numberOfCopy = ObservableField(1)
    val volumeCount = ObservableField(1)
    val updateDate=ObservableField("")
    val isVolumeActive = ObservableField(true)
    val isAutoPrintChecked = ObservableField(sharedPreference.getBooleanValue(Constants.AUTOMATIC_PRINT))
    val isAutoPrintOn = ObservableField(true)
    val isAutoPrintOff = ObservableField(false)
    val loginText = ObservableField(sharedPreference.getStringValue("Login"))
    val userNamePlaceholder = ObservableField(sharedPreference.getStringValue("UserNamePlaceholder"))
    val passwordPlaceholder = ObservableField(sharedPreference.getStringValue("PasswordPlaceholder"))
    val restaurantOpenDescription = ObservableField(sharedPreference.getStringValue("RestaurantOpenDescription"))
    val restaurantCloseDescription = ObservableField(sharedPreference.getStringValue("RestaurantCloseDescription"))
    val openButton = ObservableField(sharedPreference.getStringValue("OpenButton"))
    val closeButton = ObservableField(sharedPreference.getStringValue("CloseButton"))
    val yesButton = ObservableField(sharedPreference.getStringValue("yes"))
    val noButton = ObservableField(sharedPreference.getStringValue("no"))
    val cancelButton = ObservableField(sharedPreference.getStringValue("cancel"))
    val newStatus = ObservableField(sharedPreference.getStringValue("NewStatus"))
    val wIFISelection = ObservableField(sharedPreference.getStringValue("WIFISelection"))
    val orderHistory = ObservableField(sharedPreference.getStringValue("OrderHistory"))
    val help = ObservableField(sharedPreference.getStringValue("Help"))

    val PaymentMode = ObservableField(sharedPreference.getStringValue("PaymentMode"))
    val Date = ObservableField(sharedPreference.getStringValue("Date"))
    val Online = ObservableField(sharedPreference.getStringValue("Online"))
    val Today = ObservableField(sharedPreference.getStringValue("Today"))
    val ThisWeek = ObservableField(sharedPreference.getStringValue("ThisWeek"))
    val LastWeek = ObservableField(sharedPreference.getStringValue("LastWeek"))
    val ThisMonth = ObservableField(sharedPreference.getStringValue("ThisMonth"))
    val LastMonth = ObservableField(sharedPreference.getStringValue("LastMonth"))

    val AutomaticPrint = ObservableField(sharedPreference.getStringValue("AutomaticPrint"))
    val NumberOfCopies = ObservableField(sharedPreference.getStringValue("NumberOfCopies"))
    val Sound = ObservableField(sharedPreference.getStringValue("Sound"))
    val Volume = ObservableField(sharedPreference.getStringValue("Volume"))
    val Language = ObservableField(sharedPreference.getStringValue("Language"))

    val RejectReasonSelection = ObservableField(sharedPreference.getStringValue("RejectReasonSelection"))
    val RejectConfirmation = ObservableField(sharedPreference.getStringValue("RejectConfirmation"))
    val AcceptButton = ObservableField(sharedPreference.getStringValue("AcceptButton"))
    val MapButton = ObservableField(sharedPreference.getStringValue("MapButton"))
    val RejectButton = ObservableField(sharedPreference.getStringValue("RejectButton"))

    val Dispatched = ObservableField(sharedPreference.getStringValue("Dispatched"))

    val saveButton = ObservableField(sharedPreference.getStringValue("saveButtonText"))
    val courierName = ObservableField("")
    val Total = ObservableField(sharedPreference.getStringValue("Total"))
    val LoyaltyPoints = ObservableField(sharedPreference.getStringValue("LoyaltyPoints"))
    val CouponDiscount = ObservableField(sharedPreference.getStringValue("CouponDiscount"))
    val DeliveryText = ObservableField(sharedPreference.getStringValue("Delivery"))
    val TaxText = ObservableField(sharedPreference.getStringValue("Tax"))
    val SubTotalText = ObservableField(sharedPreference.getStringValue("SubTotal"))

    val Prev = ObservableField(sharedPreference.getStringValue("Prev"))

    val ViewButton = ObservableField(sharedPreference.getStringValue("ViewButton"))
    val OnTheWayStatus = ObservableField(sharedPreference.getStringValue("OnTheWayStatus"))
    val AcceptedStatus = ObservableField(sharedPreference.getStringValue("AcceptedStatus"))

    val LiveOrders = ObservableField(sharedPreference.getStringValue("LiveOrders"))
    val totalHistoryAmount = ObservableField("")
    val dataCount = ObservableField("20")
    val skipCount = ObservableField("0")
    val totalCount = ObservableField(0)
    val nextBtnText = ObservableField(sharedPreference.getStringValue("nextButtonText"))
    val cashText = ObservableField(sharedPreference.getStringValue("cash"))
    val time = ObservableField("")
    val orderTime = ObservableField("")
    val orderExpectedFullTime = ObservableField("")
    val orderId = ObservableField("")
    val orderType = ObservableField("")
    val isASAP = ObservableField(false)
    val showMap = ObservableField(false)
    val isOrderhistory = ObservableField(false)
    val isCurrentTime = ObservableField(true)
    val rejectList = ArrayList<String>()
    val courierList = ArrayList<String>()
    val courierListModel = ArrayList<SpinnerModel>()
    val rejectListModel = ArrayList<SpinnerModel>()
    val courierId = ObservableField("0")
    val currentTime = ObservableField("")
    val currentExpectedFullFillTime = ObservableField("")
    val orderDateTime = ObservableField("")
    val creationTime = ObservableField("")
    val rejectReason = ObservableField("")
    val expectedOrderFulFillDateTime = ObservableField("")
    val orderDeleveryType = ObservableField("ASAP")
    val orderDetailsPage = ObservableField(false)
    val totalAmount = ObservableField("0")
    val subTotal = ObservableField("0")
    val taxRate = ObservableField("0")
    val tax = ObservableField("0")
    val isdeleveryFree = ObservableField(false)
    val isOrderDetailsScreen = ObservableField(false)
    val delevery = ObservableField("0")
    val discount = ObservableField("")
    val loyalty = ObservableField("0")
    val loyaltypointsConvertedAmount = ObservableField("0")
    val customer_name = ObservableField("Customer Name")
    val customer_phone = ObservableField("")
    val customer_email = ObservableField("")
    val street = ObservableField("Street Name Door Number")
    val no = ObservableField("")
    val postalCode = ObservableField("Postal Code City")
    val email = ObservableField("Email")
    val notes = ObservableField("notes")
    val paymentType = ObservableField("Cash")
    val languageList=ObservableArrayList<String>()
    val languageDisplayNameList=ObservableArrayList<String>()
    val isOrderPaymentNull =ObservableField(false)
    val orderPaymentType =ObservableField("")
    val orderDeleveryTypes =ObservableField("")
    val orderPaymentStatus =ObservableField("")
    val orderDetailsItemList = ArrayList<OrderDetailsResponse.OrderItem>()
    val currency = ObservableField(sharedPreference.getStringValue(Constants.CURRENCY))
    val appImage = ObservableField(sharedPreference.getStringValue(Constants.APP_IMAGE))
    val appName = ObservableField(sharedPreference.getStringValue(Constants.APP_NAME))
    val timeZone = ObservableField(sharedPreference.getStringValue(Constants.TIME_ZONE))
    val isCheck = ObservableField(sharedPreference.getBooleanValue(Constants.IS_FIRST_TIME))
    val profileName = ObservableField(sharedPreference.getStringValue(Constants.USER_NAME))
    val mShowProgressBar = MutableLiveData<Boolean>(false)
    val loginResponse = MutableLiveData<Resource<LoginResponse>>()

    val orderExpectedFullTimeInitial = ObservableField("")
    val forgotPasswordResponse = MutableLiveData<Resource<BaseResponse<ForgotPasswordResponse>>>()
    val locationResponse = MutableLiveData<Resource<BaseResponse<LocationUpdateResponse>>>()
    val orderResponse = MutableLiveData<Resource<BaseResponse<OrderListItemResponse>>>()
    val orderHistoryResponse = MutableLiveData<Resource<BaseResponse<OrderListItemResponse>>>()
    val helpContentResponse = MutableLiveData<Resource<BaseResponse<String>>>()
    val restuarantOpenCloseResponse = MutableLiveData<Resource<BaseResponse<Boolean>>>()
    val restuarantOpenClickResponse = MutableLiveData<Resource<BaseResponse<Nothing>>>()
    val restuarantCloseClickResponse = MutableLiveData<Resource<BaseResponse<Nothing>>>()
    val settingsResponse = MutableLiveData<Resource<BaseResponse<SettingsResponse>>>()
    val orderDetailsResponse = MutableLiveData<Resource<BaseResponse<OrderDetailsResponse>>>()
    val logoutResponse = MutableLiveData<Resource<BaseResponse<Nothing>>>()
    val updateResponse = MutableLiveData<Resource<BaseResponse<Nothing>>>()
    val courierResponse = MutableLiveData<Resource<BaseResponse<List<CourierResponse>>>>()
    val rejectionResponse = MutableLiveData<Resource<BaseResponse<List<RejectReasonResponse>>>>()
    val localiztationResponse =
        MutableLiveData<Resource<BaseResponse<List<LanguageValuePairModel>>>>()
    val orderItem = ArrayList<Item>()
    val orderHistoryItem = ArrayList<OrderAddressFK>()
    val oderTypeSelected = ObservableField<Boolean>(true)

    val isCash = ObservableField(false)
    val isOnline = ObservableField(false)
    val startDate = ObservableField("00.00")
    val endDate = ObservableField("23:59")
    val courierIdHistory = ObservableField<Int>(-1)

    val courierResponseHistory = MutableLiveData<Resource<BaseResponse<List<CourierResponse>>>>()
    val courierListHistory = ArrayList<String>()
    val courierListModelHistory = ArrayList<SpinnerModel>()

    fun onClickAction(view: View?) {
        getNavigator().onClickView(view)
    }

    fun login() {
        if (validateCredentials()) {
            mShowProgressBar.value = true
            var registerData = LoginRequest(
                password.get().toString().trim(),
                userName.get().toString()
               )
            userControllerRepository.login(
                registerData,
                loginResponse
            )
        }
    }

    private fun validateCredentials(): Boolean {
        when {
            userName.get()!!.isEmpty() -> putToast("Please enter your email address")
            password.get()!!.isEmpty() -> putToast("Please enter your Password")
            else -> return true
        }
        return false
    }

  fun getOrderAPI() {
      mShowProgressBar.value = true
      val request = GetOrderRequest()
      request.isActiveOrdersOnly = false
      request.orderFilterDuration = Constants.TODAY.toInt()
      request.orderTypes = listOf("CurrentOrder", "PreOrder")
      request.paymentTypes = listOf("Online", "Cash")
      request.startDate = startDate.get()
      request.endDate = endDate.get()
      if (courierIdHistory.get() == -1) {
          request.courierId = null
      } else {
          request.courierId = courierIdHistory.get()
      }
      orderControllerRepository.GetOrder(
          Config.BRANCH_ID.toString(),
          request,
          dataCount.get().toString(),
          skipCount.get().toString(),
          orderResponse
      )
  }

    fun getOrderRefreshAPI() {
        val request = GetOrderRequest()
        request.isActiveOrdersOnly = true
        request.orderFilterDuration = Constants.TODAY.toInt()
        request.orderTypes = listOf("CurrentOrder", "PreOrder")
        request.paymentTypes = listOf("Online", "Cash")
        request.startDate = startDate.get()
        request.endDate = endDate.get()
        if (courierIdHistory.get() == -1) {
            request.courierId = null
        } else {
            request.courierId = courierIdHistory.get()
        }

        orderControllerRepository.GetOrder(
            Config.BRANCH_ID.toString(),
            request,
            dataCount.get().toString(),
            skipCount.get().toString(),
            orderResponse
        )
    }

    fun getOrderHistoryAPI(filterType:String?,paymentType:String?){
        mShowProgressBar.value=true
        val request=GetOrderRequest()
        request.isActiveOrdersOnly=false
        request.orderFilterDuration=filterType?.toInt()
        request.orderTypes= listOf("CurrentOrder", "PreOrder")

        if(paymentType==null){
            request.paymentTypes= listOf("Online", "Cash")
        } else if (isCash.get()!! && isOnline.get()!!) {
            request.paymentTypes = listOf("Online", "Cash")
        } else if (isCash.get()!! && !isOnline.get()!!) {
            request.paymentTypes = listOf("Cash")
        } else if (!isCash.get()!! && isOnline.get()!!) {
            request.paymentTypes = listOf("Online")
        } else if (!isCash.get()!! && !isOnline.get()!!) {
            request.paymentTypes = listOf("Online", "Cash")
        }

        request.startDate = startDate.get()
        request.endDate = endDate.get()

        if (courierIdHistory.get() == -1) {
            request.courierId = null
        } else {
            request.courierId = courierIdHistory.get()
        }
        orderControllerRepository.GetOrder(
            Config.BRANCH_ID.toString(),
            request,
            dataCount.get().toString(),
            skipCount.get().toString(),
            orderResponse
        )
    }

    fun getRefreshOrderHistoryAPI(filterType:String?,paymentType:String?){
        val request = GetOrderRequest()
        request.isActiveOrdersOnly = false
        request.orderFilterDuration = filterType?.toInt()
        request.orderTypes = listOf("CurrentOrder", "PreOrder")
        if (paymentType == null) {
            request.paymentTypes = listOf("Online", "Cash")
        } else {
            request.paymentTypes = listOf(paymentType)
        }

        request.startDate = startDate.get()
        request.endDate = endDate.get()
        if (courierIdHistory.get() == -1) {
            request.courierId = null
        } else {
            request.courierId = courierIdHistory.get()
        }

        orderControllerRepository.GetOrder(
            Config.BRANCH_ID.toString(),
            request,
            dataCount.get().toString(),
            skipCount.get().toString(),
            orderResponse
        )
    }

    fun restuarantOpenAPI() {
        orderControllerRepository.isRestaurantOpen(Config.BRANCH_ID.toString(),restuarantOpenCloseResponse)
    }

    fun getHelpAPi() {
        orderControllerRepository.getHelpContent(Config.BRANCH_ID.toString(),helpContentResponse)
    }

    fun openRestuarantAPI() {
        orderControllerRepository.openRestuarant(Config.BRANCH_ID.toString(),restuarantOpenClickResponse)
    }

    fun closeRestuarantAPI() {
        orderControllerRepository.closeRestuarant(Config.BRANCH_ID.toString(),restuarantCloseClickResponse)
    }

    fun callSettingsAPI() {
        orderControllerRepository.getSettings(Config.BRANCH_ID.toString(),settingsResponse)
    }

    fun orderDetailsAPI() {
      orderControllerRepository.getOrderDetails(Config.BRANCH_ID.toString(),orderId.get().toString(),orderDetailsResponse)
    }

    fun logoutAPI() {
        orderControllerRepository.logout(logoutResponse)
    }

    fun updatOrderAPI(status: String) {
       mShowProgressBar.value=true
        when(status){
            Constants.ACCEPT->{
                if(isCurrentTime.get()!!){
                    orderControllerRepository.updateOrder(Config.BRANCH_ID.toString(),orderId.get().toString(),
                        UpdateOrderRequest(expectedOrderFulFillDateTime = timeToDate(orderTime.get().toString(),updateDate.get().toString()),status =Constants.ACCEPT), updateResponse)
                }else{
                    orderControllerRepository.updateOrder(Config.BRANCH_ID.toString(),orderId.get().toString(),
                        UpdateOrderRequest(expectedOrderFulFillDateTime = timeToDate(orderExpectedFullTime.get().toString(),updateDate.get().toString()),status =Constants.ACCEPT), updateResponse)
                }

            }
            Constants.REJECTED->{
                orderControllerRepository.updateOrder(Config.BRANCH_ID.toString(),orderId.get().toString(),
                    UpdateOrderRequest(status = Constants.REJECTED), updateResponse)
            }
            Constants.DISPATCHED->{
                if(isCurrentTime.get()!!){
                    orderControllerRepository.updateOrder(Config.BRANCH_ID.toString(),orderId.get().toString(),
                        UpdateOrderRequest(expectedOrderFulFillDateTime = orderDateTime.get().toString(),status = Constants.DISPATCHED,courierId = courierId.get()?.toInt()), updateResponse)
                }else
                {
                    orderControllerRepository.updateOrder(Config.BRANCH_ID.toString(),orderId.get().toString(),
                        UpdateOrderRequest(expectedOrderFulFillDateTime = timeToDate(orderExpectedFullTime.get().toString(),updateDate.get().toString()),status = Constants.DISPATCHED,courierId = courierId.get()?.toInt()), updateResponse)
                }

            }

            Constants.ARCHIVE->{
                if(isCurrentTime.get()!!){
                    orderControllerRepository.updateOrder(Config.BRANCH_ID.toString(),orderId.get().toString(),
                        UpdateOrderRequest(expectedOrderFulFillDateTime = orderDateTime.get().toString()), updateResponse)
                }else
                {
                    orderControllerRepository.updateOrder(Config.BRANCH_ID.toString(),orderId.get().toString(),
                        UpdateOrderRequest(expectedOrderFulFillDateTime = timeToDate(orderExpectedFullTime.get().toString(),updateDate.get().toString())), updateResponse)
                }

            }

            Constants.PREORDERACCEPT->{
                orderControllerRepository.updateOrder(Config.BRANCH_ID.toString(),orderId.get().toString(),
                    UpdateOrderRequest(status =Constants.ACCEPT), updateResponse)
            }

            Constants.PREORDERREJECT->{
                orderControllerRepository.updateOrder(Config.BRANCH_ID.toString(),orderId.get().toString(),
                    UpdateOrderRequest(status =Constants.REJECTED), updateResponse)
            }

            Constants.REJECTED_REASON->{
                orderControllerRepository.updateOrder(
                    Config.BRANCH_ID.toString(), orderId.get().toString(),
                    UpdateOrderRequest(
                        status = Constants.REJECTED,
                        reasonForRejection = rejectReason.get()
                    ), updateResponse
                )
            }
        }

    }

    fun getCourier() {
        orderControllerRepository.getCourier(Config.BRANCH_ID.toString(), courierResponse)
    }

    fun getCourierHistory() {
        orderControllerRepository.getCourier(Config.BRANCH_ID.toString(), courierResponseHistory)
    }

    fun getRejectReason() {
        orderControllerRepository.getRejectReason(Config.BRANCH_ID.toString(), rejectionResponse)
    }

    public override fun onCleared() {
        super.onCleared()
    }

    fun callLanguageAPI(lang: String?) {
        orderControllerRepository.getLocalization(lang.toString(),localiztationResponse)
    }
}