package com.restaurant.management.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.restaurant.management.BR
import com.restaurant.management.R
import com.restaurant.management.data.remote.Status
import com.restaurant.management.databinding.ActivityFogotPasswordBinding
import com.restaurant.management.databinding.ActivityLoginBinding
import com.restaurant.management.ui.base.BaseActivity
import com.restaurant.management.ui.base.BaseNavigator
import com.restaurant.management.utils.Singleton

class FogotPassword : BaseActivity<ActivityFogotPasswordBinding, CommonViewModel>(),
    BaseNavigator {

    private lateinit var viewModel: CommonViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setNavigator(this)
        observeResponse()
    }

    private fun observeResponse() {

        viewModel.mShowProgressBar.observe(this, Observer {
            when(it){
                true->showLoadingIndicator()
                false->dismissLoadingIndicator()
            }
        })
        viewModel.forgotPasswordResponse.observe(this, Observer {
            if (it != null) {
                when (it.status) {
                    Status.LOADING -> {
                        viewModel.mShowProgressBar.value = true
                    }
                    Status.SUCCESS -> {
                        try {
                            viewModel.mShowProgressBar.value = false
                            if(it.data?.result!!.isSuccess!!){
                                putToast(""+it.data!!.result!!.message)
                                finish()
                            }
                        } catch (e: Exception) {
                            viewModel.mShowProgressBar.value = false
                            Toast.makeText(this, "" + e.message, Toast.LENGTH_SHORT).show()
                        }
                    }
                    Status.ERROR, Status.FAILURE -> {
                        viewModel.mShowProgressBar.value = false
                        if(it.message.equals("404")){
                            putToast("Please enter valid Email ID  or Password")
                        }else{
                            putToast(""+it.message)
                        }

                    }

                }
            }
        })
    }

    override fun getBindingVariable(): Int {
        return BR.forgotVM
    }

    override fun getViewModel(): CommonViewModel {
        viewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        return viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_fogot_password
    }

    override fun onClickView(v: View?) {
        when(v?.id){
            R.id.submit_btn->{

            }

        }

    }

    override fun goTo(clazz: Class<*>, mExtras: Bundle?) {
        Singleton.navigateTo(
            this@FogotPassword,
            clazz,
            Singleton.bundle
        )
    }
}