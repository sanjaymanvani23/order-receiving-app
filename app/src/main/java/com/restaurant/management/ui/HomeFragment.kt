package com.restaurant.management.ui

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayout
import com.google.firebase.firestore.*
import com.restaurant.management.BR
import com.restaurant.management.R
import com.restaurant.management.adapter.BaseRecyclerViewAdapter
import com.restaurant.management.adapter.OnDataBindCallback
import com.restaurant.management.data.model.response.Item
import com.restaurant.management.databinding.FragmentHomeBinding
import com.restaurant.management.databinding.OrderListItemBinding
import com.restaurant.management.ui.base.BaseFragment
import com.restaurant.management.ui.base.BaseNavigator
import com.restaurant.management.ui.order.OrderDetailsFragment
import com.restaurant.management.utils.Constants
import com.restaurant.management.utils.Singleton.getTime
import com.restaurant.management.utils.Singleton.mp
import kotlinx.android.synthetic.main.fragment_home.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class HomeFragment() :
    BaseFragment<FragmentHomeBinding, CommonViewModel>(),
    OnDataBindCallback<OrderListItemBinding>, BaseNavigator {


//    @SuppressLint("ValidFragment")
//    fun TemplateFragment() {
//    }

    private var baseRecyclerAdapter: BaseRecyclerViewAdapter<Item, OrderListItemBinding>? =
        null
    val firebaseFirestoreDB = FirebaseFirestore.getInstance()
    val documentRef =
        firebaseFirestoreDB.collection("RestaurantCollection").document("Restaurants-1")
            .collection("BranchCollection").document("Branch-1").collection("orders")

    var tabSelectedPosition = -1

    companion object {
        var status: String = "New"
    }

    override val bindingVariable: Int get() = BR.dashBoardVM
    override val layoutId: Int get() = R.layout.fragment_home
    override val viewModel: CommonViewModel
        get() = activity?.run {
            ViewModelProvider(this).get(CommonViewModel::class.java)
        } ?: throw Exception("Invalid Activity")


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        Log.e("HomeFragment", "onCreateView called")
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        status = "New"
        viewModel.setNavigator(this)
        viewModel.orderDetailsPage.set(false)

        setStartEndDate()

        documentRef.get().addOnSuccessListener { result ->
            for (document in result) {
                // Log.d("data",""+document.data)
            }
        }
        documentRef.addSnapshotListener(this) { documentSnapshot, firebaseFirestoreException ->
            viewModel.getOrderRefreshAPI()
        }

        viewModel.getOrderAPI()

        setUpRecyclerView()
        observeResponse()
        return viewDataBinding!!.root
    }

    private fun setStartEndDate() {
        val writeDate = SimpleDateFormat("yyyy/MM/dd", Locale.getDefault())
        writeDate.timeZone =
            TimeZone.getTimeZone(viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE))
        val currentCalendar = Calendar.getInstance()
        val year: Int = currentCalendar.get(Calendar.YEAR)
        val month: Int = currentCalendar.get(Calendar.MONTH) + 1
        val day: Int = currentCalendar.get(Calendar.DAY_OF_MONTH)
        viewModel.startDate.set("$year/$month/$day 00:00")
        viewModel.endDate.set("$year/$month/$day 23:58")
//        viewModel.startDate.set(
//            "$year/${Singleton.getTwoNumberInTwoDigit(month)}/${Singleton.getTwoNumberInTwoDigit(
//                day
//            )} 00:00"
//        )
//        viewModel.endDate.set(
//            "$year/${Singleton.getTwoNumberInTwoDigit(month)}/${Singleton.getTwoNumberInTwoDigit(
//                day
//            )} 23:59"
//        )
        viewModel.startDate.set("2021/01/11 00:00")
        viewModel.endDate.set("2021/01/14 15:40")
        Log.e("DATA", "Data: year: " + year)
        Log.e("DATA", "Data: month: " + month)
        Log.e("DATA", "Data: day: " + day)
        Log.e("DATA", "Data: stardate: " + viewModel.startDate.get())
        Log.e("DATA", "Data: endDate: " + viewModel.endDate.get())
    }

    private fun setUpNavigationForFirsttime() {
        viewModel.orderId.set(viewModel.orderItem[0].orderAddressFK?.orderId)
        viewModel.sharedPreference.setBooleanValue(Constants.FIRST_TIME_DETAILS, true)
        requireActivity().supportFragmentManager.beginTransaction().add(
            R.id.container,
            OrderDetailsFragment()
        )
            .addToBackStack(null)
            .commit()
    }


    fun Query.addSnapshotListener(
        lifecycleOwner: LifecycleOwner,
        listener: (QuerySnapshot?, FirebaseFirestoreException?) -> Unit
    ) {
        val registration = addSnapshotListener(listener)
        lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                registration.remove()
                lifecycleOwner.lifecycle.removeObserver(this)
            }
        })


    }


    fun DocumentReference.addSnapshotListener(
        owner: LifecycleOwner,
        listener: (DocumentSnapshot?, FirebaseFirestoreException?) -> Unit
    ): ListenerRegistration {
        val registration = addSnapshotListener(listener)

        owner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                registration.remove()
                owner.lifecycle.removeObserver(this)
            }

            /*
            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun onStop() {

            }
             */
        })

        return registration
    }

    private fun observeResponse() {

        viewModel.mShowProgressBar.observe(viewLifecycleOwner, Observer {
            when (it) {
                true -> showLoadingIndicator()
                false -> dismissLoadingIndicator()
            }
        })

        viewModel.orderResponse.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                viewModel.mShowProgressBar.value = false
                viewModel.orderItem.clear()
                viewModel.totalCount.set(0)
                viewModel.totalCount.set(it.data?.result?.totalCount)
                when (status) {
                    Constants.NEW, Constants.PreOrdered -> {
                        val filteredOrderList =
                            it.data?.result?.items?.filter { f -> f.status == status }
                        filteredOrderList?.let { it1 -> viewModel.orderItem.addAll(it1) }
                        if (filteredOrderList?.size!! > 0) {
                            //alarmListner.fireAlarm()
                            val activity = activity as DashBoardActivity
                            activity.fireAlarmNew()
                        }
                    }
                    Constants.COOKING -> {
                        val filteredOrderList =
                            it.data?.result?.items?.filter { f -> f.status == status || f.status == Constants.DeliveryRejected || f.status == Constants.DeliveryCancelled }
                        filteredOrderList?.let { it1 -> viewModel.orderItem.addAll(it1) }
                    }
                    Constants.Dispatched -> {
                        val filteredOrderList =
                            it.data?.result?.items?.filter { f -> f.status == status || f.status == Constants.DeliveryAccepted }
                        filteredOrderList?.let { it1 -> viewModel.orderItem.addAll(it1) }
                    }

                    Constants.Completed -> {
                        val filteredOrderList =
                            it.data?.result?.items?.filter { f -> f.status == status || f.status == Constants.Delivered || f.status == Constants.PaymentCollected }
                        filteredOrderList?.let { it1 -> viewModel.orderItem.addAll(it1) }
                    }
                }
                //val filteredOrderList = it.data?.result?.items?.filter { f -> f.status == status }
                //filteredOrderList?.let { it1 -> viewModel.orderItem.addAll(it1) }
                baseRecyclerAdapter?.cleatDataSet()
                baseRecyclerAdapter?.addDataSet(viewModel.orderItem)
                viewModel.orderResponse.value = null
                if (!viewModel.orderItem.isNullOrEmpty()) {
                    //   setUpNavigationForFirsttime()
                }
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.e("HomeFragment", "onViewCreated called")
        super.onViewCreated(view, savedInstanceState)

        tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("NewStatus"))
            ?.let { tabLayout?.addTab(it) }
        tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("Cooking"))
            ?.let { tabLayout?.addTab(it) }
        tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("CookingCompleted"))
            ?.let { tabLayout?.addTab(it) }
        tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("Completed"))
            ?.let { tabLayout?.addTab(it) }
        tabLayout?.newTab()?.setText(viewModel.sharedPreference.getStringValue("PreOrdered"))
            ?.let { tabLayout?.addTab(it) }
        tabLayout?.tabGravity = TabLayout.GRAVITY_FILL


        if (savedInstanceState != null && savedInstanceState.containsKey("tabSelectedPosition")) {
            tabSelectedPosition = savedInstanceState.getInt("tabSelectedPosition")
            viewModel.orderId.set(savedInstanceState.getString("orderId"))
        }

        if (tabSelectedPosition >= 0) {
//            tabLayout.selectTab(tabLayout.getTabAt(tabSelectedPosition))
        }

        tabLayout?.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                when (tab.text) {
                    viewModel.sharedPreference.getStringValue("NewStatus") -> status = Constants.NEW
                    viewModel.sharedPreference.getStringValue("Cooking") -> status =
                        Constants.COOKING
                    viewModel.sharedPreference.getStringValue("CookingCompleted") -> status =
                        Constants.Dispatched
                    viewModel.sharedPreference.getStringValue("Completed") -> status =
                        Constants.Completed
                    viewModel.sharedPreference.getStringValue("PreOrdered") -> status =
                        Constants.PreOrdered
                }
                viewModel.dataCount.set("10")
                viewModel.skipCount.set("0")
                viewModel.getOrderAPI()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        viewModel.time.set(getTime(viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE)))
        volume_iv.setOnClickListener {
            mp?.stop()
            mp?.reset()
            viewModel.isVolumeActive.set(false)
        }
    }

    private fun setUpRecyclerView() {
        baseRecyclerAdapter = BaseRecyclerViewAdapter(
            R.layout.order_list_item, BR.orderItem, ArrayList(), null,
            this
        )
        viewDataBinding!!.orderListRv.adapter = baseRecyclerAdapter

    }

    override fun onClickView(v: View?) {
        when (v?.id) {
            R.id.prev -> {
                if (viewModel.skipCount.get() != "0") {
                    viewModel.skipCount.set(
                        (viewModel.skipCount.get()?.toInt()?.minus(10)).toString()
                    )
                    viewModel.getOrderAPI()
                } else {
                    putToast("No items in prev")
                }

            }
            R.id.next -> {
                val skipCount = viewModel.skipCount.get()?.toInt()
                if (viewModel.totalCount.get()!! > skipCount?.plus(10)!! && !viewModel.orderItem.isNullOrEmpty()) {
                    viewModel.skipCount.set(
                        (viewModel.skipCount.get()?.toInt()?.plus(10)).toString()
                    )
                    viewModel.getOrderAPI()
                } else {
                    putToast("No more items")
                }

            }
        }
    }

    override fun goTo(clazz: Class<*>, mExtras: Bundle?) {

    }

    override fun onItemClick(view: View, position: Int, v: OrderListItemBinding) {
        if (view == v.viewTv) {
            viewModel.orderId.set(viewModel.orderItem[position].orderAddressFK?.orderId)
            requireActivity().supportFragmentManager.beginTransaction().add(
                R.id.container,
                OrderDetailsFragment()
            )
                .addToBackStack(null)
                .commit()
        }
    }

    override fun onDataBind(v: OrderListItemBinding, onClickListener: View.OnClickListener) {
        v.viewTv.setOnClickListener(onClickListener)

    }

    interface FireAlarmListner {
        fun fireAlarm()
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        Log.e("HomeFragment", "onConfigurationChanged called")
        super.onConfigurationChanged(newConfig)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.e("HomeFragment", "onSaveInstanceState called")
        outState.putString("TAB", status)
        tabSelectedPosition = tabLayout.selectedTabPosition
        outState.putInt("tabSelectedPosition", tabSelectedPosition)
        outState.putString("orderId", viewModel.orderId.get())
    }
}