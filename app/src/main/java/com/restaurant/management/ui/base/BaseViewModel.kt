package com.restaurant.management.ui.base

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import com.restaurant.management.utils.NetworkUtils


abstract class BaseViewModel<N>(application: Application) : AndroidViewModel(application) {

    private val mApplication: Application = application
    private var mNavigator: N? = null

    fun isNetworkConnected(): Boolean {
        val network = NetworkUtils.isNetworkConnected(mApplication)
        if (!network) putToast("No Network, Please check internet connection.")
        return network
    }

     fun putToast(msg: String) {
         Toast.makeText(mApplication,""+msg,Toast.LENGTH_SHORT).show()
    }

    fun getNavigator(): N {
        return mNavigator!!
    }

    fun setNavigator(navigator: N) {
        this.mNavigator = navigator
    }

    override fun onCleared() {
        super.onCleared()
    }
}