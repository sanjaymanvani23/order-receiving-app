package com.restaurant.management.ui

import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import com.restaurant.management.BR
import com.restaurant.management.R
import com.restaurant.management.data.local.sharedPref.ISharedPreferenceService
import com.restaurant.management.data.model.sidenav.SideNavItem
import com.restaurant.management.databinding.ActivityDashboardBinding
import com.restaurant.management.ui.base.BaseActivity
import com.restaurant.management.ui.base.BaseNavigator
import com.restaurant.management.ui.help.HelpFragment
import com.restaurant.management.ui.history.OrderHistoryFragment
import com.restaurant.management.ui.settings.SettingsFragment
import com.restaurant.management.ui.wifi.WifiFragment
import com.restaurant.management.utils.Constants
import com.restaurant.management.utils.Singleton
import com.restaurant.management.utils.Singleton.currencyFormat
import com.restaurant.management.utils.Singleton.mp
import com.restaurant.management.utils.Singleton.viewBtn
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.restaurant_close_top.view.*
import kotlinx.android.synthetic.main.restaurant_open_top.view.*
import org.koin.android.ext.android.inject


class DashBoardActivity : BaseActivity<ActivityDashboardBinding, CommonViewModel>(),
    IFragmentTransaction,
    BaseNavigator {
    private lateinit var viewModel: CommonViewModel
    private val sharedPreference: ISharedPreferenceService by inject()
    private val MY_PERMISSIONS_REQUEST_READ_FINE_LOCATION = 100
    private var doubleBackToExitPressedOnce = false
    val firebaseFirestoreDB = FirebaseFirestore.getInstance()
    val localOrderIdList = ArrayList<String>()
    val monitoredStatus = ArrayList<String>()
    var isNewDoumentAdded = false
    private var mIntentFilter: IntentFilter? = null
    val documentRef =
        firebaseFirestoreDB.collection("RestaurantCollection").document("Restaurants-1")
            .collection("BranchCollection").document("Branch-1").collection("orders")

    var orientationChanged = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.e("DashBoardActivity", "onCreate called")


        viewModel.setNavigator(this)
        im_hamburger?.setOnClickListener {
            drawer_layout.openDrawer(GravityCompat.START)
        }
        viewModel.restuarantOpenAPI()
        viewModel.getHelpAPi()
        addIntentFiltersForReceiver()
        viewBtn = sharedPreference.getStringValue("ViewButton")


        if (savedInstanceState != null && savedInstanceState.containsKey("orientationChanged")) {
            orientationChanged = savedInstanceState.getBoolean("orientationChanged")
            Log.e("DashBoardActivity", "onCreate called: orientationChanged: $orientationChanged")
        }

        if (!orientationChanged) {
            supportFragmentManager.beginTransaction().add(
                R.id.container,
                HomeFragment()
            ).commit()
        }
        obseveResponse()
        setupRestuarantOpenCloseclick()
        currencyFormat = sharedPreference.getStringValue(Constants.CURRENCY)
        documentRef.get().addOnSuccessListener { result ->
            for (document in result) {
                // Log.d("data",""+document.data)
            }
        }

        monitoredStatus.add("New")
        monitoredStatus.add("PreOrdered")
    }

    private fun startAlarm() {
        Log.d("myalarm", "" + viewModel.sharedPreference.getStringValue(Constants.SELECTED_ALARM))
        when (viewModel.sharedPreference.getStringValue(Constants.SELECTED_ALARM)) {
            "Alarm 1" -> {
                testRingTone(R.raw.alarm_1)
            }
            "Alarm 2" -> {
                testRingTone(R.raw.alarm_2)
            }
            "Alarm 3" -> {
                testRingTone(R.raw.alarm_3)
            }
            "Alarm 4" -> {
                testRingTone(R.raw.alarm_4)
            }
            "Alarm 5" -> {
                testRingTone(R.raw.alarm_5)
            }
            "Alarm 6" -> {
                testRingTone(R.raw.alarm_6)
            }
            "Alarm 7" -> {
                testRingTone(R.raw.alarm_7)
            }
            "Alarm 8" -> {
                testRingTone(R.raw.alarm_8)
            }
            "Alarm 9" -> {
                testRingTone(R.raw.alarm_9)
            }
            "Alarm 10" -> {
                testRingTone(R.raw.alarm_10)
            }
            "Alarm 11" -> {
                testRingTone(R.raw.alarm_11)
            }
            "Alarm 12" -> {
                testRingTone(R.raw.alarm_12)
            }
            "Alarm 13" -> {
                testRingTone(R.raw.alarm_13)
            }
            "Alarm 14" -> {
                testRingTone(R.raw.alarm_14)
            }
            "Alarm 15" -> {
                testRingTone(R.raw.alarm_15)
            }
            "Alarm 16" -> {
                testRingTone(R.raw.alarm_16)
            }
            "FC Basel 1" -> {
                testRingTone(R.raw.fc_basel_1)
            }
            "FC Basel 2" -> {
                testRingTone(R.raw.fc_basel_2)
            }
        }
    }

    private fun testRingTone(res: Int) {
        mp = MediaPlayer.create(this, res)
        mp?.setOnPreparedListener {
            mp?.start()
        }
        mp?.setOnCompletionListener {
            it.start()
        }
    }

    private fun setupRestuarantOpenCloseclick() {

        header2.open_tv.setOnClickListener {
            showOpenPopup()
        }

        header3.close_tv.setOnClickListener {
            showClosePopup()
        }
    }

    fun showOpenPopup() {
        val li = LayoutInflater.from(this)
        val promptsView: View = li.inflate(R.layout.restaurant_open_popup, null)
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setView(promptsView)
        alertDialogBuilder.setCancelable(true)
        val alertDialog: AlertDialog = alertDialogBuilder.create()
        val title = promptsView.findViewById(R.id.dialog_title) as AppCompatTextView
        title.text = viewModel.restaurantOpenDescription.get()
        val yesButton = promptsView.findViewById(R.id.yes_btn) as Button
        yesButton.text = viewModel.yesButton.get()
        yesButton.setOnClickListener {
            viewModel.openRestuarantAPI()
            alertDialog.dismiss()
        }
        val dialogButton: Button = promptsView.findViewById(R.id.cancel_button) as Button
        dialogButton.text = viewModel.cancelButton.get()
        dialogButton.setOnClickListener({ alertDialog.dismiss() })
        alertDialog.show()
    }

    fun showClosePopup() {
        val li = LayoutInflater.from(this)
        val promptsView: View = li.inflate(R.layout.restaurant_close_popup, null)
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setView(promptsView)
        alertDialogBuilder.setCancelable(true)
        val alertDialog: AlertDialog = alertDialogBuilder.create()
        val title = promptsView.findViewById(R.id.dialog_title) as AppCompatTextView
        title.text = viewModel.restaurantCloseDescription.get()
        val yesButton = promptsView.findViewById(R.id.yes_btn) as Button
        yesButton.text = viewModel.yesButton.get()
        yesButton.setOnClickListener {
            viewModel.closeRestuarantAPI()
            alertDialog.dismiss()
        }
        val dialogButton: Button = promptsView.findViewById(R.id.cancel_button) as Button
        dialogButton.text = viewModel.cancelButton.get()
        dialogButton.setOnClickListener({ alertDialog.dismiss() })
        alertDialog.show()
    }

    private fun obseveResponse() {

        viewModel.helpContentResponse.observe(this, Observer {
            if (it != null) {
                it.data?.result?.let {
                    Singleton.helpContent = it
                }
            }
        })

        viewModel.restuarantOpenCloseResponse.observe(this, Observer {
            if (it != null) {
                it.data?.result?.let {
                    if (it) {
                        header2.visibility = View.GONE
                        header3.visibility = View.VISIBLE
                    } else {
                        header2.visibility = View.VISIBLE
                        header3.visibility = View.GONE
                    }
                }
            }
        })

        viewModel.restuarantOpenClickResponse.observe(this, Observer {
            if (it != null) {
                if (it.data?.success!!) {

                    header2.visibility = View.GONE
                    header3.visibility = View.VISIBLE
                }
            }
        })

        viewModel.restuarantCloseClickResponse.observe(this, Observer {
            if (it != null) {
                if (it.data?.success!!) {
                    header2.visibility = View.VISIBLE
                    header3.visibility = View.GONE
                }
            }
        })
    }

    override fun getBindingVariable(): Int {
        return BR.dashBoardVM
    }

    override fun getViewModel(): CommonViewModel {
        viewModel = ViewModelProviders.of(this).get(CommonViewModel::class.java)
        return viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_dashboard
    }

    fun closeDrawer() {
        drawer_layout?.closeDrawer(GravityCompat.START)
    }

    fun closeDrawer(item: SideNavItem) {
        drawer_layout?.closeDrawer(GravityCompat.START)
        when (item.id) {
            1 -> {
                viewModel.orderDetailsPage.set(false)
                supportFragmentManager.beginTransaction().replace(
                    R.id.container,
                    HomeFragment()
                ).commit()
            }
            2 -> {
                //rateUs()
                viewModel.orderDetailsPage.set(true)
                supportFragmentManager.beginTransaction().replace(
                    R.id.container,
                    WifiFragment()
                ).commit()
            }
            3 -> {
                viewModel.orderDetailsPage.set(false)
                supportFragmentManager.beginTransaction().replace(
                    R.id.container,
                    OrderHistoryFragment()
                ).commit()
            }

            4 -> {
                viewModel.orderDetailsPage.set(true)
                supportFragmentManager.beginTransaction().replace(
                    R.id.container,
                    SettingsFragment()
                ).commit()
            }

            5 -> {
                viewModel.orderDetailsPage.set(false)
                supportFragmentManager.beginTransaction().replace(
                    R.id.container,
                    HelpFragment()
                ).commit()
            }

            6 -> {
                AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Log out?")
                    .setMessage("Are you sure you want to Log out?")
                    .setPositiveButton("Yes") { _, _ ->
                        //sharedPreference.clearAllValues()
                        sharedPreference.setBooleanValue(Constants.ALREADY_LOGED_IN, false)
                        sharedPreference.setStringValue(Constants.REFRESH_TOKEN, "")
                        sharedPreference.setStringValue(Constants.AUTHORIZATION_KEY, "")
                        viewModel.logoutAPI()
                        Singleton.navigateWithClearTop(
                            this,
                            LoginActivity::class.java,
                            Singleton.bundle
                        )
                    }
                    .setNegativeButton("No", null)
                    .show()
            }

        }
    }

    override fun onClickView(v: View?) {
        when (v?.id) {

        }
    }

    override fun goTo(clazz: Class<*>, mExtras: Bundle?) {
        Singleton.navigateTo(
            this@DashBoardActivity,
            clazz,
            Singleton.bundle
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_READ_FINE_LOCATION -> {
                if (grantResults.size > 0
                    && grantResults[0] === PackageManager.PERMISSION_GRANTED
                ) {
                    // permission was granted
                } else {
                    // permission was denied
                }
                return
            }
        }
    }

    override fun onBackPressed() {
        if (Singleton.isOrdeDetailsScreen) {
            super.onBackPressed()
        }
    }

    override fun transaction(fragment: Fragment) {
        val fm: FragmentManager = this.getSupportFragmentManager()
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        supportFragmentManager.beginTransaction().replace(
            R.id.container,
            HomeFragment()
        ).commit()

    }

    private fun addIntentFiltersForReceiver() {
        mIntentFilter = IntentFilter()
        mIntentFilter!!.addAction(Constants.LOGOUT)
    }

    override fun onResume() {
        super.onResume()
        Log.e("DashBoardActivity", "onResume called")
        registerReceiver(logoutReciever, mIntentFilter)
    }

    internal var logoutReciever: BroadcastReceiver? = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                Constants.LOGOUT -> {
                    sharedPreference.clearAllValues()
                    Singleton.navigateWithClearTop(
                        context,
                        LoginActivity::class.java,
                        Singleton.bundle
                    )
                }
            }

        }

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("DashBoardActivity", "onDestroy called")
        unregisterReceiver(logoutReciever)
    }

    fun fireAlarmNew() {
        documentRef.whereIn("Status", monitoredStatus)
            .addSnapshotListener { snapshots, e ->
                if (e != null) {
                    return@addSnapshotListener
                }
                for (dc in snapshots!!.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            if (!localOrderIdList.contains(
                                    dc.document.data.get("OrderId").toString()
                                )
                            ) {
                                isNewDoumentAdded = true
                                localOrderIdList.add(dc.document.data.get("OrderId").toString())
                            }
                            Log.d("TAG", "New city: ${dc.document.data}")
                        }
                        DocumentChange.Type.MODIFIED -> Log.d(
                            "TAG",
                            "Modified city: ${dc.document.data.get("OrderId")}"
                        )
                        DocumentChange.Type.REMOVED -> {
                            if (localOrderIdList.contains(
                                    dc.document.data.get("OrderId").toString()
                                )
                            ) {
                                localOrderIdList.remove(dc.document.data.get("OrderId").toString())
                            }
                            Log.d("TAG", "Removed city: ${dc.document.data}")
                        }
                    }
                }

                if (snapshots.documentChanges.size > 0) {
                    localOrderIdList.forEach {
                        Log.d("itemOrderId", it)
                    }
                    if (isNewDoumentAdded) {
                        isNewDoumentAdded = false
                        viewModel.isVolumeActive.set(true)
                        startAlarm()
                        /*   val mTimer = Timer()
                      //     mp = MediaPlayer.create(this, R.raw.analog_watch)
                           mTimer.scheduleAtFixedRate(object : TimerTask() {
                               override fun run() {
                                   mp?.start()
                               }
                           }, 1000 * 1, 1000 * 1)*/

                        isNewDoumentAdded = false
                    } else {
                        /*  if(mp!=null && mp?.isPlaying!!){
                              mp?.stop()
                          }*/
                    }
                }
            }
    }

/*
    override fun fireAlarm() {
        documentRef.whereIn("Status", monitoredStatus)
            .addSnapshotListener { snapshots, e ->
                if (e != null) {
                    return@addSnapshotListener
                }
                for (dc in snapshots!!.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            if (!localOrderIdList.contains(
                                    dc.document.data.get("OrderId").toString()
                                )
                            ) {
                                isNewDoumentAdded = true
                                localOrderIdList.add(dc.document.data.get("OrderId").toString())
                            }
                            Log.d("TAG", "New city: ${dc.document.data}")
                        }
                        DocumentChange.Type.MODIFIED -> Log.d(
                            "TAG",
                            "Modified city: ${dc.document.data.get("OrderId")}"
                        )
                        DocumentChange.Type.REMOVED -> {
                            if (localOrderIdList.contains(
                                    dc.document.data.get("OrderId").toString()
                                )
                            ) {
                                localOrderIdList.remove(dc.document.data.get("OrderId").toString())
                            }
                            Log.d("TAG", "Removed city: ${dc.document.data}")
                        }
                    }
                }

                if (snapshots.documentChanges.size > 0) {
                    localOrderIdList.forEach {
                        Log.d("itemOrderId", it)
                    }
                    if (isNewDoumentAdded) {
                        isNewDoumentAdded = false
                        viewModel.isVolumeActive.set(true)
                        startAlarm()
                       *//*   val mTimer = Timer()
                     //     mp = MediaPlayer.create(this, R.raw.analog_watch)
                          mTimer.scheduleAtFixedRate(object : TimerTask() {
                              override fun run() {
                                  mp?.start()
                              }
                          }, 1000 * 1, 1000 * 1)*//*

                        isNewDoumentAdded = false
                    } else {
                        *//*  if(mp!=null && mp?.isPlaying!!){
                              mp?.stop()
                          }*//*
                    }
                }
            }
    }*/

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        Log.e("DashBoardActivity", "onConfigurationChanged called")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("orientationChanged", true)
        Log.e(
            "DashBoardActivity",
            "onSaveInstanceState called: orientationChanged: " + outState.getBoolean("orientationChanged")
        )

    }


}