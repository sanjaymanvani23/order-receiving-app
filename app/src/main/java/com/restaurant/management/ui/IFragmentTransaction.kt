package com.restaurant.management.ui

import androidx.fragment.app.Fragment

interface IFragmentTransaction {
    fun transaction(fragment: Fragment)
}