package com.restaurant.management.ui.order

import android.Manifest
import android.app.AlertDialog
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatSpinner
import androidx.core.app.ActivityCompat
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.google.android.gms.appindexing.Action
import com.google.android.gms.appindexing.AppIndex
import com.google.android.gms.appindexing.Thing
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.firebase.firestore.*
import com.iposprinter.iposprinterservice.IPosPrinterCallback
import com.iposprinter.iposprinterservice.IPosPrinterService
import com.restaurant.management.BR
import com.restaurant.management.R
import com.restaurant.management.adapter.BaseRecyclerViewAdapter
import com.restaurant.management.adapter.OnDataBindCallback
import com.restaurant.management.data.model.SpinnerModel
import com.restaurant.management.data.model.response.OrderDetailsResponse
import com.restaurant.management.databinding.FragmentOrderDetailsBinding
import com.restaurant.management.databinding.OrderDetailsListItemBinding
import com.restaurant.management.ui.CommonViewModel
import com.restaurant.management.ui.HomeFragment
import com.restaurant.management.ui.IFragmentTransaction
import com.restaurant.management.ui.base.BaseFragment
import com.restaurant.management.ui.base.BaseNavigator
import com.restaurant.management.ui.order.map.HRMarkerAnimation
import com.restaurant.management.ui.order.map.UpdateLocationCallBack
import com.restaurant.management.ui.printing.IPosPrinterTestDemo
import com.restaurant.management.ui.printing.MemInfo
import com.restaurant.management.ui.printing.ThreadPoolManager
import com.restaurant.management.utils.Constants
import com.restaurant.management.utils.DataParser
import com.restaurant.management.utils.Singleton
import com.restaurant.management.utils.Singleton.dateFormat
import com.restaurant.management.utils.Singleton.isOrdeDetailsScreen
import com.restaurant.management.utils.Singleton.isTimeAdded
import com.restaurant.management.utils.pos.HandlerUtils.IHandlerIntent
import com.restaurant.management.utils.pos.HandlerUtils.MyHandler
import kotlinx.android.synthetic.main.fragment_order_details.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class OrderDetailsFragment : BaseFragment<FragmentOrderDetailsBinding, CommonViewModel>(),
    BaseNavigator, OnDataBindCallback<OrderDetailsListItemBinding>, OnMapReadyCallback {

    //POS
    private val TAG = "OrderDetailsFragment"
    private lateinit var iFragmentTransaction: IFragmentTransaction
    private val PRINTER_NORMAL = 0
    private var mIPosPrinterService: IPosPrinterService? = null
    private var callback: IPosPrinterCallback? = null
    private val random = Random()
    var lato: LatLng? = null
    var latd: LatLng? = null
    var marker: Marker? = null
    private var locationManager: LocationManager? = null
    private var client: GoogleApiClient? = null

    private var handler: MyHandler? = null
    private var index = 0
    private var next = 0
    private var startPosition: LatLng? = null
    private var endPosition: LatLng? = null
    private var isAccepted = false
    //private var isNumberOfCopies="0"

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private var printerStatus = 0
    private var loopPrintFlag = DEFAULT_LOOP_PRINT


    private var baseRecyclerAdapter: BaseRecyclerViewAdapter<OrderDetailsResponse.OrderItem, OrderDetailsListItemBinding>? =
        null
    private var orderAccepterLocation: LatLng? = null
    private var orderAddressLatLng: LatLng? = null
    private var mMap: GoogleMap? = null
    private var LastKnowLocation: Location? = null
    private var oldLocation: Location? = null
    private var toLocation: LatLng? = null
    private var startLat: Double = 0.0
    private var startLang: Double = 0.0
    private var endLat: Double = 0.0
    private var endLang: Double = 0.0
    private lateinit var mLocationRequest: LocationRequest

    // globally declare LocationCallback
    private lateinit var locationCallback: LocationCallback
    var origin: MarkerOptions? = null
    var destination: MarkerOptions? = null
    var cal: Calendar? = null
    var cal2: Calendar? = null
    var currentTime: Unit? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    val df = SimpleDateFormat("HH:mm")

    val firebaseFirestoreDB = FirebaseFirestore.getInstance()
    val documentRef =
        firebaseFirestoreDB.collection("RestaurantCollection").document("Restaurants-1")
            .collection("BranchCollection").document("Branch-1").collection("orders")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState)
        TimeZone.setDefault(TimeZone.getTimeZone(viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE)))
        cal = Calendar.getInstance()
        cal2 = Calendar.getInstance()
        isOrdeDetailsScreen = true
        val timeZone =
            TimeZone.getTimeZone(viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE))
        df.timeZone = timeZone
        val formattera = SimpleDateFormat("dd.MM.yyyy")
        currentTime = cal?.add(Calendar.MINUTE, 30)!!
        viewModel.currentTime.set(df.format(cal?.time))
        viewModel.updateDate.set(formattera.format(cal?.time))
        viewModel.setNavigator(this)
        viewModel.orderDetailsPage.set(true)
        viewModel.orderDetailsAPI()
        viewModel.getCourier()
        viewModel.getRejectReason()
        setUpRecyclerView()
        observeResponse()
        setUpPOSPrinter()
        createLocationRequest()
        doReadyPrintingText()
        return viewDataBinding!!.root
    }

    fun createLocationRequest() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
        mLocationRequest = LocationRequest.create()
        mLocationRequest.interval = 1000 * 5
        mLocationRequest.fastestInterval = 1000 * 3
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.smallestDisplacement = 170f // 170 m = 0.1 mile
        mLocationRequest.priority =
            LocationRequest.PRIORITY_HIGH_ACCURACY //set according to your app function
//         locationCallback = object : LocationCallback() {
//             override fun onLocationResult(locationResult: LocationResult?) {
//                 locationResult ?: return
//                 if (locationResult.locations.isNotEmpty()) {
//                     LastKnowLocation= locationResult.lastLocation
//                     if (LastKnowLocation != null && LastKnowLocation!!.longitude != 0.0
//                         && LastKnowLocation!!.longitude != 0.0) {
//                         if (mMap != null) {
//                             if(mMap!=null){
//                                 addMarker(mMap!!)
//                             }
//                             //getAnimationMarker(marker!!,mMap,marker!!.position,endPosition!!)
//                         }
//                     }
//                 }
//             }
//         }
        //getLastLocation()
    }

    fun addMarker(
        googleMap: GoogleMap,
        it: LatLng
    ) {
        try {
            val LastKnowLocation = Location("")
            LastKnowLocation.latitude = it.latitude
            LastKnowLocation.longitude = it.longitude
            if (oldLocation == null) {
                oldLocation = LastKnowLocation
            }
            HRMarkerAnimation(googleMap, 1000,
                UpdateLocationCallBack { updatedLocation ->
                    oldLocation = updatedLocation
                }).animateMarker(LastKnowLocation, oldLocation, marker)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    private fun startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mFusedLocationClient?.requestLocationUpdates(
            mLocationRequest,
            locationCallback,
            null /* Looper */
        )
    }

    private fun stopLocationUpdates() {
        mFusedLocationClient?.removeLocationUpdates(locationCallback)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        documentRef.whereEqualTo("Status", "DeliveryAccepted")
//            .get().addOnSuccessListener { result ->
//                for (document in result) {
//                    val geoPoint = document.getGeoPoint("Location")
//                    orderAccepterLocation =
//                        geoPoint?.latitude?.let { LatLng(it, geoPoint.longitude) }
//
//                }
//            }
        documentRef.addSnapshotListener(this) { documentSnapshot, firebaseFirestoreException ->
            documentRef.whereEqualTo("Status", "DeliveryAccepted")
                .whereEqualTo("OrderId", viewModel.orderId.get())
                .get().addOnSuccessListener { result ->
                    for (document in result) {
                        val geoPoint = document.getGeoPoint("Location")
                        orderAccepterLocation =
                            geoPoint?.latitude?.let { LatLng(it, geoPoint.longitude) }
                        if (mMap != null) {
                            orderAccepterLocation?.let {
                                addMarker(mMap!!, it)
                            }
                        }
                        println("locationcheck" + geoPoint?.latitude + "," + geoPoint?.longitude)
                        //putToast("" + geoPoint?.latitude + "," + geoPoint?.longitude)
                    }
                }
        }

        close_iv.setOnClickListener {
            getActivity()?.getSupportFragmentManager()?.popBackStack()
        }

        //setUpTime()
        plus_btn.setOnClickListener {
            if (viewModel.isCurrentTime.get()!!) {
                setupTimeer(10)
            } else {
                setupTimeerCalenderTwo(10)
            }
            minus_btn.isEnabled = true
        }

        minus_btn.setOnClickListener {
            if (!viewModel.isCurrentTime.get()!!) {
                val date1 = df.parse(viewModel.orderExpectedFullTimeInitial.get())
                val date2 = df.parse(viewModel.orderExpectedFullTime.get())
                Log.d("date", "1 " + date1)
                Log.d("date", "2 " + date1)
                val mills = date1.time - date2.time
                val hours: Long = mills / (1000 * 60 * 60)
                var mins = (mills / (1000 * 60) % 60).toInt()
                val diff = "$hours:$mins"
                if (hours.toInt() != 0) {
                    mins += (hours * 60).toInt()
                }
                if (mins < 0) {
                    if (mins >= -10) {
                        cal2?.add(Calendar.MINUTE, mins)
                        minus_btn.isEnabled = false
                    } else {
                        cal2?.add(Calendar.MINUTE, -10)
                    }
                    viewModel.orderExpectedFullTime.set(df.format(cal2?.time))
                }
                Log.d("maontimecheck99", diff)
            } else {
                val date1 = SimpleDateFormat(
                    "HH:mm",
                    Locale.getDefault()
                ).parse(viewModel.currentTime.get())
                val date2 =
                    SimpleDateFormat("HH:mm", Locale.getDefault()).parse(viewModel.orderTime.get())
                Log.d("date", "1 " + date1)
                Log.d("date", "2 " + date2)
//            val date2 = SimpleDateFormat("HH:mm").parse(viewModel.currentExpectedFullFillTime.get())
                val mills = date1.time - date2.time
                val hours: Long = mills / (1000 * 60 * 60)
                var mins = (mills / (1000 * 60) % 60).toInt()
                val diff = "$hours:$mins"
                if (hours.toInt() != 0) {
                    mins += (hours * 60).toInt()
                }
                if (mins != 0) {
                    if (mins >= -10) {
                        cal?.add(Calendar.MINUTE, mins)
                        minus_btn.isEnabled = false
                    } else {
                        cal?.add(Calendar.MINUTE, -10)
                    }
                    viewModel.orderTime.set(df.format(cal?.getTime()))
                }
                Log.d("maontimecheck", diff)
            }
        }

        mins15_tv.setOnClickListener {
            if (viewModel.isCurrentTime.get()!!) {
                val bol = setupTimeer(15)
                mins15_tv.isEnabled = !bol
            } else {
                val bol = setupTimeerCalenderTwo(15)
                mins15_tv.isEnabled = !bol
            }
            //mins15_tv.isEnabled=false
            minus_btn.isEnabled = true
        }

        mins30_tv.setOnClickListener {
            if (viewModel.isCurrentTime.get()!!) {
                val bol = setupTimeer(30)
                mins30_tv.isEnabled = !bol
            } else {
                val bol = setupTimeerCalenderTwo(30)
                mins30_tv.isEnabled = !bol
            }

            minus_btn.isEnabled = true
        }

        mins45_tv.setOnClickListener {
            if (viewModel.isCurrentTime.get()!!) {
                val bol = setupTimeer(45)
                mins45_tv.isEnabled = !bol
            } else {
                val bol = setupTimeerCalenderTwo(45)
                mins45_tv.isEnabled = !bol
            }

            minus_btn.isEnabled = true
        }

        mins60_tv.setOnClickListener {
            if (viewModel.isCurrentTime.get()!!) {
                val bol = setupTimeer(60)
                mins60_tv.isEnabled = !bol
            } else {
                val bol = setupTimeerCalenderTwo(60)
                mins60_tv.isEnabled = !bol
            }
            minus_btn.isEnabled = true
        }

        reject_btn.setOnClickListener {
            // shoRejectPopup()
            shoRejectPopup()

        }


        accept_btn.setOnClickListener {
            //checking
            if (viewModel.sharedPreference.getBooleanValue(Constants.AUTOMATIC_PRINT)) {
                isAccepted = true
            }

            viewModel.updatOrderAPI(Constants.ACCEPT)
            Singleton.mp?.stop()

//            for (i in 1..viewModel.sharedPreference.getIntegerValue(Constants.NUM_OF_COPIES)) {
//                if (getPrinterStatus() == PRINTER_NORMAL) {
//                    printText()
//                }
//            }


        }

        history_map_btn.setOnClickListener {
            setupMap()
        }


        archive_btn.setOnClickListener {
            viewModel.updatOrderAPI(Constants.ARCHIVE)
        }

        dispatch_btn.setOnClickListener {
            shoCourierPopup()
        }

        pre_accept_btn.setOnClickListener {
            if (viewModel.sharedPreference.getBooleanValue(Constants.AUTOMATIC_PRINT)) {
                isAccepted = true
            }
            viewModel.updatOrderAPI(Constants.PREORDERACCEPT)
        }

        pre_reject_btn.setOnClickListener {
            viewModel.updatOrderAPI(Constants.PREORDERREJECT)
        }

        pre_map_btn.setOnClickListener {
            setupMap()
        }
        del_map_btn.setOnClickListener {
            setupMap()
        }
        map_btn.setOnClickListener {
            viewModel.showMap.set(!viewModel.showMap.get()!!)
            setupMap()
        }

        map2_btn.setOnClickListener {
            viewModel.showMap.set(!viewModel.showMap.get()!!)
            setupMap()
        }
        print_iv.setOnClickListener {
            if (getPrinterStatus() == PRINTER_NORMAL) {
                printText()
            }
        }
    }


    fun setupMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun getLastLocation() {
        if (ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mFusedLocationClient?.lastLocation?.addOnCompleteListener { task ->
            if (task.isSuccessful && task.result != null) {
                LastKnowLocation = task.result!!
                putToast("lastlocation" + LastKnowLocation.toString())
            }
        }


    }

    fun setupTimeer(amount: Int): Boolean {
        val hoursold = cal!![Calendar.HOUR_OF_DAY]
        cal?.add(Calendar.MINUTE, amount)
        val nhoursold = cal!![Calendar.HOUR_OF_DAY]
        val nminsold = cal!![Calendar.MINUTE]
        return if (nhoursold >= hoursold && nminsold <= 59) {
            viewModel.orderTime.set(df.format(cal?.time))
            true
        } else {
            cal?.add(Calendar.MINUTE, -amount)
            viewModel.orderTime.set(df.format(cal?.time))
            false
        }
    }

    fun setupTimeerCalenderTwo(amount: Int): Boolean {
        val hoursold = cal2!![Calendar.HOUR_OF_DAY]
        cal2?.add(Calendar.MINUTE, amount)
        val nhoursold = cal2!![Calendar.HOUR_OF_DAY]
        val nminsold = cal2!![Calendar.MINUTE]
        return if (nhoursold >= hoursold && nminsold <= 59) {
            viewModel.orderExpectedFullTime.set(df.format(cal2?.time))
            true
        } else {
            cal2?.add(Calendar.MINUTE, -amount)
            viewModel.orderExpectedFullTime.set(df.format(cal2?.time))
            false
        }
    }


    fun setUpTime() {
        if (viewModel.isCurrentTime.get()!!) {
            viewModel.currentTime.set(df.format(cal?.getTime()))

        } else {
            val parser = SimpleDateFormat("dd.MM.yyyy HH:mm")
            val formatter = SimpleDateFormat("HH:mm")

            val formattera = SimpleDateFormat("dd.MM.yyyy")
            parser.timeZone =
                TimeZone.getTimeZone(viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE))
            cal2?.time = parser.parse(viewModel.expectedOrderFulFillDateTime.get().toString())
            Log.d("maontimecheckcal2", parser.format(cal2?.time))

            val output = formatter.format(
                parser.parse(
                    viewModel.expectedOrderFulFillDateTime.get().toString()
                )
            )
            viewModel.orderExpectedFullTime.set(output)
            viewModel.orderExpectedFullTimeInitial.set(output)
            viewModel.updateDate.set(formattera.format(cal2?.time))
        }
    }

    fun Query.addSnapshotListener(
        lifecycleOwner: LifecycleOwner,
        listener: (QuerySnapshot?, FirebaseFirestoreException?) -> Unit
    ) {
        val registration = addSnapshotListener(listener)
        lifecycleOwner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                registration.remove()
                lifecycleOwner.lifecycle.removeObserver(this)
            }
        })
    }


    fun DocumentReference.addSnapshotListener(
        owner: LifecycleOwner,
        listener: (DocumentSnapshot?, FirebaseFirestoreException?) -> Unit
    ): ListenerRegistration {
        val registration = addSnapshotListener(listener)

        owner.lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            fun onDestroy() {
                registration.remove()
                owner.lifecycle.removeObserver(this)
            }

            /*
            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun onStop() {

            }
             */
        })

        return registration
    }

    private fun observeResponse() {
        viewModel.mShowProgressBar.observe(viewLifecycleOwner, Observer {
            when (it) {
                true -> showLoadingIndicator()
                false -> dismissLoadingIndicator()
            }
        })

        viewModel.updateResponse.observe(viewLifecycleOwner, Observer {
            viewModel.mShowProgressBar.value = false
            if (it != null) {
                if (it.data?.success!!) {
                    //getActivity()?.getSupportFragmentManager()?.popBackStack()
                    viewModel.updateResponse.value = null
                    if (isAccepted) {
                        printText()
                    }
                    viewModel.orderDetailsAPI()
                    putToast("Status changed")
                } else {
                    viewModel.mShowProgressBar.value = null
                    viewModel.updateResponse.value = null
                    putToast("Something went wrong!")
                }

            }
        })

        viewModel.courierResponse.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                Log.e(
                    "OrderDetailsFragment",
                    "viewModel.courierResponse called viewModel.courierList: " + viewModel.courierList.size
                )
                viewModel.courierList.clear()
                it.data?.result?.forEach {
                    it.userFk?.name()?.let { it1 -> viewModel.courierList.add(it1) }
                    viewModel.courierListModel.add(SpinnerModel(it.id, it.userFk?.name()))
                }

            }
        })

        viewModel.rejectionResponse.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                viewModel.rejectList.clear()
                viewModel.rejectListModel.clear()
                it.data?.result?.forEach {
                    it.key?.let { it1 -> viewModel.rejectList.add(it1) }
                    viewModel.rejectListModel.add(SpinnerModel(it.id, it.key))
                }

            }
        })

        viewModel.orderDetailsResponse.observe(viewLifecycleOwner, Observer {

            if (it != null) {
                Log.d("orderDetailsApi", "called")
                viewModel.orderDetailsItemList.clear()
                it.data?.result?.orderItems?.let { it1 ->
                    viewModel.orderDetailsItemList.addAll(
                        it1
                    )
                }
                it.data?.result?.orderPayment.let { it1 ->
                    if (it1 != null) {
                        viewModel.isOrderPaymentNull.set(false)
                        viewModel.orderPaymentStatus.set(it1.paymentStatus)
                        viewModel.orderPaymentType.set(it1.paymentType)
                    } else {
                        viewModel.isOrderPaymentNull.set(true)
                    }

                }


                it.data?.result?.orderDeliveryType?.let { it1 ->
                    viewModel.putToast("" + it1)
                    if (it1.toLowerCase() == "delivery") {
                        viewModel.orderDeleveryTypes.set("Delivery")
                    } else {
                        viewModel.orderDeleveryTypes.set("pickup")
                    }
                }


                viewModel.orderType.set(it.data?.result?.status)
                viewModel.isASAP.set(it.data?.result?.isAsap)
                viewModel.totalAmount.set(it.data?.result?.finalAmount.toString())
                viewModel.subTotal.set(it.data?.result?.orderItemsTotal.toString())
                viewModel.tax.set(it.data?.result?.tax.toString())
                viewModel.taxRate.set(it.data?.result?.taxRate.toString())
                viewModel.isdeleveryFree.set(it.data?.result?.isDeliveryFree)
                viewModel.delevery.set(it.data?.result?.deliveryCharge.toString())
                viewModel.discount.set(it.data?.result?.discount.toString())
                viewModel.loyalty.set(it.data?.result?.loyaltyPoints.toString())
                viewModel.loyaltypointsConvertedAmount.set(it.data?.result?.loyaltyPointsConvertedAmount.toString())
                viewModel.currency.set(it.data?.result?.currency)
                viewModel.customer_name.set(it.data?.result?.orderAddressFK?.customerName.toString())
                viewModel.street.set(it.data?.result?.orderAddressFK?.street.toString() + " " + it.data?.result?.orderAddressFK?.no.toString())
                viewModel.postalCode.set(it.data?.result?.orderAddressFK?.postalCode.toString() + " " + it.data?.result?.orderAddressFK?.city.toString() + "\n" + it.data?.result?.orderAddressFK?.province.toString())
                viewModel.email.set(it.data?.result?.customerEmail.toString())
                viewModel.notes.set(it.data?.result?.customerNote.toString())
                it.data?.result?.paymentType?.let { payment ->
                    if (payment == "Stripe") {
                        viewModel.paymentType.set("Online")
                    } else {
                        viewModel.paymentType.set(payment)
                    }
                }

                viewModel.customer_phone.set(it.data?.result?.customerPhone)
                viewModel.customer_email.set(it.data?.result?.customerEmail)
                viewModel.courierName.set(it.data?.result?.currentCourier.toString())
                viewModel.creationTime.set(it.data?.result?.creationTime?.let { it1 ->
                    dateFormat(
                        it1,
                        viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE)
                    )
                })
                viewModel.orderDateTime.set(it.data?.result?.orderRequestedDateTime?.let { it1 ->
                    viewModel.requestedDateTime.set(it1)
                    dateFormat(
                        it1,
                        viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE)
                    )
                })

                orderAddressLatLng = it.data?.result?.orderAddressFK?.latitude?.let { it1 ->
                    it.data?.result?.orderAddressFK?.longitude?.let { it2 ->
                        LatLng(
                            it1,
                            it2
                        )
                    }
                }
                if (it.data?.result?.expectedOrderFulFillDateTime == null) {
                    isTimeAdded = false
                    viewModel.isCurrentTime.set(true)
                } else {
                    viewModel.isCurrentTime.set(false)
                }

                viewModel.expectedOrderFulFillDateTime.set(it.data?.result?.expectedOrderFulFillDateTime?.let { it1 ->
                    dateFormat(
                        it1,
                        viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE)
                    )
                })

                viewModel.currentExpectedFullFillTime.set(it.data?.result?.expectedOrderFulFillDateTime?.let { it1 ->
                    dateFormat(
                        it1,
                        viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE)
                    )
                })
                viewModel.orderExpectedFullTime.set(it.data?.result?.expectedOrderFulFillDateTime?.let { it1 ->
                    dateFormat(
                        it1,
                        viewModel.sharedPreference.getStringValue(Constants.TIME_ZONE)
                    )
                })

                setUpTime()

                if (it.data?.result?.expectedOrderFulFillDateTime == null) {
                    if (!isTimeAdded) {
                        isTimeAdded = true
                        viewModel.orderTime.set(df.format(cal?.getTime()))
                    }
                }
                baseRecyclerAdapter?.cleatDataSet()
                baseRecyclerAdapter?.addDataSet(viewModel.orderDetailsItemList)

            }
        })
    }

    private fun setUpRecyclerView() {
        baseRecyclerAdapter = BaseRecyclerViewAdapter(
            R.layout.order_details_list_item, BR.orderDetailsItem, ArrayList(), null,
            this
        )
        viewDataBinding!!.itemListRv.adapter = baseRecyclerAdapter
    }

    override val bindingVariable: Int
        get() = BR.dashBoardVM
    override val layoutId: Int
        get() = R.layout.fragment_order_details
    override val viewModel: CommonViewModel
        get() = activity?.run {
            ViewModelProvider(this).get(CommonViewModel::class.java)
        } ?: throw Exception("Invalid Activity")


    fun shoRejectPopup() {
        val li = LayoutInflater.from(requireActivity())
        val promptsView: View = li.inflate(R.layout.order_reject_popup, null)
        val alertDialogBuilder = AlertDialog.Builder(requireActivity())
        alertDialogBuilder.setView(promptsView)
        alertDialogBuilder.setCancelable(true)
        val alertDialog: AlertDialog = alertDialogBuilder.create()
        val yesButton = promptsView.findViewById(R.id.btn_yes) as Button
        yesButton.setOnClickListener {

            shoRejectReasonListPopup()
            alertDialog.dismiss()
        }
        val dialogButton: Button = promptsView.findViewById(R.id.cancel_button) as Button
        dialogButton.setOnClickListener({ alertDialog.dismiss() })
        alertDialog.show()
    }


    fun shoRejectReasonListPopup() {
        val li = LayoutInflater.from(requireActivity())
        val promptsView: View = li.inflate(R.layout.order_reject__reason_popup, null)
        val alertDialogBuilder = AlertDialog.Builder(requireActivity())
        alertDialogBuilder.setView(promptsView)
        alertDialogBuilder.setCancelable(true)
        val alertDialog: AlertDialog = alertDialogBuilder.create()
        val yesButton = promptsView.findViewById(R.id.btn_yes) as Button
        val radioGroup = promptsView.findViewById(R.id.radioGroup) as RadioGroup
        viewModel.rejectListModel.forEachIndexed { index, s ->
            val rdbtn = RadioButton(requireContext())
            s.id?.let { rdbtn.setId(it) }
            rdbtn.setText(s.name)
            radioGroup.addView(rdbtn)
        }

        radioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
                val rb = promptsView.findViewById(p1) as RadioButton
                viewModel.rejectReason.set(rb.text.toString())
            }

        })
        yesButton.setOnClickListener {
            if (viewModel.rejectReason.get() != "") {
                // radioGroup.getsele
                //viewModel.rejectReason.set(reasonBtn.text.toString())
                viewModel.updatOrderAPI(Constants.REJECTED_REASON)
                alertDialog.dismiss()
            } else {
                putToast("Please select the Reason")
            }
        }
        val dialogButton: Button = promptsView.findViewById(R.id.cancel_button) as Button
        dialogButton.setOnClickListener({ alertDialog.dismiss() })
        alertDialog.show()
    }


    fun shoCourierPopup() {
        val li = LayoutInflater.from(requireActivity())
        val promptsView: View = li.inflate(R.layout.courier_select_popup, null)
        val alertDialogBuilder = AlertDialog.Builder(requireActivity())
        alertDialogBuilder.setView(promptsView)
        alertDialogBuilder.setCancelable(true)
        val alertDialog: AlertDialog = alertDialogBuilder.create()
        val yesButton = promptsView.findViewById(R.id.btn_yes) as Button
        val spinner = promptsView.findViewById(R.id.courier_spn) as AppCompatSpinner
        yesButton.setOnClickListener {
            if (viewModel.courierId.get() != "0") {
                viewModel.updatOrderAPI(Constants.DISPATCHED)
                alertDialog.dismiss()
            } else {
                putToast("Please select the courier")
            }

        }
        val dialogButton: Button = promptsView.findViewById(R.id.cancel_button) as Button
        dialogButton.setOnClickListener({ alertDialog.dismiss() })
        val aa = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            viewModel.courierList
        )
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.setAdapter(aa)
        spinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(
                p0: AdapterView<*>?,
                p1: View?,
                position: Int,
                p3: Long
            ) {
                viewModel.courierId.set(viewModel.courierListModel[position].id.toString())
            }

        })
        alertDialog.show()
    }

    override fun onClickView(v: View?) {

    }

    override fun goTo(clazz: Class<*>, mExtras: Bundle?) {

    }

    override fun onItemClick(view: View, position: Int, v: OrderDetailsListItemBinding) {

    }

    override fun onDataBind(
        v: OrderDetailsListItemBinding,
        onClickListener: View.OnClickListener
    ) {
    }

    override fun onMapReady(map: GoogleMap?) {
        mMap = map
        setUpMap()
    }

    fun setUpMap() {
        marker?.remove()
        orderAddressLatLng?.let {
            destination = MarkerOptions().position(it).title("").snippet("to")
            mMap?.addMarker(destination)
            if (orderAccepterLocation == null) {
                moveCamera(it)
                animateCamera(it)
            }
            orderAccepterLocation?.let {
                //  val lat1=LatLng(11.504776,77.238396)
                origin = MarkerOptions().position(it).title(viewModel.customer_name.get())
                mMap?.addMarker(origin)
                marker = mMap?.addMarker(
                    origin
                )
                orderAccepterLocation?.let { showDefaultLocationOnMap(origin!!.position) }
                val height = 100
                val width = 60
                val bitmapdraw: BitmapDrawable =
                    resources.getDrawable(R.drawable.car) as BitmapDrawable
                val b: Bitmap = bitmapdraw.getBitmap()
                val smallMarker: Bitmap = Bitmap.createScaledBitmap(b, width, height, false)
                val icon = BitmapDescriptorFactory.fromBitmap(smallMarker)
                marker?.setIcon(icon)
                callRouteAPI()
            }
        }

    }

    private fun callRouteAPI() {
        putToast("caling route")
        orderAccepterLocation?.let {
            // val lat=LatLng( LastKnowLocation!!.latitude,LastKnowLocation!!.longitude)
            origin = MarkerOptions().position(it).title("").snippet("from")
        }
        orderAddressLatLng?.let {
            // val lat1=LatLng(13.072090,80.201860)
            destination = MarkerOptions().position(it).title("").snippet("to")
        }

        //setUpMap()
        val url = origin?.position?.let {
            destination?.position?.let { it1 ->
                getDirectionsUrl(
                    it,
                    it1
                )
            }
        }

        url?.let {
            DownloadTask().execute(url);
        }
    }

    private fun showDefaultLocationOnMap(latLng: LatLng) {
        moveCamera(latLng)
        animateCamera(latLng)
    }

    private fun moveCamera(latLng: LatLng) {
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    private fun animateCamera(latLng: LatLng) {
        val cameraPosition = CameraPosition.Builder().target(latLng).zoom(15.5f).build()
        mMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }


    inner class DownloadTask : AsyncTask<String?, Void?, String>() {
        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            val parserTask = ParserTask()
            parserTask.execute(result)
        }

        override fun doInBackground(vararg url: String?): String {
            var data: String? = null
            try {
                data = url[0]?.let { downloadUrl(it) }
            } catch (e: Exception) {
                Log.d("Background Task", e.toString())
            }
            return data.toString()
        }
    }

    inner class ParserTask :
        AsyncTask<String?, Int?, List<List<HashMap<String, String>>>?>() {
        override fun onPostExecute(result: List<List<HashMap<String, String>>>?) {
            val points = ArrayList<LatLng>()
            val lineOptions = PolylineOptions()
            for (i in result!!.indices) {
                val path =
                    result[i]
                for (j in path.indices) {
                    val point = path[j]
                    val lat = point["lat"]!!.toDouble()
                    val lng = point["lng"]!!.toDouble()
                    val position = LatLng(lat, lng)
                    points.add(position)
                }
                lineOptions.addAll(points)
                lineOptions.width(12f)
                lineOptions.color(Color.RED)
                lineOptions.geodesic(true)
            }

            // Drawing polyline in the Google Map
            if (points.size !== 0) mMap?.addPolyline(lineOptions)
//            val runnableCode = object: Runnable {
//                override fun run() {
//                    if (index < points.size - 1) {
//                        index++
//                        next = index + 1
//                    } else {
//                        index = -1
//                        next = 1
//                        return
//                    }
//
//                    if (index < points.size - 1) {
//                        startPosition = marker?.position
//                        endPosition = points[next]
//                        val Loca=Location("")
//                        Loca.latitude=points[next].latitude
//                         Loca.longitude=points[next].longitude
//                        LastKnowLocation=Loca
//                        addMarker(mMap!!)
//                       // getAnimationMarker(marker!!,mMap,startPosition!!,endPosition!!)
//                        println("kkfk$startPosition$endPosition")
//                    }
//                    handler!!.postDelayed(this, 2000)
//
//                }
//            }
//            handler?.postDelayed(runnableCode,100)

        }

        override fun doInBackground(vararg jsonData: String?): List<List<HashMap<String, String>>>? {
            val jObject: JSONObject
            var routes: List<List<HashMap<String, String>>>? =
                null
            try {
                jObject = JSONObject(jsonData[0])
                val parser = DataParser()
                routes = parser.parse(jObject)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            return routes
        }
    }

    private fun getDirectionsUrl(origin: LatLng, dest: LatLng): String? {

        // Origin of route
        val str_origin = "origin=" + origin.latitude + "," + origin.longitude

        // Destination of route
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude

        //setting transportation mode
        val mode = "mode=driving"
        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$mode"

        // Output format
        val output = "json"

        // Building the url to the web service
        return "https://maps.googleapis.com/maps/api/directions/$output?$parameters&key=AIzaSyD_tFEvt5xkXm4bto3ktAYsN2Xpqd2ls4w"
    }

    /**
     * A method to download json data from url
     */
    @Throws(IOException::class)
    private fun downloadUrl(strUrl: String): String? {
        var data = ""
        var iStream: InputStream? = null
        var urlConnection: HttpURLConnection? = null
        try {
            val url = URL(strUrl)
            urlConnection = url.openConnection() as HttpURLConnection
            urlConnection.connect()
            iStream = urlConnection.getInputStream()
            val br = BufferedReader(InputStreamReader(iStream))
            val sb = StringBuffer()
            var line: String? = ""
            while (br.readLine().also({ line = it }) != null) {
                sb.append(line)
            }
            data = sb.toString()
            br.close()
        } catch (e: java.lang.Exception) {
            Log.d("Exception", e.toString())
        } finally {
            iStream?.close()
            urlConnection?.disconnect()
        }
        return data
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    fun getIndexApiAction(): Action? {
        val `object` =
            Thing.Builder()
                .setName("IPosPrinterTestDemo Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build()
        return Action.Builder(Action.TYPE_VIEW)
            .setObject(`object`)
            .setActionStatus(Action.STATUS_TYPE_COMPLETED)
            .build()
    }

    override fun onStart() {
        super.onStart()
        client!!.connect()
        AppIndex.AppIndexApi.start(client, getIndexApiAction())
    }

    override fun onDestroy() {
        super.onDestroy()
        isOrdeDetailsScreen = false
        // clearAll()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        clearAll()
    }

    override fun onDetach() {
        super.onDetach()
        isOrdeDetailsScreen = false
        clearAll()
    }

    override fun onPause() {
        super.onPause()
        // stopLocationUpdates()
        //clearAll()
    }

    override fun onResume() {
        super.onResume()
        //  startLocationUpdates()
        val intent = Intent()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.DONUT) {
            intent.setPackage("com.iposprinter.iposprinterservice")
        }
        intent.action = "com.iposprinter.iposprinterservice.IPosPrintService"
        context?.bindService(intent, connectService, Context.BIND_AUTO_CREATE)
        val printerStatusFilter = IntentFilter()
        printerStatusFilter.addAction(PRINTER_NORMAL_ACTION)
        printerStatusFilter.addAction(PRINTER_PAPERLESS_ACTION)
        printerStatusFilter.addAction(PRINTER_PAPEREXISTS_ACTION)
        printerStatusFilter.addAction(PRINTER_THP_HIGHTEMP_ACTION)
        printerStatusFilter.addAction(PRINTER_THP_NORMALTEMP_ACTION)
        printerStatusFilter.addAction(PRINTER_MOTOR_HIGHTEMP_ACTION)
        printerStatusFilter.addAction(PRINTER_BUSY_ACTION)
        printerStatusFilter.addAction(GET_CUST_PRINTAPP_PACKAGENAME_ACTION)

        context?.registerReceiver(IPosPrinterStatusListener, printerStatusFilter)
    }

    override fun onStop() {
        super.onStop()
        AppIndex.AppIndexApi.end(client, getIndexApiAction())
        loopPrintFlag = DEFAULT_LOOP_PRINT
        context?.unregisterReceiver(IPosPrinterStatusListener)
        context?.unbindService(connectService)
        client!!.disconnect()
    }


    fun clearAll() {
        isTimeAdded = false
        isOrdeDetailsScreen = false
        viewModel.orderDetailsPage.set(false)
        viewModel.time.set("")
        viewModel.orderExpectedFullTime.set("")
        viewModel.orderId.set("")
        viewModel.orderType.set("")
        viewModel.isASAP.set(false)
        viewModel.showMap.set(false)
        viewModel.isOrderhistory.set(false)
        viewModel.isCurrentTime.set(true)
        viewModel.courierList.clear()
        viewModel.rejectList.clear()
        viewModel.courierListModel.clear()
        viewModel.rejectListModel.clear()
        viewModel.courierId.set("0")
        viewModel.currentTime.set("")
        viewModel.orderTime.set("")
        viewModel.currentExpectedFullFillTime.set("")
        viewModel.orderDateTime.set("")
        viewModel.creationTime.set("")
        viewModel.rejectReason.set("")
        viewModel.expectedOrderFulFillDateTime.set("")
        viewModel.orderDeleveryType.set("ASAP")
        viewModel.orderDetailsPage.set(false)
        viewModel.totalAmount.set("0")
        viewModel.subTotal.set("0")
        viewModel.taxRate.set("0")
        viewModel.tax.set("0")
        viewModel.isdeleveryFree.set(false)
        viewModel.delevery.set("0")
        viewModel.discount.set("")
        viewModel.loyalty.set("0")
        viewModel.loyaltypointsConvertedAmount.set("0")
        viewModel.customer_name.set("Customer Name")
        viewModel.street.set("Street Name Door Number")
        viewModel.postalCode.set("Postal Code City")
        viewModel.email.set("Email")
        viewModel.notes.set("notes")
        viewModel.paymentType.set("Cash")
        viewModel.orderDetailsItemList.clear()
    }


    //POS

    private fun setUpPOSPrinter() {
        handler = MyHandler(iHandlerIntent)
        callback = object : IPosPrinterCallback.Stub() {
            @Throws(RemoteException::class)
            override fun onRunResult(isSuccess: Boolean) {
                Log.i(TAG, "result:$isSuccess\n")
            }

            @Throws(RemoteException::class)
            override fun onReturnString(value: String) {
                Log.i(TAG, "result:$value\n")
            }
        }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            if (ActivityCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(
                        Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    1
                )
            }
        }

        IPosPrinterTestDemo.writePrintDataToCacheFile("*****************", null)
        client = GoogleApiClient.Builder(context!!).addApi(AppIndex.API).build()
    }

    fun doReadyPrintingText() {
        if (viewModel.orderType.get().toString().equals(Constants.NEW) || viewModel.orderType.get()
                .toString().equals(Constants.PreOrdered)
        ) {
            ThreadPoolManager.getInstance()
                .executeTask {
                    try {
                        val mBitmap =
                            BitmapFactory.decodeResource(resources, R.drawable.ic_logo)
                        try {
                            mIPosPrinterService!!.printBitmap(0, 4, mBitmap, callback)
                            mIPosPrinterService!!.printBlankLines(1, 10, callback)
                        } catch (e: RemoteException) {
                            e.printStackTrace()
                        }

                        viewModel.sharedPreference.getStringValue(Constants.APP_NAME).let {
                            if (it == "Default") {
                                val appName = "   Order Recieving App"
                                mIPosPrinterService!!.printSpecifiedTypeText(
                                    appName + "\n",
                                    "ST",
                                    32,
                                    callback
                                )
                            } else {
                                mIPosPrinterService!!.printSpecifiedTypeText(
                                    "   " + it + "\n",
                                    "ST",
                                    32,
                                    callback
                                )
                            }
                        }

                        mIPosPrinterService!!.printBlankLines(1, 8, callback)
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "       " + "SwissOrdering",
                            "ST",
                            32,
                            callback
                        )


                        Glide.with(requireActivity())
                            .asBitmap()
                            .load(viewModel.sharedPreference.getStringValue(Constants.APP_IMAGE))
                            .into(object : CustomTarget<Bitmap>() {
                                override fun onLoadCleared(placeholder: Drawable?) {

                                }

                                // For Restaurant name and logo
                                override fun onResourceReady(
                                    resource: Bitmap,
                                    transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                                ) {

                                    try {
                                        mIPosPrinterService!!.printSpecifiedTypeText(
                                            viewModel.sharedPreference.getStringValue(Constants.APP_NAME),
                                            "ST",
                                            32,
                                            callback
                                        )
                                        mIPosPrinterService!!.printBitmap(0, 4, resource, callback)
                                        mIPosPrinterService!!.printBlankLines(1, 10, callback)
                                    } catch (e: RemoteException) {
                                        e.printStackTrace()
                                    }

                                }
                            })


                        if (viewModel.sharedPreference.getBooleanValue(Constants.SHOW_CUSTOMER_DETAILS)) {
                            mIPosPrinterService!!.printBlankLines(1, 8, callback)
                            mIPosPrinterService!!.printSpecifiedTypeText(
                                viewModel.sharedPreference.getStringValue(Constants.ADDRESS) + "," + viewModel.sharedPreference.getStringValue(
                                    Constants.PHONE_NO
                                ) + "\n",
                                "ST",
                                24,
                                callback
                            )
                        }

                        mIPosPrinterService!!.printBlankLines(1, 8, callback)
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "********************************",
                            "ST",
                            24,
                            callback
                        )



                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "         " + viewModel.sharedPreference.getStringValue(
                                viewModel.orderDeleveryTypes.get().toString()
                            ) + "\n",
                            "ST",
                            32,
                            callback
                        )
                        mIPosPrinterService!!.printBlankLines(1, 8, callback)
                        if (viewModel.expectedOrderFulFillDateTime.get() != null) {
                            mIPosPrinterService!!.printSpecifiedTypeText(
                                "         " + viewModel.expectedOrderFulFillDateTime.get() + "\n",
                                "ST",
                                24,
                                callback
                            )

                        }

                        mIPosPrinterService!!.printBlankLines(1, 8, callback)
                        viewModel.isASAP.get()?.let {
                            if (it) {
                                mIPosPrinterService!!.printSpecifiedTypeText(
                                    "         Expected: ASAP" + "\n",
                                    "ST",
                                    24,
                                    callback
                                )
                            }

                        }

                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "********************************",
                            "ST",
                            24,
                            callback
                        )

                        mIPosPrinterService!!.printSpecifiedTypeText(
                            viewModel.sharedPreference.getStringValue("Order") + " " + viewModel.orderId.get() + "\n",
                            "ST",
                            32,
                            callback
                        )

                        mIPosPrinterService!!.printBlankLines(1, 8, callback)
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "   " + viewModel.sharedPreference.getStringValue("OrderedOn") + " " + viewModel.creationTime.get() + "\n",
                            "ST",
                            24,
                            callback
                        )

                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "********************************",
                            "ST",
                            24,
                            callback
                        )
                        mIPosPrinterService!!.printBlankLines(1, 8, callback)
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "         " + viewModel.sharedPreference.getStringValue("Customer") + "\n",
                            "ST",
                            32,
                            callback
                        )
                        mIPosPrinterService!!.printBlankLines(1, 8, callback)
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            viewModel.customer_name.get() + "\n",
                            "ST",
                            24,
                            callback
                        )
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            viewModel.street.get() + "\n",
                            "ST",
                            24,
                            callback
                        )
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            viewModel.postalCode.get() + "\n",
                            "ST",
                            24,
                            callback
                        )
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "Tel: " + viewModel.customer_phone.get() + "\n",
                            "ST",
                            24,
                            callback
                        )
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "Email: " + viewModel.customer_email.get() + "\n",
                            "ST",
                            24,
                            callback
                        )

                        mIPosPrinterService!!.setPrinterPrintAlignment(1, callback)
                        /*mIPosPrinterService!!.printQRCode(
                            "https://www.google.com/maps/?q=" + orderAddressLatLng?.latitude.toString() + "," + orderAddressLatLng?.longitude.toString(),
                            6,
                            2,
                            callback
                        )*/
                        mIPosPrinterService!!.printQRCode(
                            viewModel.orderId.get().toString(),
                            6,
                            2,
                            callback
                        )
                        mIPosPrinterService!!.printBlankLines(1, 15, callback)
                        mIPosPrinterService!!.printBlankLines(1, 16, callback)
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "********************************",
                            "ST",
                            24,
                            callback
                        )
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "   " + viewModel.sharedPreference.getStringValue("Items") + "\n",
                            "ST",
                            42,
                            callback
                        )



                        mIPosPrinterService!!.setPrinterPrintAlignment(0, callback)
                        mIPosPrinterService!!.setPrinterPrintFontSize(24, callback)
                        val text = arrayOfNulls<String>(4)
                        val width = intArrayOf(11, 6, 11)
                        val align = intArrayOf(0, 2, 2) // 左齐,右齐,右齐,右齐

                        text[0] = viewModel.sharedPreference.getStringValue("Items")
                        text[1] = viewModel.sharedPreference.getStringValue("QTY")
                        text[2] = viewModel.sharedPreference.getStringValue("Price")
                        //  text[3] = viewModel.sharedPreference.getStringValue("Total")
                        mIPosPrinterService!!.printColumnsText(text, width, align, 1, callback)
                        mIPosPrinterService!!.setPrinterPrintFontSize(24, callback)
                        mIPosPrinterService!!.setPrinterPrintFontType("ST", callback)
                        viewModel.orderDetailsItemList.forEachIndexed { index, orderItem ->
                            text[0] = orderItem.getItems()
                            text[1] = orderItem.getQty()
                            text[2] =
                                viewModel.sharedPreference.getStringValue(Constants.CURRENCY) + " " + orderItem.getPrice()
                            // text[3] = ""
                            mIPosPrinterService!!.printColumnsText(
                                text,
                                width,
                                align,
                                1,
                                callback
                            )
                        }
//                        text[0] = ""
//                        text[1] = ""
//                        text[2] = ""
//                        text[3] = viewModel.sharedPreference.getStringValue(Constants.CURRENCY)+" "+viewModel.totalAmount.get()
//                        mIPosPrinterService!!.printColumnsText(text, width, align, 1, callback)
                        //mIPosPrinterService!!.printBlankLines(1, 16, callback)
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "********************************",
                            "ST",
                            24,
                            callback
                        )

                        mIPosPrinterService!!.printBlankLines(1, 16, callback)
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            " " + viewModel.sharedPreference.getStringValue("SubTotal") + "  " + viewModel.sharedPreference.getStringValue(
                                Constants.CURRENCY
                            ) + " " + viewModel.subTotal.get() + "\n",
                            "ST",
                            32,
                            callback
                        )
                        mIPosPrinterService!!.printBlankLines(1, 16, callback)
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            " " + viewModel.sharedPreference.getStringValue("Tax") + " %" + viewModel.taxRate.get() + " Included " + viewModel.sharedPreference.getStringValue(
                                Constants.CURRENCY
                            ) + " " + viewModel.tax.get() + "\n",
                            "ST",
                            32,
                            callback
                        )
                        mIPosPrinterService!!.printBlankLines(1, 16, callback)
                        if (viewModel.isdeleveryFree.get()!!) {
                            mIPosPrinterService!!.printSpecifiedTypeText(
                                " " + viewModel.sharedPreference.getStringValue("Delivery") + "     Free" + "\n",
                                "ST",
                                32,
                                callback
                            )
                        } else {
                            mIPosPrinterService!!.printSpecifiedTypeText(
                                " " + viewModel.sharedPreference.getStringValue("Delivery") + "   " + viewModel.delevery.get() + "\n",
                                "ST",
                                32,
                                callback
                            )
                        }

                        if (viewModel.discount.get() != "0") {
                            mIPosPrinterService!!.printSpecifiedTypeText(
                                " " + viewModel.sharedPreference.getStringValue("CouponDiscount") + "   " + viewModel.discount.get() + "\n",
                                "ST",
                                32,
                                callback
                            )
                        }

                        mIPosPrinterService!!.printBlankLines(1, 16, callback)
                        if (viewModel.loyaltypointsConvertedAmount.get() != "0") {
                            mIPosPrinterService!!.printSpecifiedTypeText(
                                " " + viewModel.sharedPreference.getStringValue("LoyaltyPoints") + "   " + viewModel.discount.get() + "\n",
                                "ST",
                                32,
                                callback
                            )
                        }
                        mIPosPrinterService!!.printBlankLines(1, 16, callback)
                        mIPosPrinterService!!.printSpecifiedTypeText(
                            " " + viewModel.sharedPreference.getStringValue("Total") + "  " + viewModel.sharedPreference.getStringValue(
                                Constants.CURRENCY
                            ) + " " + viewModel.totalAmount.get(),
                            "ST",
                            42,
                            callback
                        )

                        mIPosPrinterService!!.printBlankLines(1, 16, callback)

                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "   NOTES" + "\n",
                            "ST",
                            42,
                            callback
                        )

                        mIPosPrinterService!!.printSpecifiedTypeText(
                            viewModel.notes.get() + "\n",
                            "ST",
                            24,
                            callback
                        )

                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "********************************",
                            "ST",
                            24,
                            callback
                        )

                        mIPosPrinterService!!.printSpecifiedTypeText(
                            "         " + viewModel.sharedPreference.getStringValue("Payment") + "\n",
                            "ST",
                            32,
                            callback
                        )


                        if (viewModel.isOrderPaymentNull.get()!! || viewModel.orderPaymentStatus.get() != "Paid") {
                            mIPosPrinterService!!.printSpecifiedTypeText(
                                "" + viewModel.sharedPreference.getStringValue("NotPaidDescription") + "\n",
                                "ST",
                                32,
                                callback
                            )

                            mIPosPrinterService!!.printSpecifiedTypeText(
                                "" + viewModel.sharedPreference.getStringValue("NotPaid") + "\n",
                                "ST",
                                32,
                                callback
                            )
                        } else if (!viewModel.isOrderPaymentNull.get()!! || viewModel.orderPaymentStatus.get() == "Paid") {
                            mIPosPrinterService!!.printSpecifiedTypeText(
                                "" + viewModel.sharedPreference.getStringValue("PaidOnlineDescription") + "\n",
                                "ST",
                                32,
                                callback
                            )

                            mIPosPrinterService!!.printSpecifiedTypeText(
                                "" + viewModel.sharedPreference.getStringValue("PaidOnline") + "\n",
                                "ST",
                                32,
                                callback
                            )
                        }

                        MemInfo.bitmapRecycle(mBitmap)
//                        mIPosPrinterService!!.printerPerformPrint(160, callback)
                    } catch (e: RemoteException) {
                        e.printStackTrace()
                    }
                }
        }
    }

    fun printText() {
        mIPosPrinterService!!.printerPerformPrint(160, callback)
    }

    fun getPrinterStatus(): Int {
        Log.i(TAG, "***** printerStatus$printerStatus")
        try {
            if (mIPosPrinterService != null) {
                printerStatus = mIPosPrinterService!!.printerStatus
            } else {
                printerStatus = -1
            }
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
        Log.i(TAG, "#### printerStatus$printerStatus")
        return printerStatus
    }

    private val iHandlerIntent =
        IHandlerIntent { msg ->
            when (msg.what) {
                MSG_TEST -> {
                }
                MSG_IS_NORMAL -> if (getPrinterStatus() == PRINTER_NORMAL) {
                    loopPrint(loopPrintFlag)
                }
                MSG_IS_BUSY -> Toast.makeText(
                    context!!,
                    R.string.printer_is_working,
                    Toast.LENGTH_SHORT
                ).show()
                MSG_PAPER_LESS -> {
                    loopPrintFlag = DEFAULT_LOOP_PRINT
                    Toast.makeText(
                        context!!,
                        R.string.out_of_paper,
                        Toast.LENGTH_SHORT
                    ).show()
                }
                MSG_PAPER_EXISTS -> Toast.makeText(
                    context!!,
                    R.string.exists_paper,
                    Toast.LENGTH_SHORT
                ).show()
                MSG_THP_HIGH_TEMP -> Toast.makeText(
                    context!!,
                    R.string.printer_high_temp_alarm,
                    Toast.LENGTH_SHORT
                ).show()
                MSG_MOTOR_HIGH_TEMP -> {
                    loopPrintFlag = DEFAULT_LOOP_PRINT
                    Toast.makeText(
                        context!!,
                        R.string.motor_high_temp_alarm,
                        Toast.LENGTH_SHORT
                    ).show()
                    handler!!.sendEmptyMessageDelayed(
                        MSG_MOTOR_HIGH_TEMP_INIT_PRINTER,
                        180000
                    ) //马达高温报警，等待3分钟后复位打印机
                }
                MSG_MOTOR_HIGH_TEMP_INIT_PRINTER -> {
                    //printerInit()
                }
                MSG_CURRENT_TASK_PRINT_COMPLETE -> Toast.makeText(
                    context!!,
                    R.string.printer_current_task_print_complete,
                    Toast.LENGTH_SHORT
                ).show()
                else -> {
                }
            }
        }

    fun loopPrint(flag: Int) {
//        when (flag) {
//            MULTI_THREAD_LOOP_PRINT -> multiThreadLoopPrint()
//            DEMO_LOOP_PRINT -> demoLoopPrint()
//            INPUT_CONTENT_LOOP_PRINT -> bigDataPrintTest(127, loopContent)
//            PRINT_DRIVER_ERROR_TEST -> printDriverTest()
//            else -> {
//            }
//        }
    }


    private val IPosPrinterStatusListener: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action
            if (action == null) {
                Log.d(
                    TAG,
                    "IPosPrinterStatusListener onReceive action = null"
                )
                return
            }
            Log.d(
                TAG,
                "IPosPrinterStatusListener action = $action"
            )
            if (action == PRINTER_NORMAL_ACTION) {
                handler!!.sendEmptyMessageDelayed(MSG_IS_NORMAL, 0)
            } else if (action == PRINTER_PAPERLESS_ACTION) {
                handler!!.sendEmptyMessageDelayed(MSG_PAPER_LESS, 0)
            } else if (action == PRINTER_BUSY_ACTION) {
                handler!!.sendEmptyMessageDelayed(MSG_IS_BUSY, 0)
            } else if (action == PRINTER_PAPEREXISTS_ACTION) {
                handler!!.sendEmptyMessageDelayed(MSG_PAPER_EXISTS, 0)
            } else if (action == PRINTER_THP_HIGHTEMP_ACTION) {
                handler!!.sendEmptyMessageDelayed(MSG_THP_HIGH_TEMP, 0)
            } else if (action == PRINTER_THP_NORMALTEMP_ACTION) {
                handler!!.sendEmptyMessageDelayed(MSG_THP_TEMP_NORMAL, 0)
            } else if (action == PRINTER_MOTOR_HIGHTEMP_ACTION) //此时当前任务会继续打印，完成当前任务后，请等待2分钟以上时间，继续下一个打印任务
            {
                handler!!.sendEmptyMessageDelayed(MSG_MOTOR_HIGH_TEMP, 0)
            } else if (action == PRINTER_CURRENT_TASK_PRINT_COMPLETE_ACTION) {
                handler!!.sendEmptyMessageDelayed(MSG_CURRENT_TASK_PRINT_COMPLETE, 0)
            } else if (action == GET_CUST_PRINTAPP_PACKAGENAME_ACTION) {
                val mPackageName = intent.getPackage()
                Log.d(
                    TAG,
                    "*******GET_CUST_PRINTAPP_PACKAGENAME_ACTION：$action*****mPackageName:$mPackageName"
                )
            } else {
                handler!!.sendEmptyMessageDelayed(MSG_TEST, 0)
            }
        }
    }

    private val connectService: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            mIPosPrinterService = IPosPrinterService.Stub.asInterface(service)
            //setButtonEnable(true)
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mIPosPrinterService = null
        }
    }

    companion object {
        /*定义打印机状态*/
        private const val PRINTER_NORMAL = 0
        private const val PRINTER_PAPERLESS = 1
        private const val PRINTER_THP_HIGH_TEMPERATURE = 2
        private const val PRINTER_MOTOR_HIGH_TEMPERATURE = 3
        private const val PRINTER_IS_BUSY = 4
        private const val PRINTER_ERROR_UNKNOWN = 5

        /*打印机当前状态*/


        /*定义状态广播*/
        private const val PRINTER_NORMAL_ACTION =
            "com.iposprinter.iposprinterservic e.NORMAL_ACTION"
        private const val PRINTER_PAPERLESS_ACTION =
            "com.iposprinter.iposprinterservice.PAPERLESS_ACTION"
        private const val PRINTER_PAPEREXISTS_ACTION =
            "com.iposprinter.iposprinterservice.PAPEREXISTS_ACTION"
        private const val PRINTER_THP_HIGHTEMP_ACTION =
            "com.iposprinter.iposprinterservice.THP_HIGHTEMP_ACTION"
        private const val PRINTER_THP_NORMALTEMP_ACTION =
            "com.iposprinter.iposprinterservice.THP_NORMALTEMP_ACTION"
        private const val PRINTER_MOTOR_HIGHTEMP_ACTION =
            "com.iposprinter.iposprinterservice.MOTOR_HIGHTEMP_ACTION"
        private const val PRINTER_BUSY_ACTION =
            "com.iposprinter.iposprinterservice.BUSY_ACTION"
        private const val PRINTER_CURRENT_TASK_PRINT_COMPLETE_ACTION =
            "com.iposprinter.iposprinterservice.CURRENT_TASK_PRINT_COMPLETE_ACTION"
        private const val GET_CUST_PRINTAPP_PACKAGENAME_ACTION =
            "android.print.action.CUST_PRINTAPP_PACKAGENAME"

        /*定义消息*/
        private const val MSG_TEST = 1
        private const val MSG_IS_NORMAL = 2
        private const val MSG_IS_BUSY = 3
        private const val MSG_PAPER_LESS = 4
        private const val MSG_PAPER_EXISTS = 5
        private const val MSG_THP_HIGH_TEMP = 6
        private const val MSG_THP_TEMP_NORMAL = 7
        private const val MSG_MOTOR_HIGH_TEMP = 8
        private const val MSG_MOTOR_HIGH_TEMP_INIT_PRINTER = 9
        private const val MSG_CURRENT_TASK_PRINT_COMPLETE = 10

        /*循环打印类型*/
        private const val MULTI_THREAD_LOOP_PRINT = 1
        private const val INPUT_CONTENT_LOOP_PRINT = 2
        private const val DEMO_LOOP_PRINT = 3
        private const val PRINT_DRIVER_ERROR_TEST = 4
        private const val DEFAULT_LOOP_PRINT = 0

        //循环打印标志位

        private const val loopContent: Byte = 0x00
        private const val printDriverTestCount = 0
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        Log.e("HomeFragment", "onConfigurationChanged called")
        super.onConfigurationChanged(newConfig)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.e("HomeFragment", "onSaveInstanceState called")
        outState.putString("TAB", HomeFragment.status)
//        tabSelectedPosition = tabLayout.selectedTabPosition
//        outState.putInt("tabSelectedPosition", tabSelectedPosition)
    }

}