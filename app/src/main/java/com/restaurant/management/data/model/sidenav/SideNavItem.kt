package com.restaurant.management.data.model.sidenav

data class SideNavItem(
    var id:Int,
    var itemName:String,
    var resourceId:Int,
    var resourceIdInactive:Int,
    var isSelected:Boolean
)