package com.restaurant.management.data.model.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class OrderHistoryListResponse {

    @SerializedName("totalCount")
    @Expose
    var totalCount: Int? = null

    @SerializedName("items")
    @Expose
    var items: List<Item>? = null
}