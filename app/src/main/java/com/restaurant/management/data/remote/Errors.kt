package com.restaurant.management.data.remote

import com.google.gson.annotations.SerializedName

data class Errors(@SerializedName("msg")
                  var message: String)