package com.restaurant.management.data.model.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




data class LoginResponse(
    var result: Result,
    var error: Error,
)

data class Result(
    val accessToken: String,
    val encryptedAccessToken: String,
    val expireInSeconds: Int,
    val isEmailConfirmed: Boolean,
    val isPhoneConfirmed: Boolean,
    val passwordResetCode: Any,
    val refreshToken: String,
    val requiresTwoFactorVerification: Boolean,
    val returnUrl: Any,
    val shouldResetPassword: Boolean,
    val twoFactorAuthProviders: Any,
    val twoFactorRememberClientToken: Any,
    val userId: Int
)


data class Error(
    val code: Int,
    val details: String,
    val message: String,
    val validationErrors: Any
)