package com.restaurant.management.data.remote.client

import com.google.gson.GsonBuilder
import com.restaurant.management.data.local.sharedPref.ISharedPreferenceService
import com.restaurant.management.utils.Constants
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.KoinComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class TrackingClientBuilder(private val interceptor: Interceptor,private val sharedPreferences: ISharedPreferenceService) : KoinComponent,ITrackingClientBuilder {

    private val API_BASE_URL ="https://default.ordering.ch/api/"
    private val CONNECT_TIMEOUT = 100
    private val READ_TIMEOUT = 100
    private val WRITE_TIMEOUT = 100


    private val httpLoggingInterceptor = HttpLoggingInterceptor()

    private var gson = GsonBuilder()
        .setLenient()
        .create()

    private val builder = Retrofit.Builder()
        .baseUrl(API_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))



    private fun createClientAuth(): OkHttpClient {
        var tlsSocketFactory= TLSSocketFactory()
        //ADD DISPATCHER WITH MAX REQUEST TO 1

        val dispatcher = Dispatcher()
        dispatcher.maxRequests = 3
        val client: OkHttpClient = OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(interceptor)
            .sslSocketFactory(tlsSocketFactory, tlsSocketFactory.trustManager)
            .build()

        return client
    }



    /*private val client: OkHttpClient = OkHttpClient.Builder()
        .dispatcher(Dispatcher())
        .connectTimeout(CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
        .writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
        .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .build()*/

    private val retrofit: Retrofit = builder.client(createClientAuth()).build()

    init {
        if (API_BASE_URL=="https://default.ordering.ch/api/") {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        } else {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        }
    }


    override fun getClient() = createClientAuth()

    override fun getBuilder() = builder

    override fun getRetrofit()= retrofit
}