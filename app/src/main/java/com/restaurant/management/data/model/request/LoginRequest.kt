package com.restaurant.management.data.model.request

data class LoginRequest(
    val Password: String,
    val UserNameOrEmailAddress: String
)