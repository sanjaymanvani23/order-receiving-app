package com.restaurant.management.data.model.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class ForgotPasswordResponse {

    @SerializedName("isSuccess")
    @Expose
    var isSuccess: Boolean? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("data")
    @Expose
    var data: Any? = null
}