package com.restaurant.management.data.remote.client


import com.restaurant.management.data.repository.IUserRepository
import com.restaurant.management.data.repository.order.IOrderRepository
import org.koin.core.KoinComponent

class TrackingClient(private val iKinderClientBuilder: ITrackingClientBuilder) : KoinComponent,
    ITrackingClient {


    override fun getUserControllerRepository(): IUserRepository {
        return iKinderClientBuilder.getRetrofit().create(IUserRepository::class.java)
    }

    override fun getOrderControllerRepository(): IOrderRepository {
        return iKinderClientBuilder.getRetrofit().create(IOrderRepository::class.java)
    }

}