package com.restaurant.management.data.model.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class RejectReasonResponse {


    @SerializedName("key")
    @Expose
    var key: String? = null

    @SerializedName("reason")
    @Expose
    var reason: String? = null

    @SerializedName("id")
    @Expose
    var id: Int? = null
}