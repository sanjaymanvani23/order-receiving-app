package com.restaurant.management.data.remote.client

import com.restaurant.management.data.repository.IUserRepository
import com.restaurant.management.data.repository.order.IOrderRepository

interface ITrackingClient {
    fun getUserControllerRepository(): IUserRepository
    fun getOrderControllerRepository(): IOrderRepository

}