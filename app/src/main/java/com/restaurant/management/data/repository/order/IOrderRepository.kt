package com.restaurant.management.data.repository.order

import com.restaurant.management.data.model.request.GetOrderRequest
import com.restaurant.management.data.model.request.LoginRequest
import com.restaurant.management.data.model.request.UpdateOrderRequest
import com.restaurant.management.data.model.response.*
import com.restaurant.management.data.remote.BaseResponse
import retrofit2.Call
import retrofit2.http.*

interface IOrderRepository {

    @POST("restaurant/{branchId}/order-service/order-history")
    fun GetOrder(
        @Path("branchId") branchId:String,
        @Body request: GetOrderRequest,
        @Query("MaxResultCount") maxResultCount: String,
        @Query("SkipCount") skipCount: String,
    ): Call<BaseResponse<OrderListItemResponse>>


    @GET("restaurant/{branchId}/order-service/help-content")
    fun getHelpContent(
        @Path("branchId") branchId:String,
    ): Call<BaseResponse<String>>

    @GET("restaurant/{branchId}/order-service/is-opened")
    fun isRestaurantOpen(
        @Path("branchId") branchId:String,
    ): Call<BaseResponse<Boolean>>

    @GET("tokenAuth/logout")
    fun logout(
    ): Call<BaseResponse<Nothing>>


    @POST("restaurant/{branchId}/order-service/open")
    fun openRestaurant(
        @Path("branchId") branchId:String,
    ): Call<BaseResponse<Nothing>>

    @POST("restaurant/{branchId}/order-service/close")
    fun closeRestaurant(
        @Path("branchId") branchId:String,
    ): Call<BaseResponse<Nothing>>

    @GET("restaurant/{branchId}/order-service/settings")
    fun settings(
        @Path("branchId") branchId:String,
    ): Call<BaseResponse<SettingsResponse>>

    @GET("restaurant/{branchId}/order-service/order-details/{orderId}")
    fun getOrderDetails(
        @Path("branchId") branchId:String,
        @Path("orderId") orderId:String,
    ): Call<BaseResponse<OrderDetailsResponse>>

    @POST("restaurant/{branchId}/order-service/update-order/{orderId}")
    fun updateOrderDetails(
        @Path("branchId") branchId:String,
        @Path("orderId") orderId:String,
        @Body request: UpdateOrderRequest,
    ): Call<BaseResponse<Nothing>>

    @GET("restaurant/{branchId}/order-service/couriers")
    fun getCourier(
        @Path("branchId") branchId:String,
    ): Call<BaseResponse<List<CourierResponse>>>

    @GET("restaurant/{branchId}/order-service/reject-reasons")
    fun getRejectionReason(
        @Path("branchId") branchId:String,
    ): Call<BaseResponse<List<RejectReasonResponse>>>

    @GET("locale-info")
    fun getLocalization(
        @Query("culture") culture:String,
    ): Call<BaseResponse<List<LanguageValuePairModel>>>

}

