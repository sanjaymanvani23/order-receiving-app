package com.restaurant.management.data.remote

import com.google.gson.annotations.SerializedName
import com.restaurant.management.data.model.response.Result

data class BaseResponse<T>(
    @SerializedName("error")
    val error: String?,
    @SerializedName("result")
    val result: T?,
    @SerializedName("success")
    val success: Boolean?,
    @SerializedName("message")
    val message:String?
)
