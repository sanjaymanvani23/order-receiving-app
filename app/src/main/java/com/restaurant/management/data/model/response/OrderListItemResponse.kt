package com.restaurant.management.data.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.restaurant.management.utils.Singleton
import com.restaurant.management.utils.Singleton.currencyFormat
import com.restaurant.management.utils.Singleton.dateFormat
import com.restaurant.management.utils.Singleton.viewBtn


data class OrderListItemResponse(


    @SerializedName("ordersTotal")
    @Expose
    var ordersTotal: Double? = null,

    @SerializedName("totalCount")
    @Expose
    var totalCount: Int? = null,

    @SerializedName("items")
    @Expose
    var items: List<Item>? = null
)

data class Item(
    @SerializedName("orderAddressFK")
    @Expose
    var orderAddressFK: OrderAddressFK? = null,

    @SerializedName("finalAmount")
    @Expose
    var finalAmount: Double? = null,

    @SerializedName("status")
    @Expose
    var status: String? = null,

    @SerializedName("isPaid")
    @Expose
    var isPaid: Boolean? = null,

    @SerializedName("isPreOrder")
    @Expose
    var isPreOrder: Boolean? = null,


    @SerializedName("dateTime")
    @Expose
    var dateTime: String? = null,

    @SerializedName("id")
    @Expose
    var id: String? = null
) {

    fun getFullAddress(): String {
        return orderAddressFK?.customerName.toString() + "\n" + orderAddressFK?.street.toString() + " " + orderAddressFK?.no.toString() + "\n" + orderAddressFK?.postalCode.toString() + " " + orderAddressFK?.city.toString() + "\n"
    }

    fun getStreetNameDoorNum(): String {
        return orderAddressFK?.street.toString() + " " + orderAddressFK?.no.toString()
    }

    fun getPostalCity(): String {
        return orderAddressFK?.postalCode.toString() + " " + orderAddressFK?.city.toString()
    }

    fun getCustomerName(): String {
        return orderAddressFK?.customerName.toString()
    }

    fun getDateTimeFormat(dateTime: String): String {
        return dateFormat(dateTime, Singleton.timeZone)
    }

    fun getCurrencytype(): String {
        return currencyFormat
    }

    fun viewBtns(): String {
        return viewBtn
    }
}

data class OrderAddressFK(
    @SerializedName("customerName")
    @Expose
    var customerName: String? = null,

    @SerializedName("street")
    @Expose
    var street: String? = null,

    @SerializedName("no")
    @Expose
    var no: String? = null,

    @SerializedName("city")
    @Expose
    var city: String? = null,

    @SerializedName("postalCode")
    @Expose
    var postalCode: Any? = null,

    @SerializedName("province")
    @Expose
    var province: Any? = null,

    @SerializedName("postalCodeId")
    @Expose
    var postalCodeId: Int? = null,

    @SerializedName("countryId")
    @Expose
    var countryId: Int? = null,

    @SerializedName("provinceId")
    @Expose
    var provinceId: Int? = null,

    @SerializedName("orderId")
    @Expose
    var orderId: String? = null,

    @SerializedName("country")
    @Expose
    var country: CountryModel? = null,

    @SerializedName("longitude")
    @Expose
    var longitude: Double? = null,

    @SerializedName("latitude")
    @Expose
    var latitude: Double? = null,

    @SerializedName("id")
    @Expose
    var id: String? = null
) {
    fun getFullAddress(): String {
        return customerName + "\n" + street.toString() + " " + no.toString() + "\n" + postalCode.toString() + " " + city.toString() + "\n" + province.toString() + " " + country?.name
    }

}

data class CountryModel(
    @SerializedName("id")
    @Expose
    var id: Double? = null,

    @SerializedName("name")
    @Expose
    var name: String? = null,

    )
