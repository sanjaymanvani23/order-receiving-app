package com.restaurant.management.data.repository

import androidx.lifecycle.MutableLiveData
import com.restaurant.management.data.model.request.LoginRequest
import com.restaurant.management.data.model.response.LoginResponse
import com.restaurant.management.data.remote.Resource
import org.koin.core.KoinComponent

class UserControllerRepository (private val iUserControllerRepository: IUserRepository): KoinComponent, IUserControllerRespository, BaseRespository(){

    override fun login(
        loginRequest: LoginRequest,

        response: MutableLiveData<Resource<LoginResponse>>
    ) {
        iUserControllerRepository.login(loginRequest)
            .enqueue(getCallbackForLogin(response))
    }

}