package com.restaurant.management.data.model.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class GetOrderRequest {

    @SerializedName("startDate")
    @Expose
    var startDate: String? = null

    @SerializedName("endDate")
    @Expose
    var endDate: String? = null

    @SerializedName("courierId")
    @Expose
    var courierId: Int? = null

    @SerializedName("orderFilterDuration")
    @Expose
    var orderFilterDuration: Int? = null

    @SerializedName("isActiveOrdersOnly")
    @Expose
    var isActiveOrdersOnly: Boolean? = null

    @SerializedName("orderTypes")
    @Expose
    var orderTypes: List<String>? = null

    @SerializedName("paymentTypes")
    @Expose
    var paymentTypes: List<String>? = null

}