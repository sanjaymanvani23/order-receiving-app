package com.restaurant.management.data.repository.order

import androidx.lifecycle.MutableLiveData
import com.restaurant.management.data.model.request.GetOrderRequest
import com.restaurant.management.data.model.request.LoginRequest
import com.restaurant.management.data.model.request.UpdateOrderRequest
import com.restaurant.management.data.model.response.*
import com.restaurant.management.data.remote.BaseResponse
import com.restaurant.management.data.remote.Resource
import com.restaurant.management.data.repository.BaseRespository
import org.koin.core.KoinComponent

class OrderControllerRepository (private val iOrderControllerRepository: IOrderRepository): KoinComponent, IOrderControllerRespository, BaseRespository(){

    override fun GetOrder(
        branchId: String,
        request: GetOrderRequest,
        maxResultCount: String,
        skipCount: String,
        response: MutableLiveData<Resource<BaseResponse<OrderListItemResponse>>>
    ) {
       iOrderControllerRepository.GetOrder(branchId,request,maxResultCount,skipCount).enqueue(getCallback(response))
    }

    override fun logout(response: MutableLiveData<Resource<BaseResponse<Nothing>>>) {
        iOrderControllerRepository.logout().enqueue(getCallback(response))
    }

    override fun getHelpContent(
        branchId: String,
        response: MutableLiveData<Resource<BaseResponse<String>>>
    ) {
        iOrderControllerRepository.getHelpContent(branchId).enqueue(getCallback(response))
    }

    override fun isRestaurantOpen(
        branchId: String,
        response: MutableLiveData<Resource<BaseResponse<Boolean>>>
    ) {
        iOrderControllerRepository.isRestaurantOpen(branchId).enqueue(getCallback(response))
    }

    override fun openRestuarant(
        branchId: String,
        response: MutableLiveData<Resource<BaseResponse<Nothing>>>
    ) {
        iOrderControllerRepository.openRestaurant(branchId).enqueue(getCallback(response))
    }

    override fun closeRestuarant(
        branchId: String,
        response: MutableLiveData<Resource<BaseResponse<Nothing>>>
    ) {
        iOrderControllerRepository.closeRestaurant(branchId).enqueue(getCallback(response))
    }

    override fun getSettings(
        branchId: String,
        response: MutableLiveData<Resource<BaseResponse<SettingsResponse>>>
    ) {
        iOrderControllerRepository.settings(branchId).enqueue(getCallback(response))
    }

    override fun getOrderDetails(
        branchId: String,
        orderId: String,
        response: MutableLiveData<Resource<BaseResponse<OrderDetailsResponse>>>
    ) {
        iOrderControllerRepository.getOrderDetails(branchId,orderId).enqueue(getCallback(response))
    }

    override fun updateOrder(
        branchId: String,
        orderId: String,
        request: UpdateOrderRequest,
        response: MutableLiveData<Resource<BaseResponse<Nothing>>>
    ) {
        iOrderControllerRepository.updateOrderDetails(branchId,orderId,request).enqueue(getCallback(response))
    }

    override fun getCourier(
        branchId: String,
        response: MutableLiveData<Resource<BaseResponse<List<CourierResponse>>>>
    ) {
        iOrderControllerRepository.getCourier(branchId).enqueue(getCallback(response))
    }

    override fun getRejectReason(
        branchId: String,
        response: MutableLiveData<Resource<BaseResponse<List<RejectReasonResponse>>>>
    ) {
        iOrderControllerRepository.getRejectionReason(branchId).enqueue(getCallback(response))
    }

    override fun getLocalization(
        culture: String,
        response: MutableLiveData<Resource<BaseResponse<List<LanguageValuePairModel>>>>
    ) {
        iOrderControllerRepository.getLocalization(culture).enqueue(getCallback(response))
    }


}