 package com.restaurant.management.data.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.restaurant.management.data.model.response.Error
import com.restaurant.management.data.model.response.LoginResponse
import com.restaurant.management.data.model.response.Result
import com.restaurant.management.data.remote.BaseResponse
import com.restaurant.management.data.remote.Resource
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.lang.Exception

open class BaseRespository {

    fun <T> getCallback(responseData: MutableLiveData<Resource<BaseResponse<T>>>): Callback<BaseResponse<T>> {
        return object : Callback<BaseResponse<T>> {
            override fun onFailure(call: Call<BaseResponse<T>>, t: Throwable) {
                Log.e("error",t.toString())
                if (t is IOException)
                    responseData.value = Resource.failure("Server time out", null)
                else
                    responseData.value = Resource.failure(t.message!!, null)
            }
            override fun onResponse(call: Call<BaseResponse<T>>, response: Response<BaseResponse<T>>) {
                if (response.isSuccessful && response.body() != null) {
                        responseData.value = Resource.success(response.body()!!)

                } else {
                    try {
                        if(response.code()==204){
                            responseData.value = Resource.failure("204",null)
                        }else{
                            val errorResponse = getErrorResponse<T>(response.errorBody()!!)
                            responseData.value = Resource.failure(response.code().toString(),errorResponse)
                        }
                    }catch (t:Exception){

                    }
                }
            }
        }
    }

    fun <T> getErrorResponse(responseBody: ResponseBody): BaseResponse<T> {
        var truckErrorResponse: BaseResponse<T>
        try {
            val jObjError = JSONObject(responseBody.string())
            val listPathObject = jObjError.getJSONObject("error")
            val iter = listPathObject.keys() //This should be the iterator you want.
            val key = iter.next()
            truckErrorResponse = BaseResponse(
               error =listPathObject.getString("details"),
                result = null,
                success = false,
                message = null
            )
        } catch (e: JSONException) {
            truckErrorResponse = BaseResponse(
                error = null,
                result = null,
                success = false,
                message = null)
        }
        return truckErrorResponse
    }


    fun <T> getCallbackForLogin(responseData: MutableLiveData<Resource<T>>): Callback<T> {
        return object : Callback<T> {
            override fun onFailure(call: Call<T>, t: Throwable) {
                if (t is IOException)
                    responseData.value=Resource.failure("Server time out", null)
                else
                    responseData.value=Resource.failure(t.message!!, null)
            }
            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful && response.body() != null) {
                    responseData.value= Resource.success(response.body()!!)
                } else {
                    val errorResponse = getLoginErrorResponse<LoginResponse>(response.errorBody()!!)
                    responseData.value = Resource.failure(""+errorResponse.error.message, null)
                }
            }
        }
    }

    fun <T> getLoginErrorResponse(responseBody: ResponseBody): LoginResponse {
        var result=Result("","",0,false,false,"","",false,"",false,"","",1)
        val loginErrorResponse=
            LoginResponse(result,com.restaurant.management.data.model.response.Error(0,"Login failed!","Invalid user name or password","null"))
        try {
            val jObjError = JSONObject(responseBody.string())
            loginErrorResponse.error  =com.restaurant.management.data.model.response.Error(0,"Login failed!","Invalid user name or password","null")
        } catch (e: JSONException) {

        }
        return loginErrorResponse
    }

    fun createPlainTextRequestBody(data: String?): RequestBody {
        Log.i("Prepare", "$data -----coming")
        return RequestBody.create(MediaType.parse("text/plain"), data ?: "")
    }

    fun getRequestBody(file: File?): RequestBody? {
        if (file != null) {
            return if (file.exists()) {
                RequestBody.create(MediaType.parse("image/*"), file)
            } else {
                null
            }
        }else {
            return null
        }
    }

    fun prepareFilePart(partName: String, file: File?): MultipartBody.Part? {
        val requestBody = getRequestBody(file)
        return if (requestBody != null) {
            MultipartBody.Part.createFormData(partName, file!!.name, requestBody)
        } else {
            null
        }
    }


}