package com.restaurant.management.data.repository

import com.restaurant.management.data.model.LocationUpdateRequest
import com.restaurant.management.data.model.request.LoginRequest
import com.restaurant.management.data.model.response.ForgotPasswordResponse
import com.restaurant.management.data.model.response.LocationUpdateResponse
import com.restaurant.management.data.model.response.LoginResponse
import com.restaurant.management.data.remote.BaseResponse
import retrofit2.Call
import retrofit2.http.*

interface IUserRepository {

    @POST("TokenAuth/Authenticate")
    fun login(
        @Body request: LoginRequest
    ): Call<LoginResponse>
}