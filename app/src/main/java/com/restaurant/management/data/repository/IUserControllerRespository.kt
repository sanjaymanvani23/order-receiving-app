package com.restaurant.management.data.repository

import androidx.lifecycle.MutableLiveData
import com.restaurant.management.data.model.LocationUpdateRequest
import com.restaurant.management.data.model.request.LoginRequest
import com.restaurant.management.data.model.response.ForgotPasswordResponse
import com.restaurant.management.data.model.response.LocationUpdateResponse
import com.restaurant.management.data.model.response.LoginResponse
import com.restaurant.management.data.remote.BaseResponse
import com.restaurant.management.data.remote.Resource

interface IUserControllerRespository {

    fun login(loginRequest: LoginRequest, response: MutableLiveData<Resource<LoginResponse>>)

}