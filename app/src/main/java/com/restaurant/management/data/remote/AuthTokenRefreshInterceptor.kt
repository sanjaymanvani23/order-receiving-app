package com.restaurant.management.data.remote

import android.content.Intent
import android.util.Log
import com.restaurant.management.data.local.sharedPref.ISharedPreferenceService
import com.restaurant.management.utils.Constants
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.io.IOException
import kotlin.jvm.Throws


class AuthTokenRefreshInterceptor(private val sharedPreferences: ISharedPreferenceService)  : Interceptor, KoinComponent {

    private val sharedPreference: ISharedPreferenceService by inject()

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        var request = chain.request()
        val builder = request.newBuilder()
        builder.header("Accept", "application/json")
        val token = sharedPreferences.getStringValue(Constants.AUTHORIZATION_KEY)
        setAuthHeader(builder, token)
        request = builder.build()
        val response = chain.proceed(request)         // Sends the request (Original w/ Auth.)
        //------------------- 401 --- 401 --- UNAUTHORIZED --- 401 --- 401 -------------------------
        if (response.code() == RESPONSE_UNAUTHORIZED_401) { //If unauthorized (Token expired)...
            Log.w(TAG_THIS, "Request responses code: " + response.code())

            synchronized(this) {
                val currentToken = sharedPreferences.getStringValue(Constants.AUTHORIZATION_KEY)  //Get currently stored token (...)
                if (currentToken == token) {
                    val code = refreshToken() / 100                    //Refactor resp. cod ranking
                    if (code != RESPONSE_HTTP_RANK_2XX) {                // If refresh token failed
                        if (code == RESPONSE_HTTP_CLIENT_ERROR           // If failed by error 4xx...
                            || code == RESPONSE_HTTP_SERVER_ERROR
                        ) {   // If failed by error 5xx...

                            logout()                                   // ToDo GoTo login screen
                            return response                            // Todo Shows auth error to user
                        }
                    }   // <<--------------------------------------------New Auth. Token acquired --
                }   // <<-----------------------------------New Auth. Token acquired double check --
                setAuthHeader(builder, sharedPreferences.getStringValue(Constants.AUTHORIZATION_KEY) )   // Add Current Auth. Token
                request = builder.build()
                runCatching { response.close() }// O/w the original request
                val responseRetry = chain.proceed(request)// Sends request (w/ New Auth.)
                return responseRetry
            }
        } else {
            //------------------- 200 --- 200 --- AUTHORIZED --- 200 --- 200 -----------------------
            Log.w(TAG_THIS, "Request responses code: " + response.code())
        }
        return response
    }

    // Sets/Adds the authentication header to current request builder.-----------------------------|
    private fun setAuthHeader(builder: Request.Builder, token: String?) {
        Log.i(TAG_THIS, "Setting authentication header...")
        if (token != null) {
            builder.header("Authorization", String.format("Bearer %s", token))
        }

        Log.w(TAG_THIS, "Current Auth Token = " + sharedPreferences.getStringValue(Constants.AUTHORIZATION_KEY) )
        Log.w(TAG_THIS, "Current Refresh Token = " +sharedPreferences.getStringValue(Constants.REFRESH_TOKEN) )
    }

    // Refresh/renew Synchronously Authentication Token & refresh token----------------------------|
    private fun refreshToken(): Int {
        Log.w(TAG_THIS, "Refreshing tokens... ;o")
        // Builds a client...
        val client = OkHttpClient.Builder().build()

        val url = HttpUrl.parse("https://default.ordering.ch/api/tokenAuth/refreshToken")!!.newBuilder()
            .addQueryParameter("refreshToken", sharedPreference.getStringValue(Constants.REFRESH_TOKEN))
            .build()
        val request = Request.Builder()
            .url(url)
            .post(RequestBody.create(null,""))
            .removeHeader("Authorization")
            .build()

        var response: Response? = null
        var code = 0
        try {
            response = client.newCall(request).execute()       //Sends Refresh token request
            code = response.code()
            Log.i(TAG_THIS, "Token Refresh responses code: $code")
            when (code) {
                200 ->
                    // READS NEW TOKENS AND SAVES THEM -----------------------------------------
                    try {
                        //Log.i(TAG_THIS,"Decoding tokens start");
                        var jsonBody: JSONObject? = null
                        jsonBody = JSONObject(response.body()!!.string())
                        val resultObject=jsonBody.getJSONObject("result")
                        val newAuthtoken = resultObject.getString("accessToken")
                        Log.i(TAG_THIS, "New Access Token = $newAuthtoken")
                        sharedPreference.setStringValue(Constants.AUTHORIZATION_KEY,newAuthtoken)

                    } catch (e: JSONException) {
                        Log.w(
                            TAG_THIS, "Responses code " + code
                                    + " but error getting response body.\n"
                                    + e.message
                        )
                    }

                else ->
                    try {
                        var jsonBodyE: JSONObject? = null
                        jsonBodyE = JSONObject(response.body()!!.string())
                        val error = jsonBodyE.getString("error")
                        val errorDescription = jsonBodyE.getString("error_description")
                    } catch (e: JSONException) {
                        Log.w(
                            TAG_THIS, "Responses code " + code
                                    + " but error getting response body.\n"
                                    + e.message
                        )
                        e.printStackTrace()
                    }
            }
            response.body()!!.close()

        } catch (e: IOException) {
            Log.w(TAG_THIS, "Error while Sending Refresh Token Request\n" + e.message)
            e.printStackTrace()
        }
        return code

    }

    private fun logout(): Int {
        val intent = Intent()
//        intent.action = Constants.LOGOUT
//        MyApplication.appContext.sendBroadcast(intent)
        return 0
    }

    companion object {
        private val TAG_THIS = AuthTokenRefreshInterceptor::class.java.simpleName
        private val RESPONSE_UNAUTHORIZED_401 = 401
        private val RESPONSE_HTTP_RANK_2XX = 2
        private val RESPONSE_HTTP_CLIENT_ERROR = 4
        private val RESPONSE_HTTP_SERVER_ERROR = 5
        @Deprecated("")
        private fun bodyToString(request: Request): String {
            return "Nullix"
        }
    }

}
