package com.restaurant.management.data.model.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class LocationUpdateResponse {

    @SerializedName("usersLocationId")
    @Expose
    var usersLocationId: Int? = null

    @SerializedName("userId")
    @Expose
    var userId: Int? = null

    @SerializedName("lacationAddress")
    @Expose
    var lacationAddress: String? = null

    @SerializedName("longitude")
    @Expose
    var longitude: String? = null

    @SerializedName("latitude")
    @Expose
    var latitude: String? = null

    @SerializedName("isActive")
    @Expose
    var isActive: Boolean? = null

    @SerializedName("createdDate")
    @Expose
    var createdDate: String? = null

    @SerializedName("createdBy")
    @Expose
    var createdBy: Int? = null

    @SerializedName("updatedDate")
    @Expose
    var updatedDate: Any? = null

    @SerializedName("updatedBy")
    @Expose
    var updatedBy: Any? = null

    @SerializedName("user")
    @Expose
    var user: Any? = null
}