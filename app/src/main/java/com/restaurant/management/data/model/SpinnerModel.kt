package com.restaurant.management.data.model

data class SpinnerModel (
    val id:Int?=null,
    val name:String?=null,
)