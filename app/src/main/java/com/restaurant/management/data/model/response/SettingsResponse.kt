package com.restaurant.management.data.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class SettingsResponse {

    @SerializedName("applicationMedias")
    @Expose
    var applicationMedias: List<ApplicationMedia>? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("languages")
    @Expose
    var languages: List<Language>? = null

    @SerializedName("currency")
    @Expose
    var currency: String? = null

    @SerializedName("timeZone")
    @Expose
    var timeZone: TimeZone? = null

    @SerializedName("contactDetails")
    @Expose
    var contactDetails: ContactDetails? = null

    @SerializedName("isShowCustomerDetails")
    @Expose
    var isShowCustomerDetails: Boolean? = null


    class TimeZone {
        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("value")
        @Expose
        var value: String? = null
    }

    class ContactDetails {
        @SerializedName("address")
        @Expose
        var address: String? = null

        @SerializedName("phone")
        @Expose
        var phone: String? = null
    }

    class Language {
        @SerializedName("name")
        @Expose
        var name: String? = null

        @SerializedName("displayName")
        @Expose
        var displayName: String? = null

        @SerializedName("icon")
        @Expose
        var icon: String? = null
    }

    class ApplicationMedia {
        @SerializedName("type")
        @Expose
        var type: String? = null

        @SerializedName("blobLink")
        @Expose
        var blobLink: String? = null

        @SerializedName("mimeType")
        @Expose
        var mimeType: String? = null

        @SerializedName("fileName")
        @Expose
        var fileName: Any? = null

        @SerializedName("status")
        @Expose
        var status: String? = null

        @SerializedName("id")
        @Expose
        var id: Int? = null
    }
}