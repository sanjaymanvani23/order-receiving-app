package com.restaurant.management.data.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class OrderDetailsResponse {

    @SerializedName("creationTime")
    @Expose
    var creationTime: String? = null

    @SerializedName("refundAmount")
    @Expose
    var refundAmount: Any? = null

    @SerializedName("reasonForRejection")
    @Expose
    var reasonForRejection: Any? = null

    @SerializedName("currency")
    @Expose
    var currency: String? = null

    @SerializedName("tenantId")
    @Expose
    var tenantId: Int? = null

    @SerializedName("adminNote")
    @Expose
    var adminNote: Any? = null

    @SerializedName("customerNote")
    @Expose
    var customerNote: String? = null

    @SerializedName("orderType")
    @Expose
    var orderType: String? = null

    @SerializedName("orderDeliveryType")
    @Expose
    var orderDeliveryType: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("isTest")
    @Expose
    var isTest: Boolean? = null

    @SerializedName("orderRequestedDateTime")
    @Expose
    var orderRequestedDateTime: String? = null

    @SerializedName("expectedOrderFulFillDateTime")
    @Expose
    var expectedOrderFulFillDateTime: String? = null

    @SerializedName("preparationDuration")
    @Expose
    var preparationDuration: Double? = null

    @SerializedName("customerPhone")
    @Expose
    var customerPhone: String? = null

    @SerializedName("customerEmail")
    @Expose
    var customerEmail: String? = null

    @SerializedName("buyerAcceptsMarketing")
    @Expose
    var buyerAcceptsMarketing: Boolean? = null

    @SerializedName("orderItemsTotal")
    @Expose
    var orderItemsTotal: Double? = null

    @SerializedName("finalAmount")
    @Expose
    var finalAmount: Double? = null

    @SerializedName("paymentCommission")
    @Expose
    var paymentCommission: Double? = null

    @SerializedName("totalBeforeTax")
    @Expose
    var totalBeforeTax: Double? = null

    @SerializedName("totalAfterTax")
    @Expose
    var totalAfterTax: Double? = null

    @SerializedName("taxVariation")
    @Expose
    var taxVariation: String? = null

    @SerializedName("discount")
    @Expose
    var discount: Double? = null

    @SerializedName("couponCode")
    @Expose
    var couponCode: Any? = null

    @SerializedName("coupon")
    @Expose
    var coupon: Double? = null

    @SerializedName("deliveryCharge")
    @Expose
    var deliveryCharge: Double? = null

    @SerializedName("isDeliveryFree")
    @Expose
    var isDeliveryFree: Boolean? = null

    @SerializedName("taxRate")
    @Expose
    var taxRate: Double? = null

    @SerializedName("tax")
    @Expose
    var tax: Double? = null

    @SerializedName("loyaltyPointsConvertedAmount")
    @Expose
    var loyaltyPointsConvertedAmount: Any? = null

    @SerializedName("loyaltyPoints")
    @Expose
    var loyaltyPoints: Double? = null

    @SerializedName("loyaltyPointsConversionRate")
    @Expose
    var loyaltyPointsConversionRate: Int? = null

    @SerializedName("isAsap")
    @Expose
    var isAsap: Boolean? = null

    @SerializedName("paymentMethod")
    @Expose
    var paymentMethod: String? = null

    @SerializedName("paymentType")
    @Expose
    var paymentType: String? = null

    @SerializedName("checkOutID")
    @Expose
    var checkOutID: String? = null

    @SerializedName("orderAddressId")
    @Expose
    var orderAddressId: String? = null

    @SerializedName("branchId")
    @Expose
    var branchId: Int? = null

    @SerializedName("branch")
    @Expose
    var branch: Branch? = null

    @SerializedName("orderRejectReasonId")
    @Expose
    var orderRejectReasonId: Any? = null

    @SerializedName("orderRejectReason")
    @Expose
    var orderRejectReason: Any? = null

    @SerializedName("orderName")
    @Expose
    var orderName: Any? = null

    @SerializedName("orderItems")
    @Expose
    var orderItems: List<OrderItem>? = null

    @SerializedName("comboOrderItems")
    @Expose
    var comboOrderItems: List<Any>? = null

    @SerializedName("orderAddressFK")
    @Expose
    var orderAddressFK: OrderAddressFK? = null

    @SerializedName("orderStripePaymentFK")
    @Expose
    var orderStripePaymentFK: Any? = null

    @SerializedName("orderCustomerFK")
    @Expose
    var orderCustomerFK: OrderCustomerFK? = null

    @SerializedName("orderPayment")
    @Expose
    var orderPayment: OrderPayment? = null

    @SerializedName("isPaymentDeviceRequired")
    @Expose
    var isPaymentDeviceRequired: Boolean? = null

    @SerializedName("couriers")
    @Expose
    var couriers: List<Any>? = null

    @SerializedName("currentCourier")
    @Expose
    var currentCourier: Any? = null

    @SerializedName("orderTrack")
    @Expose
    var orderTrack: OrderTrack? = null

    @SerializedName("invoiceNo")
    @Expose
    var invoiceNo: String? = null

    @SerializedName("registerCustomerId")
    @Expose
    var registerCustomerId: Int? = null

    @SerializedName("id")
    @Expose
    var id: String? = null


    class OrderPayment {
        @SerializedName("amount")
        @Expose
        var amount: String? = null

        @SerializedName("paymentStatus")
        @Expose
        var paymentStatus: String? = null

        @SerializedName("paymentType")
        @Expose
        var paymentType: String? = null

        @SerializedName("cardNumber")
        @Expose
        var cardNumber: String? = null

        @SerializedName("id")
        @Expose
        var id: String? = null
    }

    class OrderTrack {
        @SerializedName("tenantId")
        @Expose
        var tenantId: Int? = null

        @SerializedName("orderId")
        @Expose
        var orderId: String? = null

        @SerializedName("trackingId")
        @Expose
        var trackingId: String? = null

        @SerializedName("id")
        @Expose
        var id: String? = null
    }


    class OrderItem {
        @SerializedName("orderId")
        @Expose
        var orderId: String? = null

        @SerializedName("mealId")
        @Expose
        var mealId: Int? = null

        @SerializedName("mealPriceId")
        @Expose
        var mealPriceId: Int? = null

        @SerializedName("mealPrice")
        @Expose
        var mealPrice: Double? = null

        @SerializedName("loyaltyPoints")
        @Expose
        var loyaltyPoints: Any? = null

        @SerializedName("quantity")
        @Expose
        var quantity: Int? = null

        @SerializedName("discount")
        @Expose
        var discount: Double? = null

        @SerializedName("order")
        @Expose
        var order: Any? = null

        @SerializedName("mealName")
        @Expose
        var mealName: String? = null

        @SerializedName("mealPriceName")
        @Expose
        var mealPriceName: Any? = null

        @SerializedName("mealPriceFK")
        @Expose
        var mealPriceFK: Any? = null

        @SerializedName("orderItemChoices")
        @Expose
        var orderItemChoices: List<OrderItemChoice>? = null

        @SerializedName("discountType")
        @Expose
        var discountType: Int? = null

        @SerializedName("id")
        @Expose
        var id: Int? = null

        inner class OrderItemChoice {
            @SerializedName("orderItemId")
            @Expose
            var orderItemId: Int? = null

            @SerializedName("choiceItemId")
            @Expose
            var choiceItemId: Int? = null

            @SerializedName("choiceItemPrice")
            @Expose
            var choiceItemPrice: Double? = null

            @SerializedName("choiceItemName")
            @Expose
            var choiceItemName: String? = null

            @SerializedName("quantity")
            @Expose
            var quantity: Int? = null

            @SerializedName("orderItem")
            @Expose
            var orderItem: Any? = null

            @SerializedName("choiceItem")
            @Expose
            var choiceItem: Any? = null

            @SerializedName("id")
            @Expose
            var id: Int? = null
        }


        fun getQty():String{
            var qty=quantity.toString()
            if(orderItemChoices.isNullOrEmpty()){
                return quantity.toString()+"\n"
            }else{
                orderItemChoices?.forEach {
                    qty=  qty+"\n"+it.quantity
                }
                return qty
            }
        }

        fun getItems():String{
            var items=mealName.toString()
            if(orderItemChoices.isNullOrEmpty()){
                return mealName.toString()
            }else{
                orderItemChoices?.forEach {
                    items=  items+"\n"+it.choiceItemName
                }
                return items
            }
        }

        fun getPrice():String{
            var items=mealPrice?.times(quantity!!).toString()
            if(orderItemChoices.isNullOrEmpty()){
                return items
            }else{
                orderItemChoices?.forEach {
                    items=  items+"\n"+it.choiceItemPrice?.times(quantity!!)
                }
                return items
            }
        }

    }

    class OrderCustomerFK {
        @SerializedName("tenantId")
        @Expose
        var tenantId: Int? = null

        @SerializedName("firstName")
        @Expose
        var firstName: Any? = null

        @SerializedName("lastName")
        @Expose
        var lastName: Any? = null

        @SerializedName("orderId")
        @Expose
        var orderId: String? = null

        @SerializedName("registeredCustomerId")
        @Expose
        var registeredCustomerId: Int? = null

        @SerializedName("rating")
        @Expose
        var rating: Int? = null

        @SerializedName("feedback")
        @Expose
        var feedback: Any? = null

        @SerializedName("id")
        @Expose
        var id: Int? = null
    }

    class OrderAddressFK {
        @SerializedName("customerName")
        @Expose
        var customerName: Any? = null

        @SerializedName("street")
        @Expose
        var street: Any? = null

        @SerializedName("no")
        @Expose
        var no: Any? = null

        @SerializedName("city")
        @Expose
        var city: Any? = null

        @SerializedName("postalCode")
        @Expose
        var postalCode: String? = null

        @SerializedName("province")
        @Expose
        var province: Any? = null

        @SerializedName("postalCodeId")
        @Expose
        var postalCodeId: Int? = null

        @SerializedName("countryId")
        @Expose
        var countryId: Int? = null

        @SerializedName("provinceId")
        @Expose
        var provinceId: Int? = null

        @SerializedName("orderId")
        @Expose
        var orderId: String? = null

        @SerializedName("country")
        @Expose
        var country: Any? = null

        @SerializedName("longitude")
        @Expose
        var longitude: Double? = null

        @SerializedName("latitude")
        @Expose
        var latitude: Double? = null

        @SerializedName("id")
        @Expose
        var id: String? = null
    }

    class Branch {
        @SerializedName("branchName")
        @Expose
        var branchName: String? = null

        @SerializedName("streetName")
        @Expose
        var streetName: String? = null

        @SerializedName("streetNumber")
        @Expose
        var streetNumber: Any? = null

        @SerializedName("city")
        @Expose
        var city: String? = null

        @SerializedName("primaryBranch")
        @Expose
        var primaryBranch: Boolean? = null

        @SerializedName("branchWebLink")
        @Expose
        var branchWebLink: Any? = null

        @SerializedName("isActive")
        @Expose
        var isActive: Boolean? = null

        @SerializedName("provinceFk")
        @Expose
        var provinceFk: Any? = null

        @SerializedName("postalCodeFk")
        @Expose
        var postalCodeFk: Any? = null

        @SerializedName("countryFk")
        @Expose
        var countryFk: Any? = null

        @SerializedName("longitude")
        @Expose
        var longitude: Any? = null

        @SerializedName("latitude")
        @Expose
        var latitude: Any? = null

        @SerializedName("contactDetails")
        @Expose
        var contactDetails: Any? = null

        @SerializedName("branchSchedules")
        @Expose
        var branchSchedules: List<Any>? = null

        @SerializedName("temporarySchedules")
        @Expose
        var temporarySchedules: List<Any>? = null

        @SerializedName("applicationMedia")
        @Expose
        var applicationMedia: List<Any>? = null

        @SerializedName("contentWidgets")
        @Expose
        var contentWidgets: List<Any>? = null

        @SerializedName("deliverySetting")
        @Expose
        var deliverySetting: Any? = null

        @SerializedName("preOrderSetting")
        @Expose
        var preOrderSetting: Any? = null

        @SerializedName("id")
        @Expose
        var id: Int? = null
    }
}
