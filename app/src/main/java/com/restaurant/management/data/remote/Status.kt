package com.restaurant.management.data.remote

enum class Status {
    SUCCESS, ERROR, LOADING, FAILURE
}