package com.restaurant.management.data.repository.order

import androidx.lifecycle.MutableLiveData
import com.restaurant.management.data.model.LocationUpdateRequest
import com.restaurant.management.data.model.request.GetOrderRequest
import com.restaurant.management.data.model.request.LoginRequest
import com.restaurant.management.data.model.request.UpdateOrderRequest
import com.restaurant.management.data.model.response.*
import com.restaurant.management.data.remote.BaseResponse
import com.restaurant.management.data.remote.Resource

interface IOrderControllerRespository {


    fun GetOrder(branchId:String,request: GetOrderRequest,maxResultCount:String,skipCount:String, response: MutableLiveData<Resource<BaseResponse<OrderListItemResponse>>>)

    fun logout(response: MutableLiveData<Resource<BaseResponse<Nothing>>>)

    fun getHelpContent(branchId:String,response: MutableLiveData<Resource<BaseResponse<String>>>)

    fun isRestaurantOpen(branchId:String,response: MutableLiveData<Resource<BaseResponse<Boolean>>>)

    fun openRestuarant(branchId:String,response: MutableLiveData<Resource<BaseResponse<Nothing>>>)

    fun closeRestuarant(branchId:String,response: MutableLiveData<Resource<BaseResponse<Nothing>>>)

    fun getSettings(branchId:String,response: MutableLiveData<Resource<BaseResponse<SettingsResponse>>>)

    fun getOrderDetails(branchId:String,orderId:String,response: MutableLiveData<Resource<BaseResponse<OrderDetailsResponse>>>)

    fun updateOrder(branchId:String, orderId:String, request: UpdateOrderRequest, response: MutableLiveData<Resource<BaseResponse<Nothing>>>)

    fun getCourier(branchId:String,response: MutableLiveData<Resource<BaseResponse<List<CourierResponse>>>>)

    fun getRejectReason(branchId:String,response: MutableLiveData<Resource<BaseResponse<List<RejectReasonResponse>>>>)

    fun getLocalization(culture:String,response: MutableLiveData<Resource<BaseResponse<List<LanguageValuePairModel>>>>)
}