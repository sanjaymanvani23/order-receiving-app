package com.restaurant.management.data.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LanguageValuePairModel (

    @SerializedName("key")
    @Expose
    var key: String? = null,

    @SerializedName("label")
    @Expose
    var label: String? = null
)