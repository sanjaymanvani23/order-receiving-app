package com.restaurant.management.data.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




data class LocationUpdateRequest (

    @SerializedName("userId")
    @Expose
    var userId: Int? = null,

    @SerializedName("lacationAddress")
    @Expose
    var lacationAddress: String? = null,

    @SerializedName("longitude")
    @Expose
    var longitude: String? = null,

    @SerializedName("latitude")
    @Expose
    var latitude: String? = null
)