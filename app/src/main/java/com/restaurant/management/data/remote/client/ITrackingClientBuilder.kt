package com.restaurant.management.data.remote.client

import okhttp3.OkHttpClient
import retrofit2.Retrofit

interface ITrackingClientBuilder {
    fun getClient(): OkHttpClient
    fun getBuilder(): Retrofit.Builder
    fun getRetrofit(): Retrofit
}