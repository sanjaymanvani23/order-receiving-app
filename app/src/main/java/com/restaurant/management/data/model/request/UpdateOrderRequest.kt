package com.restaurant.management.data.model.request

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class UpdateOrderRequest (

    @SerializedName("expectedOrderFulFillDateTime")
    @Expose
    var expectedOrderFulFillDateTime: String? = null,

    @SerializedName("status")
    @Expose
    var status: String? = null,

    @SerializedName("courierId")
    @Expose
    var courierId: Int? = null,

    @SerializedName("reasonForRejection")
    @Expose
    var reasonForRejection: String? = null,

    )