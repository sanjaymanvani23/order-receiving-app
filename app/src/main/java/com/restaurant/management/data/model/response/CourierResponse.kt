package com.restaurant.management.data.model.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class CourierResponse {
    @SerializedName("userId")
    @Expose
    var userId: Int? = null

    @SerializedName("userFk")
    @Expose
    var userFk: UserFk? = null

    @SerializedName("branchId")
    @Expose
    var branchId: Int? = null

    @SerializedName("isActive")
    @Expose
    var isActive: Boolean? = null

    @SerializedName("id")
    @Expose
    var id: Int? = null

    class UserFk {
        @SerializedName("name")
        @Expose
        var names: String? = null

        @SerializedName("surname")
        @Expose
        var surname: String? = null

        fun name():String=names+" "+surname

        @SerializedName("userName")
        @Expose
        var userName: String? = null

        @SerializedName("emailAddress")
        @Expose
        var emailAddress: String? = null

        @SerializedName("phoneNumber")
        @Expose
        var phoneNumber: Any? = null

        @SerializedName("profilePictureId")
        @Expose
        var profilePictureId: Any? = null

        @SerializedName("isEmailConfirmed")
        @Expose
        var isEmailConfirmed: Boolean? = null

        @SerializedName("roles")
        @Expose
        var roles: List<Any>? = null

        @SerializedName("isActive")
        @Expose
        var isActive: Boolean? = null

        @SerializedName("creationTime")
        @Expose
        var creationTime: String? = null

        @SerializedName("id")
        @Expose
        var id: Int? = null
    }
}