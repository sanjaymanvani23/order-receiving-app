package com.restaurant.management

import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.multidex.MultiDexApplication
import com.colan.kindercare.bindiingUtils.BaseDataBindingComponent
import com.facebook.stetho.Stetho
import com.restaurant.management.di.KoinCoreModule
import com.splunk.mint.Mint
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication : MultiDexApplication(){

    companion object {
        lateinit  var appContext: Context
        var instance: MyApplication = MyApplication()
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        DataBindingUtil.setDefaultComponent(BaseDataBindingComponent())
        Stetho.initializeWithDefaults(this)
        Mint.initAndStartSession(this, "a2b60124");
        val moduleList = listOf(KoinCoreModule().koinCoreModule)
        startKoin{
            androidContext(this@MyApplication)
            modules(moduleList)
        }
    }
}