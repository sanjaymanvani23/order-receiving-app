package com.restaurant.management.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.restaurant.management.R
import com.restaurant.management.data.model.sidenav.SideNavItem
import kotlinx.android.synthetic.main.item_slide_nav.view.*


class SideNavAdapter(private val onItemClick: ((position: Int, item: SideNavItem) -> Unit)) :
    RecyclerView.Adapter<SideNavAdapter.SideNavVH>() {
    var menuItemsList = ArrayList<SideNavItem>()
    private var checkedPosition = 0

    fun setNavItemsData( list: List<SideNavItem>){
        menuItemsList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): SideNavAdapter.SideNavVH {
        return SideNavVH(
            LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_slide_nav, viewGroup, false)
        )
    }

    inner class SideNavVH(inflate: View) : RecyclerView.ViewHolder(inflate) {




        fun setData(
            sideNavItem: SideNavItem,
        ) {
            if (checkedPosition == adapterPosition) {
                itemView.iv_nav_option?.setImageResource(sideNavItem.resourceId)
            } else {
                itemView.iv_nav_option?.setImageResource(sideNavItem.resourceIdInactive)
            }
            itemView.setOnClickListener {
                itemView.iv_nav_option?.setImageResource(sideNavItem.resourceId)
                if (checkedPosition != adapterPosition) {
                    notifyItemChanged(checkedPosition)
                    checkedPosition = adapterPosition
                }
                sideNavItem.let {
                    onItemClick.invoke(adapterPosition, sideNavItem)
                }
            }
            itemView.tv_nav_text?.text = sideNavItem.itemName
        }
    }

    override fun getItemCount(): Int {
        return menuItemsList.size
    }

    override fun onBindViewHolder(holder: SideNavVH, position: Int) {
        holder.setData(menuItemsList[position])
    }
}
